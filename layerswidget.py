from __future__ import print_function
from __future__ import division

#from gi.repository import GObject
from gi.repository import Gtk

import layers
import weakref

class LayersWidget(Gtk.Box):
  def __init__(self, canvas):
    Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
    
    self.canvas_ref = weakref.ref(canvas)
    
    self.layer_map = dict()
    
    self.pack_start(Gtk.Label("Layers"), False, False, 0)
    
    # --- Layer list ---
    self.list_store = Gtk.ListStore(str, bool, object)
    self.LS_NAME_INDEX    = 0
    self.LS_VISIBLE_INDEX = 1
    self.LS_OBJ_INDEX     = 2
    
    self.list_view = Gtk.TreeView(self.list_store)
    self.list_view.set_headers_visible(False)
    self.list_view.set_enable_search(False)
    
    cell_renderer = Gtk.CellRendererText()
    cell_renderer.set_property("editable", True)
    cell_renderer.connect("edited", self.on_edit_layer_name)
    
    # Size the list to always show at least N layers
    prefered_height = cell_renderer.get_preferred_height(self.list_view)[1] # [1] is the prefered size, [0] is the minimum
    self.list_view.set_size_request(-1, prefered_height * 5)
    
    toggle_cell_renderer = Gtk.CellRendererToggle()
    toggle_cell_renderer.connect("toggled", self.on_toggle_visible)

    self.list_view.append_column(Gtk.TreeViewColumn("Visible", toggle_cell_renderer, active=self.LS_VISIBLE_INDEX))
    self.list_view.append_column(Gtk.TreeViewColumn("Name", cell_renderer, text=self.LS_NAME_INDEX))
    
    self.list_view.get_selection().connect("changed", self.on_new_selection)
    self.list_view.get_selection().set_mode(Gtk.SelectionMode.BROWSE)
    
    scroll_view = Gtk.ScrolledWindow()
    scroll_view.set_size_request(-1, prefered_height * 5)
    scroll_view.add(self.list_view)
    self.pack_start(scroll_view, False, False, 0)
    
    # --- Layer Settings ---
    layer_mode_store = Gtk.ListStore(str, str)

    for m_name, m_id in layers.LayerModes.user.items():
      layer_mode_store.append([m_name, m_id])

    settings_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    self.mode_menu = Gtk.ComboBox.new_with_model(layer_mode_store)
    self.mode_menu.set_id_column(1)
    renderer_text = Gtk.CellRendererText()
    self.mode_menu.pack_start(renderer_text, True)
    self.mode_menu.add_attribute(renderer_text, "text", 0)
    label_widget = Gtk.Label("Mode")
    settings_box.set_tooltip_text("Layer Blend Mode")
    settings_box.pack_start(label_widget, False, False, 0)
    settings_box.pack_end(self.mode_menu, True, True, 0)
    self.pack_start(settings_box, False, False, 0)

    self.mode_menu.connect("changed", self.mode_menu_changed)

    settings_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    self.opacity_slider = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0, 100, 1)
    self.opacity_slider.set_property("draw-value", False)
    label_widget = Gtk.Label("Opacity")
    settings_box.set_tooltip_text("Layer Opacity %")
    settings_box.pack_start(label_widget, False, False, 0)
    settings_box.pack_end(self.opacity_slider, True, True, 0)
    self.pack_start(settings_box, False, False, 0)
    
    self.opacity_slider.connect("value-changed", self.opacity_slider_changed)
    
    # --- Buttons ---
    button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    
    self.add_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_ADD))
    self.add_button.connect("clicked", self.on_add_button)
    self.add_button.set_tooltip_text("New Layer")
    button_box.pack_start(self.add_button, False, False, 0)
    
    self.del_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_REMOVE))
    self.del_button.connect("clicked", self.on_del_button)
    self.del_button.set_tooltip_text("Delete layer")
    button_box.pack_start(self.del_button, False, False, 0)
    
    self.dup_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_COPY))
    self.dup_button.connect("clicked", self.on_dup_button)
    self.dup_button.set_tooltip_text("Duplicate layer")
    button_box.pack_start(self.dup_button, False, False, 0)
    
    #FIXME: Register this as a stock icon
    self.merge_button = Gtk.Button(image=Gtk.Image(stock="thedraw-merge-down"))
    self.merge_button.connect("clicked", self.on_merge_button)
    self.merge_button.set_tooltip_text("Merge layer down")
    button_box.pack_start(self.merge_button, False, False, 0)
    
    self.up_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_GO_UP))
    self.up_button.connect("clicked", self.on_move_button, 1)
    self.up_button.set_tooltip_text("Move layer up")
    button_box.pack_start(self.up_button, False, False, 0)
    
    self.down_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_GO_DOWN))
    self.down_button.connect("clicked", self.on_move_button, -1)
    self.down_button.set_tooltip_text("Move layer down")
    button_box.pack_start(self.down_button, False, False, 0)
    
    button_allignment = Gtk.Alignment()
    button_allignment.set(0.5, 0, 0, 0)
    button_allignment.add(button_box)
    
    self.pack_start(button_allignment, False, False, 0)
    
    self.update_layers()
  
  def opacity_slider_changed(self, widget):
    value = widget.get_value()
    canvas = self.canvas_ref()
    if canvas and canvas.current_layer:
      canvas.set_layer_prop(canvas.current_layer, "opacity", value)

  def mode_menu_changed(self, widget):
    value = widget.get_active_id()
    canvas = self.canvas_ref()
    if canvas and canvas.current_layer:
      canvas.set_layer_prop(canvas.current_layer, "mode", value)
  
  def update_layer_settings(self):
    list_store, tree_iter = self.list_view.get_selection().get_selected()
    if not tree_iter:
      # Ignore empty selection
      return
    
    target_layer = list_store[tree_iter][self.LS_OBJ_INDEX]()
    if target_layer is None:
      return
      
    self.opacity_slider.handler_block_by_func(self.opacity_slider_changed)
    try:
      self.opacity_slider.set_value(target_layer.opacity * 100)
    finally:
      self.opacity_slider.handler_unblock_by_func(self.opacity_slider_changed)

    self.mode_menu.handler_block_by_func(self.mode_menu_changed)
    try:
      self.mode_menu.set_active_id(target_layer.mode)
    finally:
      self.mode_menu.handler_unblock_by_func(self.mode_menu_changed)
  
  def on_add_button(self, widget):
    canvas = self.canvas_ref()
    if canvas:
      active_id = canvas.layers.index(canvas.current_layer)
      new_layer = canvas.new_layer(active_id)
      canvas.set_current_layer(new_layer)
  
  def on_del_button(self, widget):
    canvas = self.canvas_ref()
    if canvas:
      canvas.del_layer(canvas.current_layer)
  
  def on_dup_button(self, widget):
    canvas = self.canvas_ref()
    if canvas:
      new_layer = canvas.current_layer.duplicate()
      canvas.insert_layer(canvas.current_layer, new_layer)
      canvas.set_current_layer(new_layer)
  
  def on_merge_button(self, widget):
    canvas = self.canvas_ref()
    if canvas and canvas.current_layer:
      canvas.merge_down_layer(canvas.current_layer)
  
  def on_move_button(self, widget, shift):
    canvas = self.canvas_ref()
    if canvas:
      new_pos = canvas.layers.index(canvas.current_layer) + shift
      canvas.move_layer(canvas.current_layer, new_pos)
  
  def on_new_selection(self, selection):
    list_store, tree_iter = selection.get_selected()
    if not tree_iter:
      # Ignore empty selection
      return
    
    row = list_store[tree_iter]
    
    canvas = self.canvas_ref()
    if canvas:
      target_layer = row[self.LS_OBJ_INDEX]()
      if target_layer:
        canvas.set_current_layer(target_layer)
    self.update_layer_settings()

  def on_toggle_visible(self, widget, path):
    canvas = self.canvas_ref()
    layer = self.list_store[path][self.LS_OBJ_INDEX]()
    if canvas and layer:
      visible = not widget.get_active()
      canvas.set_layer_prop(layer, "visible", visible)
      self.list_store[path][self.LS_VISIBLE_INDEX] = visible

  def on_edit_layer_name(self, widget, path, text):
    canvas = self.canvas_ref()
    layer = self.list_store[path][self.LS_OBJ_INDEX]()
    if canvas and layer:
      canvas.set_layer_prop(layer, "name", text)
      self.list_store[path][self.LS_NAME_INDEX] = text
  
  def update_layers(self):
    self.list_view.get_selection().handler_block_by_func(self.on_new_selection)
    try:
      self.list_store.clear()
      
      canvas = self.canvas_ref()
      if canvas:
        index = 0
        active_index = None
        for l in reversed(canvas.layers):
          #FIXME: For some reason this just will not let go of the layer object, use a weakref for now
          self.list_store.append([l.name, l.visible, weakref.ref(l)])
          if l == canvas.current_layer:
            active_index = index
          index += 1
        if active_index is not None:
          self.list_view.get_selection().select_path("%d" % active_index)
    finally:
      self.list_view.get_selection().handler_unblock_by_func(self.on_new_selection)
    
    self.update_layer_settings()
