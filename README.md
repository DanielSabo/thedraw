TheDraw
=======
TheDraw is a collection of Python+GTK3 widgets that happen to combine into a painting application.

"Installation"
=============
There is currently no install target, run `scons` in the root directory to build the required python module. Run `./thedraw.py` to start the application.

Requirements
===========
   - SCons 2.1 or newer
   - BABL (git master)
   - GEGL (git master, build with --enable-introspection)
   - GTK 3.4 or newer
   - Python 2.7 (python-dev)
   - PyCairo (python-cairo-dev)
   - PyGObject (python-gi-dev)
   - GTK introspection (gir1.2-gtk-3.0)
   - Rsvg introspection (gir1.2-rsvg-2.0)
   - gtk-mac-integration (if building for GTK Quartz)

