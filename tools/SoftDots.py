import math
import random

from gi.repository import GObject
from settingswidget import GPropSetting, GPropSlider, GPropColor
import tools.BasicBrush

class SoftDots(tools.BasicBrush.Brush):
  __gproperties__ = {
    'size-spread' : (GObject.TYPE_FLOAT,
        'Size Spread',
        'dot size = (0.0, size spread) * brush size',
        0.0,
        1.0,
        0.2,
        GObject.PARAM_READWRITE),
    'opacity-spread' : (GObject.TYPE_FLOAT,
        'Opacity Spread',
        'paint opacity = brush opacity +/- opacity spread',
        0.0,
        100.0,
        0.0,
        GObject.PARAM_READWRITE),
    }
  
  setting_list = [
      GPropSetting("size"),
      GPropSlider("size-dynamics"),
      GPropSlider("size-spread"),
      GPropSlider("opacity"),
      GPropSlider("opacity-dynamics"),
      GPropSlider("opacity-spread"),
      GPropColor("color"),
    ]

  def __init__(self):
    tools.BasicBrush.Brush.__init__(self)
    self.name = "Soft Dots"
  
  def new_stroke(self):
    return BrushStroke(self)

class BrushStroke(tools.BasicBrush.BrushStroke):
  def __init__(self, brush):
    tools.BasicBrush.BrushStroke.__init__(self, brush)
    self.size_spread = brush.size_spread
    self.opacity_spread = brush.opacity_spread / 100.0
    self.seed = hash(random.random()) # No idea how random this actualy will be, 
    self.random_generator = random.Random(self.seed)
    
  def apply_dab(self, drawbuf, x, y, pressure):
    mapped_brush_opaicty = self.calc_dynamic_opacity(pressure)
    mapped_brush_size    = self.calc_dynamic_size(pressure)
    
    dab_size = mapped_brush_size * self.size_spread * self.random_generator.random()
    angle    = math.pi * 2 * self.random_generator.random()
    dab_dist = (mapped_brush_size - dab_size * 2) * self.random_generator.random()
    if dab_dist < 0:
      dab_dist = 0
    
    dab_aspect = 2.0
    dab_angle  = 360 * self.random_generator.random()
    
    opacity_shift = self.opacity_spread * self.random_generator.random() - self.opacity_spread / 2
    mapped_brush_opaicty = min(1.0, max(0.0, mapped_brush_opaicty + opacity_shift))
    
    color = (self.color[0], self.color[1], self.color[2], mapped_brush_opaicty)
    
    if not dab_size:
      return
    x = x + math.sin(angle) * dab_dist
    y = y + math.cos(angle) * dab_dist
    drawbuf.soft_dab(x, y, dab_size, color=color, hardness=0.8, aspect_ratio=dab_aspect, angle=dab_angle)
