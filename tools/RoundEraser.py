import math
import layers

import tools.BasicBrush

class RoundEraser(tools.BasicBrush.Brush):
  setting_list = [s for s in tools.BasicBrush.Brush.setting_list if s.prop_name != "color"]
  
  def __init__(self):
    tools.BasicBrush.Brush.__init__(self)

  def new_stroke(self):
    return BrushStroke(self)
      
class BrushStroke(tools.BasicBrush.BrushStroke):
  def apply_dab(self, drawbuf, x, y, pressure):
    mapped_brush_opaicty = self.calc_dynamic_opacity(pressure)
    mapped_brush_size    = self.calc_dynamic_size(pressure)

    color = (0.0, 0.0, 0.0, mapped_brush_opaicty)
    drawbuf.circle(x, y, mapped_brush_size, color=color)
  
  def start_stroke(self, target_layer, x, y, pressure):
    target_layer.set_mode(layers.LayerModes.ERASE)
    self.current_stroke_points = []
    self.current_stroke_points.append((x, y, pressure))
    
    self.apply_dab(target_layer.drawbuf, x, y, pressure)
    
    min_x = x - 1 * self.brush.size
    min_y = y - 1 * self.brush.size
    max_x = x + 1 * self.brush.size
    max_y = y + 1 * self.brush.size
    
    min_x = int(math.floor(min_x))
    min_y = int(math.floor(min_y))
    max_x = int(math.ceil(max_x))
    max_y = int(math.ceil(max_y))
    
    return (min_x, min_y, max_x, max_y)