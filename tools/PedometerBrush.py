from gi.repository import GObject

class PedometerBrush(GObject.Object):
    __gproperties__ = {
    'size' : (GObject.TYPE_FLOAT,
        'Size',
        'Brush Radius',
        1.0,
        1000.0,
        5.0,
        GObject.PARAM_READWRITE),
    'Opacity' : (GObject.TYPE_FLOAT,
        'Opacity',
        'Opacity',
        0.0,
        1.0,
        0.01,
        GObject.PARAM_READWRITE),
    'color' : (GObject.TYPE_PYOBJECT,
        'Color',
        'Color',
        GObject.PARAM_READWRITE),
    }
