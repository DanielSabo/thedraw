from gi.repository import GObject
from settingswidget import GPropSetting, GPropSlider, GPropColor

import math

import tools.BasicBrush

class SoftRound(tools.BasicBrush.Brush):
  __gproperties__ = {
    'hardness' : (GObject.TYPE_FLOAT,
        'Hardness',
        'Hardness',
        0.0,
        1.0,
        0.8,
        GObject.PARAM_READWRITE),
    }
  
  setting_list = [
      GPropSetting("size"),
      GPropSlider("size-dynamics"),
      GPropSlider("opacity"),
      GPropSlider("opacity-dynamics"),
      GPropSlider("hardness"),
      GPropColor("color"),
    ]
  
  def __init__(self):
    tools.BasicBrush.Brush.__init__(self)
  
  def new_stroke(self):
    return BrushStroke(self)

class BrushStroke(tools.BasicBrush.BrushStroke):
  def __init__(self, brush):
    tools.BasicBrush.BrushStroke.__init__(self, brush)
    self.hardness = brush.hardness
    self.rate_coefficient = 0.1
    self.last_dabbed_x = None
    self.last_dabbed_y = None

  def apply_dab(self, drawbuf, x, y, pressure):
    mapped_brush_opaicty = self.calc_dynamic_opacity(pressure)
    mapped_brush_size    = self.calc_dynamic_size(pressure)

    color = (self.color[0], self.color[1], self.color[2], mapped_brush_opaicty)
    
    should_do_dab = False
    if not self.last_dabbed_x:
      # Always apply the first dab
      should_do_dab = True
    elif mapped_brush_size < 5:
      # Small brush sizes can show artifacts around the edge of the circle if
      # we try to rate limit them.
      should_do_dab = True
    else:
      dist = math.sqrt((x - self.last_dabbed_x) ** 2 + (y - self.last_dabbed_y) ** 2)
      if dist > mapped_brush_size * self.rate_coefficient:
        should_do_dab = True
    
    if should_do_dab:
      drawbuf.soft_circle(x, y, mapped_brush_size, hardness=self.hardness, color=color)
      self.last_dabbed_x = x
      self.last_dabbed_y = y
