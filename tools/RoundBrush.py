import math

#from settingswidget import GPropSetting, GPropSlider, GPropColor
import tools.BasicBrush

class RoundBrush(tools.BasicBrush.Brush):
  def __init__(self):
    tools.BasicBrush.Brush.__init__(self)
  
  def new_stroke(self):
    return BrushStroke(self)

class BrushStroke(tools.BasicBrush.BrushStroke):
  def __init__(self, brush):
    tools.BasicBrush.BrushStroke.__init__(self, brush)
    self.rate_coefficient = 0.1
    self.last_dabbed_x = None
    self.last_dabbed_y = None

  def apply_dab(self, drawbuf, x, y, pressure):
    mapped_brush_opaicty = self.calc_dynamic_opacity(pressure)
    mapped_brush_size    = self.calc_dynamic_size(pressure)

    color = (self.color[0], self.color[1], self.color[2], mapped_brush_opaicty)
    
    should_do_dab = False
    if not self.last_dabbed_x:
      # Always apply the first dab
      should_do_dab = True
    elif mapped_brush_size < 5:
      # Small brush sizes can show artifacts around the edge of the circle if
      # we try to rate limit them.
      should_do_dab = True
    else:
      dist = math.sqrt((x - self.last_dabbed_x) ** 2 + (y - self.last_dabbed_y) ** 2)
      if dist > mapped_brush_size * self.rate_coefficient:
        should_do_dab = True
    
    if should_do_dab:
      drawbuf.circle(x, y, mapped_brush_size, color=color)
      self.last_dabbed_x = x
      self.last_dabbed_y = y
