from gi.repository import GObject
from settingswidget import GPropSetting, GPropSlider, GPropColor

import math
import layers
from util import raster_line

def calc_dynmaic_sqr(coef, pressure, value):
  return (coef * (pressure ** 2) + (1.0 - coef)) * value

def calc_dynmaic_inv_sqr(coef, pressure, value):
  return (coef * (1 - (pressure - 1) ** 2) + (1.0 - coef)) * value

class Brush(GObject.Object):
  __gproperties__ = {
    'name' : (GObject.TYPE_STRING,
        'Name',
        'Brush Name',
        'Brush',
        GObject.PARAM_READWRITE),
    'size' : (GObject.TYPE_FLOAT,
        'Size',
        'Brush radius (px)',
        1.0,
        1000.0,
        6.0,
        GObject.PARAM_READWRITE),
    'size-dynamics' : (GObject.TYPE_FLOAT,
        'Size Dynamics',
        'Brush size dynamics coefficent',
        0.0,
        1.0,
        0.8,
        GObject.PARAM_READWRITE),
    'opacity' : (GObject.TYPE_FLOAT,
        'Opacity',
        'Brush opacity %',
        0.0,
        100.0,
        100.0,
        GObject.PARAM_READWRITE),
    'opacity-dynamics' : (GObject.TYPE_FLOAT,
        'Opacity Dynamics',
        'Brush opacity dynamics coefficent',
        0.0,
        1.0,
        0.8,
        GObject.PARAM_READWRITE),
    'color' : (GObject.TYPE_PYOBJECT,
        'Color',
        'Brush color',
        GObject.PARAM_READWRITE),
    }
  
  __gsignals__ = {
    'modified' : (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE,
                 ())
    }
  
  setting_list = [
      GPropSetting("size"),
      GPropSlider("size-dynamics"),
      GPropSlider("opacity"),
      GPropSlider("opacity-dynamics"),
      GPropColor("color"),
    ]
   
  def __init__(self):
    GObject.Object.__init__(self)
    
    for pspec in self.props:
      self.__dict__[pspec.name.replace("-", "_")] = pspec.default_value
    self.color = (0.0, 0.0, 0.0)
  
  def clone(self):
    rb = self.__class__()
    for pspec in self.props:
      name = pspec.name.replace("-", "_")
      rb.__dict__[name] = self.__dict__[name]
    return rb

  def new_stroke(self):
    return BrushStroke(self)
    
  # --- GObject glue ---
  def do_set_property(self, prop, value):
    prop_name = prop.name.replace("-", "_")
    modified = False
    if prop_name == "color":
      if self.color != value:
        self.color = value[:]
        modified = True
    elif prop_name in self.__dict__:
      if self.__dict__[prop_name] != value:
        self.__dict__[prop_name] = value
        modified = True
    else:
      raise ValueError("Unkown property name: %s", prop_name)

    if modified:
      self.emit("modified")

  def do_get_property(self, prop):
    prop_name = prop.name.replace("-", "_")
    if prop_name in self.__dict__:
      return self.__dict__[prop_name]
    else:
      raise ValueError("Unkown property name: %s", prop_name)
      
class BrushStroke():
  def __init__(self, brush):
    self.brush = brush
    self.size = brush.size
    self.size_dynamic = brush.size_dynamics
    self.opacity = brush.opacity / 100.0
    self.opacity_dynamic = brush.opacity_dynamics
    self.color = brush.color
    
    self.current_stroke_points = []
  
  def apply_dab(self, drawbuf, x, y, pressure):
    pass
  
  def calc_dynamic_opacity(self, pressure):
    return calc_dynmaic_sqr(self.opacity_dynamic, pressure, self.opacity)

  def calc_dynamic_size(self, pressure):
    return calc_dynmaic_inv_sqr(self.size_dynamic, pressure, self.size)
  
  def start_stroke(self, target_layer, x, y, pressure):
    target_layer.set_mode(layers.LayerModes.NORMAL)
    self.current_stroke_points = []
    self.current_stroke_points.append((x, y, pressure))
    
    self.apply_dab(target_layer.drawbuf, x, y, pressure)
    
    min_x = x - 1 - self.brush.size
    min_y = y - 1 - self.brush.size
    max_x = x + 1 + self.brush.size
    max_y = y + 1 + self.brush.size
    
    min_x = int(math.floor(min_x))
    min_y = int(math.floor(min_y))
    max_x = int(math.ceil(max_x))
    max_y = int(math.ceil(max_y))
        
    return (min_x, min_y, max_x, max_y)
  
  def stroke_to(self, target_layer, x, y, pressure):
    last_x, last_y, last_pressure = self.current_stroke_points[-1]
    
    delta_pressure = pressure - last_pressure
    line_len = math.sqrt((x - last_x) ** 2 + (y - last_y) ** 2)
    
    for line_x, line_y in raster_line(last_x, last_y, x, y):
      point_dist = math.sqrt((line_x - last_x) ** 2 + (line_y - last_y) ** 2)
      if not line_len:
        line_pressure = pressure
      else:
        line_pressure = last_pressure + delta_pressure * (point_dist / line_len)
      self.apply_dab(target_layer.drawbuf, line_x, line_y, line_pressure)
    
    self.current_stroke_points.append((x, y, pressure))
    
    min_x = min(last_x, x) - 1 - self.brush.size
    min_y = min(last_y, y) - 1 - self.brush.size
    max_x = max(last_x, x) + 1 + self.brush.size
    max_y = max(last_y, y) + 1 + self.brush.size
    
    min_x = int(math.floor(min_x))
    min_y = int(math.floor(min_y))
    max_x = int(math.ceil(max_x))
    max_y = int(math.ceil(max_y))
    
    return (min_x, min_y, max_x, max_y)
  
  def end_stroke(self, target_layer, x, y, pressure):
    min_x = float("inf")
    min_y = float("inf")
    max_x = float("-inf")
    max_y = float("-inf")
    
    for x, y, p in self.current_stroke_points:
      min_x = min(min_x, x - 1 - self.brush.size)
      min_y = min(min_y, y - 1 - self.brush.size)
      max_x = max(max_x, x + 1 + self.brush.size)
      max_y = max(max_y, y + 1 + self.brush.size)
    min_x = int(math.floor(min_x))
    min_y = int(math.floor(min_y))
    max_x = int(math.ceil(max_x))
    max_y = int(math.ceil(max_y))

    return (min_x, min_y, max_x, max_y)
  
  def stroke_points(self, target_layer, points):
    self.start_stroke(target_layer, points[0][0], points[0][1], points[0][2])
    
    for p in points[1:]:
      self.stroke_to(target_layer, p[0], p[1], p[2])
    
    return self.end_stroke(target_layer, points[-1][0], points[-1][1], points[-1][2])
