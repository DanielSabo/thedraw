from gi.repository import GObject

import math

import drawbuffer

import tools.BasicBrush

class ImageBrush(tools.BasicBrush.Brush):
  __gproperties__ = {
    'mask' : (GObject.TYPE_PYOBJECT,
        'Mask',
        'The MaskBuffer object applied by the brush',
        GObject.PARAM_READWRITE),
    'airbrush' : (GObject.TYPE_BOOLEAN,
        'Airbush',
        'Use "airbrush" style blending',
        False,
        GObject.PARAM_READWRITE),
    }
   
  def __init__(self, mask=None):
    tools.BasicBrush.Brush.__init__(self)
    self.mask = mask

  def new_stroke(self):
    return BrushStroke(self)

class BrushStroke(tools.BasicBrush.BrushStroke):
  def __init__(self, brush):
    tools.BasicBrush.BrushStroke.__init__(self, brush)
    self.mask = brush.mask
    if brush.airbrush:
      self.blendmode = drawbuffer.BLEND_MODE_OVER
    else:
      self.blendmode = drawbuffer.BLEND_MODE_GROW_ALPHA
  
  def apply_dab(self, drawbuf, x, y, pressure):
    mapped_brush_opaicty = self.calc_dynamic_opacity(pressure)
    mapped_brush_size    = self.calc_dynamic_size(pressure)

    color = (self.color[0], self.color[1], self.color[2], mapped_brush_opaicty)
    
    brush_width = mapped_brush_size * 2
    if brush_width > 0:
      x_offset, apply_x = math.modf(x)
      y_offset, apply_y = math.modf(y)
      
      scaled_mask = self.mask.scale_with_size(0, 0, brush_width, brush_width)
      blendmode=drawbuffer.BLEND_MODE_GROW_ALPHA
      drawbuf.apply_mask(int(apply_x) - scaled_mask.width // 2,
                         int(apply_y) - scaled_mask.height // 2,
                         scaled_mask, color=color,
                         blendmode=self.blendmode)

