from gi.repository import Gtk

class LimitedEntry(Gtk.Entry):
  def __init__(self, valid_chars=None):
    Gtk.Entry.__init__(self)
    
    self.valid_chars = valid_chars
    
    self.connect("insert-text", self.do_insert_text)
  
  def do_insert_text(self, widget, new_text, pos, user_data):
    if self.valid_chars is not None:
      for c in new_text:
        if not c in self.valid_chars:
          self.stop_emission("insert-text")
          return

class IntEntry(LimitedEntry):
  def __init__(self):
    LimitedEntry.__init__(self, "1234567890")
