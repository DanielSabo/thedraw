from gi.repository import Gdk

import cairo
import math

def make_cursor(radius):
  gdk_display = Gdk.Display.get_default()
  
  max_size = max(gdk_display.get_maximal_cursor_size())
  radius = min(int(math.ceil(radius)), max_size//2 - 2)
  
  img = cairo.ImageSurface(cairo.FORMAT_ARGB32, radius*2+4, radius*2+4)

  cr = cairo.Context(img)
  cr.set_source_rgba(0.0, 0.0, 0.0, 0.0);
  cr.paint()
  cr.set_line_width(1.5)
  cr.set_operator(cairo.OPERATOR_SOURCE)
  cr.set_source_rgba(1.0, 1.0, 1.0, 1.0);
  cr.arc(radius+2, radius+2, radius, 0, 2*math.pi)
  cr.stroke()
  cr.set_line_width(1)
  cr.set_source_rgba(0.0, 0.0, 0.0, 1.0);
  cr.arc(radius+2, radius+2, radius+1, 0, 2*math.pi)
  cr.stroke()
  img.flush()
  
  pixbuf = Gdk.pixbuf_get_from_surface(img,
                                       0, 0,
                                       img.get_width(), img.get_height())
  
  cursor = Gdk.Cursor.new_from_pixbuf(Gdk.Display.get_default(),
                                      pixbuf,
                                      pixbuf.get_width()  // 2,
                                      pixbuf.get_height() // 2)
  return cursor
