#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import prep
prep.init()

from gi.repository import Gegl
import drawbuffer
import unittest
import layers

from testcommon import make_color, check_pixel

class TestLayer(unittest.TestCase):
  def test_layer_from_drawbuf(self):
    buf = drawbuffer.DrawBuffer(100,100)
    layer = layers.Layer(drawbuf=buf)
    
    self.assertEqual(buf.width,  layer.drawbuf.width)
    self.assertEqual(buf.height, layer.drawbuf.height)
    self.assertEqual(buf, layer.drawbuf)
    
  def test_layer_from_size(self):
    layer = layers.Layer(width=100,height=100)
    
    self.assertEqual(layer.drawbuf.width, 100)
    self.assertEqual(layer.drawbuf.height, 100)
    self.assertIsNotNone(layer.drawbuf)

  def test_fail_no_arguments(self):
    with self.assertRaises(ValueError):
      layers.Layer()

  def test_fail_too_many_arguments(self):
    buf = drawbuffer.DrawBuffer(100,100)
    with self.assertRaises(ValueError):
      layers.Layer(buf, 100, 100)

  def test_init_with_name(self):
    layer = layers.Layer(width=100,height=100, name="Bob")
    self.assertEqual(layer.name, "Bob")
    
  def test_merge_down(self):
    white_layer = layers.Layer(width=100,height=100, name="White")
    black_layer = layers.Layer(width=100,height=100, name="Black")
    white_layer.drawbuf.fill(color=make_color("#FFFFFFFF"))
    black_layer.drawbuf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(0,0), "#000000FF")
    
    white_layer.merge_down(black_layer)
    self.assertEqual(black_layer.x, 0)
    self.assertEqual(black_layer.y, 0)
    self.assertEqual(black_layer.width,  100)
    self.assertEqual(black_layer.height, 100)
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    
  def test_merge_down_expand(self):
    white_layer = layers.Layer(width=100,height=100, name="White")
    black_layer = layers.Layer(x=50, y=50, width=100,height=100, name="Black")
    white_layer.drawbuf.fill(color=make_color("#FFFFFFFF"))
    black_layer.drawbuf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#000000FF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
    white_layer.merge_down(black_layer, expand=True)
    self.assertEqual(black_layer.x, 0)
    self.assertEqual(black_layer.y, 0)
    self.assertEqual(black_layer.width,  150)
    self.assertEqual(black_layer.height, 150)
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, white_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
  def test_merge_down_no_expand(self):
    white_layer = layers.Layer(width=100,height=100, name="White")
    black_layer = layers.Layer(x=50, y=50, width=100,height=100, name="Black")
    white_layer.drawbuf.fill(color=make_color("#FFFFFFFF"))
    black_layer.drawbuf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#000000FF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
    white_layer.merge_down(black_layer)
    self.assertEqual(black_layer.x, 50)
    self.assertEqual(black_layer.y, 50)
    self.assertEqual(black_layer.width,  100)
    self.assertEqual(black_layer.height, 100)
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, white_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
  def test_merge_down_expand_rect(self):
    white_layer = layers.Layer(width=100,height=100, name="White")
    black_layer = layers.Layer(x=50, y=50, width=100,height=100, name="Black")
    white_layer.drawbuf.fill(color=make_color("#FFFFFFFF"))
    black_layer.drawbuf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#000000FF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
    white_layer.merge_down(black_layer, (25, 25, 25, 25), expand=True)
    self.assertEqual(black_layer.x, 25)
    self.assertEqual(black_layer.y, 25)
    self.assertEqual(black_layer.width,  125)
    self.assertEqual(black_layer.height, 125)
    check_pixel(self, white_layer.drawbuf.get_pixel(25,25), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(25,25), "#FFFFFFFF")
    check_pixel(self, white_layer.drawbuf.get_pixel(49,49), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(49,49), "#FFFFFFFF")
    check_pixel(self, white_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#000000FF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
  def test_merge_down_no_expand_rect(self):
    white_layer = layers.Layer(width=100,height=100, name="White")
    black_layer = layers.Layer(x=50, y=50, width=100,height=100, name="Black")
    white_layer.drawbuf.fill(color=make_color("#FFFFFFFF"))
    black_layer.drawbuf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#000000FF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
    
    white_layer.merge_down(black_layer, (25, 25, 60, 60))
    self.assertEqual(black_layer.x, 50)
    self.assertEqual(black_layer.y, 50)
    self.assertEqual(black_layer.width,  100)
    self.assertEqual(black_layer.height, 100)
    check_pixel(self, white_layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, white_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(50,50), "#FFFFFFFF")
    check_pixel(self, white_layer.drawbuf.get_pixel(84,84), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(84,84), "#FFFFFFFF")
    check_pixel(self, black_layer.drawbuf.get_pixel(85,85), "#000000FF")
    check_pixel(self, black_layer.drawbuf.get_pixel(101,101), "#000000FF")
  
  def test_duplicate(self):
    layer = layers.Layer(width=100,height=100)
    layer.drawbuf.fill(color=make_color("#AABBCCFF"))
    dup_layer = layer.duplicate()
    
    check_pixel(self, layer.drawbuf.get_pixel(0,0), "#AABBCCFF")
    check_pixel(self, dup_layer.drawbuf.get_pixel(0,0), "#AABBCCFF")
    
    layer.drawbuf.fill(color=make_color("#FFFFFFFF"))
    dup_layer.drawbuf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, layer.drawbuf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, dup_layer.drawbuf.get_pixel(0,0), "#000000FF")

  def test_set_opacity(self):
    layer = layers.Layer(width=100,height=100)
    self.assertEqual(layer.opacity, 1.0)
    layer.set_property("opacity", 0.0)
    self.assertEqual(layer.opacity, 0.0)
    layer.set_property("opacity", 50.0)
    self.assertEqual(layer.opacity, 0.5)
    layer.set_opacity(1.0)
    self.assertEqual(layer.opacity, 1.0)
    layer.set_opacity(0.0)
    self.assertEqual(layer.opacity, 0.0)
    layer.set_opacity(0.5)
    self.assertEqual(layer.opacity, 0.5)

  def test_set_mode(self):
    #print()
    layer = layers.Layer(width=100,height=100)
    self.assertIs(layer.mode, layers.LayerModes.NORMAL)
    
    for test_mode in layers.LayerModes.valid:
      #print("Setting mode to %s" % test_mode)
      layer.set_mode(test_mode)
      self.assertIs(layer.mode, test_mode)
    
    layer.set_mode(layers.LayerModes.NORMAL)
    with self.assertRaises(ValueError):
      layer.set_mode(None)
    self.assertIs(layer.mode, layers.LayerModes.NORMAL)
    layer.set_mode(layers.LayerModes.NORMAL)
    self.assertIs(layer.mode, layers.LayerModes.NORMAL)

  def test_set_mode_with_graph(self):
    #print()
    layer = layers.Layer(width=100,height=100)
    self.assertIs(layer.mode, layers.LayerModes.NORMAL)
    
    ptn = Gegl.Node()
    graph_node = layer.get_graph(ptn)
    
    for test_mode in layers.LayerModes.valid:
      #print("Setting mode to %s" % test_mode)
      layer.set_mode(test_mode)
      self.assertIs(layer.mode, test_mode)
      # It would be better to check for an exact name, but the
      # alias we use may not match the final operation gegl picks.
      # So this check only verifies that gegl considers the mode's
      # name to be a valid operation.
      op_name = graph_node.get_operation()
      #print(" ", op_name)
      self.assertNotEqual(op_name, "gegl:nop")
    
    layer.set_mode(layers.LayerModes.NORMAL)
    self.assertNotEqual(graph_node.get_operation(), "gegl:nop")
    with self.assertRaises(ValueError):
      layer.set_mode(None)
    self.assertIs(layer.mode, layers.LayerModes.NORMAL)
    self.assertNotEqual(graph_node.get_operation(), "gegl:nop")
    layer.set_mode(layers.LayerModes.NORMAL)
    self.assertIs(layer.mode, layers.LayerModes.NORMAL)
    self.assertNotEqual(graph_node.get_operation(), "gegl:nop")

if __name__ == '__main__':
    unittest.main(verbosity=10)
