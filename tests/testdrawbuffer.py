#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import prep
prep.init()

import unittest
import drawbuffer
import cairo

#OK_STR ="\033[01;32mOK!\033[00m"
#ERROR_STR ="\033[01;31mERROR!\033[00m"

from testcommon import make_color, check_pixel


class TestDrawBuffer(unittest.TestCase):
  def test_drawbuffer_init(self):
    ops_buf = drawbuffer.DrawBuffer(10,10)
    
    self.assertEqual(ops_buf.width, 10)
    self.assertEqual(ops_buf.height, 10)
    
    extent = ops_buf.get_gegl().get_properties("x", "y", "width", "height")
    self.assertEqual(extent, (0, 0, 10, 10))
    self.assertEqual(ops_buf.get_gegl().get_property("pixels"), 10*10)
    self.assertEqual(ops_buf.get_pixel(0,0), (0.0, 0.0, 0.0, 0.0))

  def test_drawbuffer_init_fill(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AABBCCFF"))
    
    pixel = ops_buf.get_pixel(0,0)
    check_pixel(self, pixel, "#AABBCCFF")
    #self.assertAlmostEqual(pixel[0], int(0xAA) / 255.0)
    #self.assertAlmostEqual(pixel[1], int(0xBB) / 255.0)
    #self.assertAlmostEqual(pixel[2], int(0xCC) / 255.0)
    #self.assertAlmostEqual(pixel[3], int(0xFF) / 255.0)
    
  def xxxtest_drawbuffer_init_fill_list(self):
    ops_buf = drawbuffer.DrawBuffer(10, 10, color=(int(0xAA) / 255.0,
                                                   int(0xBB) / 255.0,
                                                   int(0xCC) / 255.0,
                                                   int(0xFF) / 255.0))
    pixel = ops_buf.get_pixel(0,0)
    check_pixel(self, pixel, "#AABBCCFF")

class TestDrawBufferBlit(unittest.TestCase):
  def setUp(self):
    pass

  def test_drawbuffer_blit_to_none(self):
    ops_buf = drawbuffer.DrawBuffer(10,10)
    with self.assertRaises(TypeError):
      ops_buf.blit_to(None)

  def test_drawbuffer_blit_to(self):  
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AAAAAAAA"))
    dst_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#BBBBBBBB"))
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    
    ops_buf.blit_to(dst_buf)
    check_pixel(self, dst_buf.get_pixel(0,0), "#AAAAAAAA")

  def test_drawbuffer_blit_to_smaller(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AAAAAAAA"))
    dst_buf = drawbuffer.DrawBuffer(5,5,color=make_color("#BBBBBBBB"))
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    ops_buf.blit_to(dst_buf)
    check_pixel(self, dst_buf.get_pixel(0,0), "#AAAAAAAA")

  def test_drawbuffer_blit_to_offset(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AAAAAAAA"))
    dst_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#BBBBBBBB"))
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    ops_buf.blit_to(dst_buf, 5, 5)
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    check_pixel(self, dst_buf.get_pixel(9,9), "#AAAAAAAA")

  def test_drawbuffer_blit_to_larger(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AAAAAAAA"))
    dst_buf = drawbuffer.DrawBuffer(20,20,color=make_color("#BBBBBBBB"))
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    ops_buf.blit_to(dst_buf, 10, 10)
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    check_pixel(self, dst_buf.get_pixel(19,19), "#AAAAAAAA")

  def test_drawbuffer_blit_to_rect(self):  
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AAAAAAAA"))
    dst_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#BBBBBBBB"))
    ops_buf.blit_to(dst_buf, 1, 1, (0,0,8,8))
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    check_pixel(self, dst_buf.get_pixel(1,1), "#AAAAAAAA")
    check_pixel(self, dst_buf.get_pixel(8,8), "#AAAAAAAA")
    check_pixel(self, dst_buf.get_pixel(9,9), "#BBBBBBBB")

  def test_drawbuffer_blit_to_rect2(self):  
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#AAAAAAAA"))
    dst_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#BBBBBBBB"))
    ops_buf.blit_to(dst_buf, 1, 1, (1,1,9,9))
    check_pixel(self, dst_buf.get_pixel(0,0), "#BBBBBBBB")
    check_pixel(self, dst_buf.get_pixel(1,1), "#AAAAAAAA")
    check_pixel(self, dst_buf.get_pixel(8,8), "#AAAAAAAA")
    check_pixel(self, dst_buf.get_pixel(9,9), "#AAAAAAAA")

  def test_drawbuffer_blit_cairo_opaque(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#804020FF"))
    dst_surf = cairo.ImageSurface(cairo.FORMAT_ARGB32, 10, 10)
    ops_buf.blit_to_cairo_image(dst_surf)
    
    dst_data = dst_surf.get_data()
    a = [int(ord(i)) for i in dst_data[:4]]
    b = [32, 64, 128, 255]
    #print(a, "==", b)
    self.assertEqual(a, b)
  
  def test_drawbuffer_blit_cairo_alpha(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#80402080"))
    dst_surf = cairo.ImageSurface(cairo.FORMAT_ARGB32, 10, 10)
    ops_buf.blit_to_cairo_image(dst_surf)
    
    dst_data = dst_surf.get_data()
    a = [int(ord(i)) for i in dst_data[:4]]
    b = [16, 32, 64, 128]
    #print(a, "==", b)
    self.assertEqual(a, b)
  
  def test_drawbuffer_blit_cairo_bad_format(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#80402080"))
    dst_surf = cairo.ImageSurface(cairo.FORMAT_RGB24, 10, 10)
    
    with self.assertRaises(ValueError):
      ops_buf.blit_to_cairo_image(dst_surf)

class TestDrawBufferDraw(unittest.TestCase):
  def test_drawbuffer_draw_circle(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#ABABAB00"))
    ops_buf.circle(5, 5, 3)
    check_pixel(self, ops_buf.get_pixel(0,0), "#ABABAB00")
    check_pixel(self, ops_buf.get_pixel(1,1), "#ABABAB00")
    check_pixel(self, ops_buf.get_pixel(2,2), "#ABABAB00")
    
    # 3, 3 Will have some alpha to it, we don't care about the exact value
    pixel = ops_buf.get_pixel(3,3)
    self.assertEqual(pixel[0], 0)
    self.assertEqual(pixel[1], 0)
    self.assertEqual(pixel[2], 0)
    self.assertGreater(pixel[3], 0)
    
    check_pixel(self, ops_buf.get_pixel(4,4), "#000000FF")
    check_pixel(self, ops_buf.get_pixel(5,5), "#000000FF")
    check_pixel(self, ops_buf.get_pixel(6,6), "#000000FF")
    
    # 7, 7 Will have some alpha to it, we don't care about the exact value
    pixel = ops_buf.get_pixel(7,7)
    self.assertEqual(pixel[0], 0)
    self.assertEqual(pixel[1], 0)
    self.assertEqual(pixel[2], 0)
    self.assertGreater(pixel[3], 0)
    
    check_pixel(self, ops_buf.get_pixel(8,8), "#ABABAB00")
    check_pixel(self, ops_buf.get_pixel(9,9), "#ABABAB00")

  def test_drawbuffer_draw_circle_color(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#FFFFFFFF"))
    ops_buf.circle(5, 5, 3, color=make_color("#AABBCCFF"))
    check_pixel(self, ops_buf.get_pixel(5,5), "#AABBCCFF")
    
  def test_drawbuffer_draw_circle_color_list(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#FFFFFFFF"))
    ops_buf.circle(5, 5, 3, color=(0.667, 0.735, 0.800, 1.000))
    
    pixel = ops_buf.get_pixel(5,5)
    self.assertAlmostEqual(pixel[0], 0.667, 3)
    self.assertAlmostEqual(pixel[1], 0.735, 3)
    self.assertAlmostEqual(pixel[2], 0.800, 3)
    self.assertAlmostEqual(pixel[3], 1.000, 3)

  def test_drawbuffer_draw_fill(self):
    ops_buf = drawbuffer.DrawBuffer(10,10,color=make_color("#FFFFFFFF"))
    ops_buf.fill(color=make_color("#AABBCCFF"))
    
    check_pixel(self, ops_buf.get_pixel(0,0), "#AABBCCFF")
    check_pixel(self, ops_buf.get_pixel(5,5), "#AABBCCFF")
    
    ops_buf.fill(2,2,color=make_color("#112233FF"))
    check_pixel(self, ops_buf.get_pixel(0,0), "#AABBCCFF")
    check_pixel(self, ops_buf.get_pixel(2,2), "#112233FF")
    
    ops_buf.fill(color=make_color("#AABBCCFF"))
    ops_buf.fill(w=2,h=2,color=make_color("#112233FF"))
    check_pixel(self, ops_buf.get_pixel(0,0), "#112233FF")
    check_pixel(self, ops_buf.get_pixel(2,2), "#AABBCCFF")


class TestDrawBufferDuplicate(unittest.TestCase):
  def test_drawbuffer_duplicate(self):
    ops_buf = drawbuffer.DrawBuffer(10,10)
    ops_buf.fill(color=make_color("#AABBCCFF"))
    dup_buf = ops_buf.duplicate()
    
    check_pixel(self, ops_buf.get_pixel(0,0), "#AABBCCFF")
    check_pixel(self, dup_buf.get_pixel(0,0), "#AABBCCFF")
    
    ops_buf.fill(color=make_color("#FFFFFFFF"))
    dup_buf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, ops_buf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, dup_buf.get_pixel(0,0), "#000000FF")
  
  def test_drawbuffer_duplicate_tiles(self):
    ops_buf = drawbuffer.DrawBuffer(10,10)
    ops_buf.fill(color=make_color("#AABBCCFF"))
    dup_buf = ops_buf.duplicate_tiles()
    
    check_pixel(self, ops_buf.get_pixel(0,0), "#AABBCCFF")
    check_pixel(self, dup_buf.get_pixel(0,0), "#AABBCCFF")
    
    ops_buf.fill(color=make_color("#FFFFFFFF"))
    dup_buf.fill(color=make_color("#000000FF"))
    
    check_pixel(self, ops_buf.get_pixel(0,0), "#FFFFFFFF")
    check_pixel(self, dup_buf.get_pixel(0,0), "#000000FF")


if __name__ == '__main__':
    unittest.main(verbosity=10)
