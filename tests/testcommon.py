import drawbuffer

def make_color(color_str):
  test_pixel = [color_str[i:i+2] for i in range(1, 9, 2)]
  test_pixel = [int(i, 16) / 255.0 for i in test_pixel]
  return tuple(list(drawbuffer.srgb_to_rgb(test_pixel[:3])) + [test_pixel[3]])

def check_pixel(test, pixel, color_str):
  pixel = list(drawbuffer.rgb_to_srgb(pixel[0:3])) + [pixel[3]]
  pixel = [int(round(i * 0xFFFF)) for i in pixel]

  test_pixel = [color_str[i:i+2] for i in range(1, 9, 2)]
  test_pixel = [int(i, 16) / 255.0 for i in test_pixel]
  test_pixel = [int(round(i * 0xFFFF)) for i in test_pixel]

  test.assertEqual(pixel, test_pixel)