#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import prep
prep.init()

import unittest
import thedraw
import layers

import os
import tempfile
import drawbuffer
import struct

from testcommon import make_color, check_pixel

def png_peek(filename):
  """Read a PNG file's header to find it's bit depth and other data"""
  with open(filename) as f:
    data = f.read(12+17)
    if not data[:8] == "\x89PNG\r\n\x1a\n":
      # Not a PNG file
      return None
    # The IHDR chunk is guranteed to be first, so we just assume it's
    # offset instead of reading all the chunks
    image_header = struct.unpack("!4s2i5B", data[12:(12+17)])
    if not image_header[0] == "IHDR":
      return None
    return {"width": image_header[1], "height": image_header[2],
            "depth": image_header[3], "color-type": image_header[4]}

class CallbackCounter():
  def __init__(self):
    self.notify_count = 0
  
  def notify_callback(self, obj, prop):
    self.notify_count += 1

class TestDrawCanvas_000(unittest.TestCase):
  def test_init(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertIsNotNone(canvas_widget.get_property("layers"))
    self.assertEqual(canvas_widget.get_property("scale"), 1.0)
    
    self.assertEqual(len(canvas_widget.layers), 1)
    layer_a = canvas_widget.layers[0]
    self.assertEqual(layer_a.drawbuf.width, 0)
    self.assertEqual(layer_a.drawbuf.height, 0)
    
  def test_new_drawing(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertEqual(len(canvas_widget.layers), 1)
    layer_a = canvas_widget.layers[0]
    self.assertEqual(layer_a.drawbuf.width, 0)
    self.assertEqual(layer_a.drawbuf.height, 0)
    
    canvas_widget.new_drawing()
    
    self.assertEqual(len(canvas_widget.layers), 1)
    layer_b = canvas_widget.layers[0]
    self.assertIsNot(layer_a, layer_b)
    self.assertEqual(layer_b.drawbuf.width, 0)
    self.assertEqual(layer_b.drawbuf.height, 0)

  @unittest.skip("TODO")
  def test_new_drawing_from_png(self):
    pass
    
  @unittest.skip("TODO")
  def test_new_drawing_from_ora(self):
    pass

class TestDrawCanvas_100_Layers(unittest.TestCase):
  def test_add_layer(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertIsNotNone(canvas_widget.layers)
    self.assertEqual(len(canvas_widget.layers), 1)
    
    layer_a = canvas_widget.layers[0]
    layer_b = None
    layer_c = None
    layer_d = None
    
    counter = CallbackCounter()
    
    canvas_widget.connect("notify::layers", counter.notify_callback)
    
    canvas_widget.new_layer(0)
    self.assertEqual(len(canvas_widget.layers), 2)
    self.assertEqual(counter.notify_count, 1)

    # Check ordering
    self.assertIs(layer_a, canvas_widget.layers[0])
    layer_b = canvas_widget.layers[1]
    
    canvas_widget.new_layer(0)
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertEqual(counter.notify_count, 2)
    
    # Check ordering
    self.assertIs(layer_a, canvas_widget.layers[0])
    layer_c = canvas_widget.layers[1]
    self.assertIs(layer_b, canvas_widget.layers[2])

    canvas_widget.new_layer(2)
    self.assertEqual(len(canvas_widget.layers), 4)
    self.assertEqual(counter.notify_count, 3)
    
    # Check ordering
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    layer_d = canvas_widget.layers[3]
    
    # Test undo
    canvas_widget.undo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    self.assertEqual(counter.notify_count, 4)
    
    canvas_widget.undo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertEqual(counter.notify_count, 5)
    
    canvas_widget.undo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertEqual(counter.notify_count, 6)
    
    canvas_widget.undo() # Extra undo off the end of the stack
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertEqual(counter.notify_count, 6)
    
    # Test redo
    canvas_widget.redo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertEqual(counter.notify_count, 7)
    
    canvas_widget.redo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    self.assertEqual(counter.notify_count, 8)
    
    canvas_widget.redo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    self.assertIs(layer_d, canvas_widget.layers[3])
    self.assertEqual(counter.notify_count, 9)
    
    canvas_widget.redo() # Extra redo off the end of the stack
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    self.assertIs(layer_d, canvas_widget.layers[3])
    self.assertEqual(counter.notify_count, 9)

  def test_del_layer(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertIsNotNone(canvas_widget.layers)
    self.assertEqual(len(canvas_widget.layers), 1)
    
    counter = CallbackCounter()
    canvas_widget.connect("notify::layers", counter.notify_callback)
    
    canvas_widget.new_layer(0)
    canvas_widget.new_layer(1)
    self.assertEqual(counter.notify_count, 2)
    
    layer_a = canvas_widget.layers[0]
    layer_b = canvas_widget.layers[1]
    layer_c = canvas_widget.layers[2]
    
    canvas_widget.del_layer(layer_b)
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertEqual(counter.notify_count, 3)
    
    canvas_widget.undo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertIs(layer_c, canvas_widget.layers[2])
    self.assertEqual(counter.notify_count, 4)
    
    canvas_widget.redo()
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertEqual(counter.notify_count, 5)

  def test_move_layer(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertIsNotNone(canvas_widget.layers)
    self.assertEqual(len(canvas_widget.layers), 1)
    
    counter = CallbackCounter()
    canvas_widget.connect("notify::layers", counter.notify_callback)
    
    canvas_widget.new_layer(0)
    canvas_widget.new_layer(1)
    self.assertEqual(counter.notify_count, 2)
    
    layer_a = canvas_widget.layers[0]
    layer_b = canvas_widget.layers[1]
    layer_c = canvas_widget.layers[2]
    
    canvas_widget.move_layer(layer_c, 0)
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_c, canvas_widget.layers[0])
    self.assertIs(layer_a, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    
    canvas_widget.move_layer(layer_b, 1)
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_c, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertIs(layer_a, canvas_widget.layers[2])
    
    canvas_widget.move_layer(layer_c, 3)
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_b, canvas_widget.layers[0])
    self.assertIs(layer_a, canvas_widget.layers[1])
    self.assertIs(layer_c, canvas_widget.layers[2])
    
    canvas_widget.move_layer(layer_a, -5)
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertIs(layer_c, canvas_widget.layers[2])
    
    canvas_widget.move_layer(layer_b, 99)
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_c, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    
    self.assertEqual(counter.notify_count, 7)
    canvas_widget.undo()
    canvas_widget.undo()
    canvas_widget.undo()
    canvas_widget.undo()
    canvas_widget.undo()
    self.assertEqual(counter.notify_count, 12)
    
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertIs(layer_c, canvas_widget.layers[2])
    
    canvas_widget.redo()
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertIs(layer_c, canvas_widget.layers[0])
    self.assertIs(layer_a, canvas_widget.layers[1])
    self.assertIs(layer_b, canvas_widget.layers[2])
    
    self.assertEqual(counter.notify_count, 13)
    
  def test_merge_layer_down(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertIsNotNone(canvas_widget.layers)
    self.assertEqual(len(canvas_widget.layers), 1)
    
    counter = CallbackCounter()
    canvas_widget.connect("notify::layers", counter.notify_callback)
    
    canvas_widget.new_layer(0)
    canvas_widget.new_layer(1)
    self.assertEqual(counter.notify_count, 2)
    
    layer_a = canvas_widget.layers[0]
    layer_b = canvas_widget.layers[1]
    layer_c = canvas_widget.layers[2]
    
    layer_a_name = layer_a.name
    layer_b_name = layer_b.name
    
    canvas_widget.merge_down_layer(layer_c)
    self.assertEqual(len(canvas_widget.layers), 2)
    self.assertEqual(canvas_widget.layers[1].name, layer_b_name)
    self.assertEqual(counter.notify_count, 3)
    
    canvas_widget.merge_down_layer(canvas_widget.layers[1])
    self.assertEqual(len(canvas_widget.layers), 1)
    self.assertEqual(canvas_widget.layers[0].name, layer_a_name)
    self.assertEqual(counter.notify_count, 4)
    
    canvas_widget.merge_down_layer(canvas_widget.layers[0])
    self.assertEqual(len(canvas_widget.layers), 1)
    self.assertEqual(counter.notify_count, 4)
    
    canvas_widget.undo()
    self.assertEqual(len(canvas_widget.layers), 2)
    self.assertEqual(counter.notify_count, 5)
    canvas_widget.undo()
    self.assertEqual(len(canvas_widget.layers), 3)
    self.assertEqual(counter.notify_count, 6)
    self.assertIs(layer_a, canvas_widget.layers[0])
    self.assertIs(layer_b, canvas_widget.layers[1])
    self.assertIs(layer_c, canvas_widget.layers[2])

  def test_layer_size(self):
    canvas_widget = thedraw.DrawCanvas()
    
    self.assertIsNotNone(canvas_widget.layers)
    self.assertEqual(len(canvas_widget.layers), 1)

    self.assertEqual(canvas_widget.layers[0].x, 0)
    self.assertEqual(canvas_widget.layers[0].y, 0)
    self.assertEqual(canvas_widget.layers[0].width, 0)
    self.assertEqual(canvas_widget.layers[0].height, 0)
    
    canvas_widget.new_layer(0)
    self.assertEqual(canvas_widget.layers[1].x, 0)
    self.assertEqual(canvas_widget.layers[1].y, 0)
    self.assertEqual(canvas_widget.layers[1].width, 0)
    self.assertEqual(canvas_widget.layers[1].height, 0)

    # Reset
    canvas_widget.undo()
    self.assertIsNotNone(canvas_widget.layers)
    self.assertEqual(len(canvas_widget.layers), 1)

    canvas_widget.layers[0].drawbuf.set_extent(-50, -50, 100, 100)

    self.assertEqual(canvas_widget.layers[0].x, -50)
    self.assertEqual(canvas_widget.layers[0].y, -50)
    self.assertEqual(canvas_widget.layers[0].width, 100)
    self.assertEqual(canvas_widget.layers[0].height, 100)
    
    canvas_widget.new_layer(0)
    self.assertEqual(canvas_widget.layers[1].x, 0)
    self.assertEqual(canvas_widget.layers[1].y, 0)
    self.assertEqual(canvas_widget.layers[1].width, 0)
    self.assertEqual(canvas_widget.layers[1].height, 0)

    
class TestDrawCanvas_200_Save(unittest.TestCase):
  def test_save_ora(self):
    width  = 100
    height = 100
    
    canvas_widget = thedraw.DrawCanvas()
    canvas_widget._new_drawing_from_layers([layers.Layer(width=width, height=height)])
    
    filename = tempfile.mktemp(suffix=".ora")
    print()
    print("Saving to", filename)
    canvas_widget.save_drawing_to_ora(filename)
    self.assertTrue(os.path.isfile(filename))
    try:
      second_canvas_widget = thedraw.DrawCanvas()
      second_canvas_widget.new_drawing_from_filename(filename)
      self.assertEqual(second_canvas_widget.drawing_width, width)
      self.assertEqual(second_canvas_widget.drawing_height, height)
      self.assertIsNot(canvas_widget.layers[0], second_canvas_widget.layers[0])
      self.assertEqual(len(second_canvas_widget.layers), 1)
      self.assertEqual(second_canvas_widget.layers[0].drawbuf.width, width)
      self.assertEqual(second_canvas_widget.layers[0].drawbuf.height, height)
    finally:
      os.remove(filename)
      print("Deleted", filename)
    
  def test_save_png_with_background(self):
    width  = 100
    height = 100
    
    canvas_widget = thedraw.DrawCanvas()
    canvas_widget._new_drawing_from_layers([layers.Layer(width=width, height=height)])
    
    filename = tempfile.mktemp(suffix=".png")
    print()
    print("Saving to", filename)
    canvas_widget.save_drawing_to_png(filename, with_background=True)
    self.assertTrue(os.path.isfile(filename))
    try:
      print("Loading", filename)
      loaded_drawbuffer = drawbuffer.drawbuffer_from_png(filename)
      self.assertIsNotNone(loaded_drawbuffer)
      self.assertEqual(loaded_drawbuffer.width, width)
      self.assertEqual(loaded_drawbuffer.height, height)
    finally:
      os.remove(filename)
      print("Deleted", filename)
    
  def test_save_png(self):
    width  = 100
    height = 100
    
    canvas_widget = thedraw.DrawCanvas()
    canvas_widget._new_drawing_from_layers([layers.Layer(width=width, height=height)])
    
    filename = tempfile.mktemp(suffix=".png")
    print()
    print("Saving to", filename)
    canvas_widget.save_drawing_to_png(filename, with_background=False)
    self.assertTrue(os.path.isfile(filename))
    try:
      print("Loading", filename)
      peek = png_peek(filename)
      self.assertEqual(peek["width"], width)
      self.assertEqual(peek["width"], height)
      self.assertEqual(peek["depth"], 16)
      
      loaded_drawbuffer = drawbuffer.drawbuffer_from_png(filename)
      self.assertIsNotNone(loaded_drawbuffer)
      self.assertEqual(loaded_drawbuffer.width, width)
      self.assertEqual(loaded_drawbuffer.height, height)
    finally:
      os.remove(filename)
      print("Deleted", filename)
  
  def test_save_png_8bit(self):
    width  = 100
    height = 100
    
    canvas_widget = thedraw.DrawCanvas()
    canvas_widget._new_drawing_from_layers([layers.Layer(width=width, height=height)])
    
    filename = tempfile.mktemp(suffix=".png")
    print()
    print("Saving to", filename)
    canvas_widget.save_drawing_to_png(filename, sixteen_bit=False, with_background=False)
    self.assertTrue(os.path.isfile(filename))
    try:
      print("Loading", filename)
      peek = png_peek(filename)
      self.assertEqual(peek["width"], width)
      self.assertEqual(peek["width"], height)
      self.assertEqual(peek["depth"], 8)
      
      loaded_drawbuffer = drawbuffer.drawbuffer_from_png(filename)
      self.assertIsNotNone(loaded_drawbuffer)
      self.assertEqual(loaded_drawbuffer.width, width)
      self.assertEqual(loaded_drawbuffer.height, height)
    finally:
      os.remove(filename)
      print("Deleted", filename)
  
  def test_save_png_8bit_with_background(self):
    width  = 100
    height = 100
    
    canvas_widget = thedraw.DrawCanvas()
    canvas_widget._new_drawing_from_layers([layers.Layer(width=width, height=height)])
    
    filename = tempfile.mktemp(suffix=".png")
    print()
    print("Saving to", filename)
    canvas_widget.save_drawing_to_png(filename, sixteen_bit=False, with_background=True)
    self.assertTrue(os.path.isfile(filename))
    try:
      print("Loading", filename)
      loaded_drawbuffer = drawbuffer.drawbuffer_from_png(filename)
      self.assertIsNotNone(loaded_drawbuffer)
      self.assertEqual(loaded_drawbuffer.width, width)
      self.assertEqual(loaded_drawbuffer.height, height)
    finally:
      os.remove(filename)
      print("Deleted", filename)

class TestDrawCanvas_400_Undo(unittest.TestCase):
  @unittest.skip("takes a very long time")
  def test_max_undo_default(self):
    canvas_widget = thedraw.DrawCanvas()
    undo_limit = canvas_widget.get_property("max-undo-steps")
    
    for i in range(5):
      canvas_widget.new_layer(0)
    expected_layers = canvas_widget.layers
    for i in range(undo_limit):
      canvas_widget.new_layer(0)
    
    self.assertEqual(len(canvas_widget.undo_history), undo_limit)

    for i in range(undo_limit):
      canvas_widget.undo()
    self.assertEqual(len(canvas_widget.undo_history), 0)
    
    self.assertEqual(expected_layers, canvas_widget.layers)
    
  def test_max_undo(self):
    canvas_widget = thedraw.DrawCanvas()
    canvas_widget.set_property("max-undo-steps", 10)
    counter = CallbackCounter()
    
    canvas_widget.connect("notify::layers", counter.notify_callback)
    
    canvas_widget.new_layer(0)
    self.assertEqual(len(canvas_widget.layers), 2)
    self.assertEqual(counter.notify_count, 1)
    
    canvas_widget.undo()
    self.assertEqual(counter.notify_count, 2)
    
    for i in range(5):
      canvas_widget.new_layer(0)
    expected_layers = canvas_widget.layers
    for i in range(5):
      canvas_widget.new_layer(0)
    self.assertEqual(counter.notify_count, 12)
    self.assertEqual(len(canvas_widget.undo_history), 10)
    
    for i in range(5):
      canvas_widget.new_layer(0)
    self.assertEqual(counter.notify_count, 17)
    self.assertEqual(len(canvas_widget.undo_history), 10)

    for i in range(10):
      canvas_widget.undo()
    self.assertEqual(counter.notify_count, 27)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    
    self.assertEqual(expected_layers, canvas_widget.layers)
    
    for i in range(10):
      canvas_widget.undo()
    self.assertEqual(counter.notify_count, 27)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    
    self.assertEqual(expected_layers, canvas_widget.layers)
    
  def test_max_undo_truncate(self):
    canvas_widget = thedraw.DrawCanvas()
    counter = CallbackCounter()
    
    canvas_widget.connect("notify::layers", counter.notify_callback)
    
    canvas_widget.new_layer(0)
    self.assertEqual(len(canvas_widget.layers), 2)
    self.assertEqual(counter.notify_count, 1)
    
    canvas_widget.undo()
    self.assertEqual(counter.notify_count, 2)
    
    for i in range(5):
      canvas_widget.new_layer(0)
    expected_layers = canvas_widget.layers
    for i in range(10):
      canvas_widget.new_layer(0)
    self.assertEqual(counter.notify_count, 17)
    self.assertEqual(len(canvas_widget.undo_history), 15)
    
    canvas_widget.set_property("max-undo-steps", 10)
    self.assertEqual(len(canvas_widget.undo_history), 10)
    
    for i in range(10):
      canvas_widget.undo()
    self.assertEqual(counter.notify_count, 27)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    
    self.assertEqual(expected_layers, canvas_widget.layers)

    for i in range(10):
      canvas_widget.undo()
    self.assertEqual(counter.notify_count, 27)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    
    self.assertEqual(expected_layers, canvas_widget.layers)

class TestDrawCanvas_500_LayerProp(unittest.TestCase):
  def test_set_layer_name(self):
    canvas_widget = thedraw.DrawCanvas()
    new_name = "NewName"
    original_name = canvas_widget.layers[0].get_property("name")
    
    self.assertNotEqual(original_name, new_name)
    
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "name", new_name)
    self.assertEqual(canvas_widget.layers[0].get_property("name"), new_name)
    self.assertEqual(len(canvas_widget.undo_history), 1)
    canvas_widget.undo()
    self.assertEqual(canvas_widget.layers[0].get_property("name"), original_name)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    canvas_widget.redo()
    self.assertEqual(canvas_widget.layers[0].get_property("name"), new_name)
    self.assertEqual(len(canvas_widget.undo_history), 1)
    
  def test_set_layer_opacity(self):
    canvas_widget = thedraw.DrawCanvas()
    new_opacity = 0.5
    original_opacity = canvas_widget.layers[0].get_property("opacity")
    
    self.assertNotEqual(original_opacity, new_opacity)
    
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "opacity", new_opacity)
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), new_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 1)
    canvas_widget.undo()
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), original_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    canvas_widget.redo()
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), new_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 1)
    
  def test_set_layer_opacity_fold(self):
    canvas_widget = thedraw.DrawCanvas()
    new_opacity = 0.5
    original_opacity = canvas_widget.layers[0].get_property("opacity")
    
    self.assertNotEqual(original_opacity, new_opacity)
    
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "opacity", 0.25)
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "opacity", new_opacity)
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), new_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 1)
    canvas_widget.undo()
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), original_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 0)
    canvas_widget.redo()
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), new_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 1)
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "opacity", 0.25)
    self.assertEqual(len(canvas_widget.undo_history), 2)
    canvas_widget.undo()
    canvas_widget.undo()
    self.assertEqual(len(canvas_widget.undo_history), 0)
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "opacity", 0.25)
    
    # Fake a time delay
    canvas_widget.undo_history[-1].timestamp -= 1.0
    
    canvas_widget.set_layer_prop(canvas_widget.current_layer, "opacity", new_opacity)
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), new_opacity)
    self.assertEqual(len(canvas_widget.undo_history), 2)

    canvas_widget.undo()
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), 0.25)
    canvas_widget.undo()
    self.assertEqual(canvas_widget.layers[0].get_property("opacity"), original_opacity)

if __name__ == '__main__':
    unittest.main(verbosity=10)
