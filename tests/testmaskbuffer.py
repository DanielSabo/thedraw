#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import prep
prep.init()

import unittest
import drawbuffer

#OK_STR ="\033[01;32mOK!\033[00m"
#ERROR_STR ="\033[01;31mERROR!\033[00m"

class TestMaskBuffer(unittest.TestCase):
  def test_drawbuffer_scale_with_size(self):
    mask = drawbuffer.MaskBuffer(2, 2)
    mask.set_pixel(0, 0, 0.25)
    mask.set_pixel(0, 1, 0.50)
    mask.set_pixel(1, 0, 0.75)
    mask.set_pixel(1, 1, 1.00)
    
    scaled = mask.scale_with_size(0, 0, 2, 2)
    self.assertEqual(scaled.width, 2)
    self.assertEqual(scaled.height, 2)
    self.assertAlmostEqual(scaled.get_pixel(0, 0), 0.25)
    self.assertAlmostEqual(scaled.get_pixel(0, 1), 0.50)
    self.assertAlmostEqual(scaled.get_pixel(1, 0), 0.75)
    self.assertAlmostEqual(scaled.get_pixel(1, 1), 1.00)

    scaled = mask.scale_with_size(1, 0, 2, 2)
    self.assertEqual(scaled.width, 3)
    self.assertEqual(scaled.height, 2)
    self.assertAlmostEqual(scaled.get_pixel(0, 0), 0.00)
    self.assertAlmostEqual(scaled.get_pixel(0, 1), 0.00)
    self.assertAlmostEqual(scaled.get_pixel(1, 0), 0.25)
    self.assertAlmostEqual(scaled.get_pixel(1, 1), 0.50)
    self.assertAlmostEqual(scaled.get_pixel(2, 0), 0.75)
    self.assertAlmostEqual(scaled.get_pixel(2, 1), 1.00)

    scaled = mask.scale_with_size(0, 1, 2, 2)
    self.assertEqual(scaled.width, 2)
    self.assertEqual(scaled.height, 3)
    self.assertAlmostEqual(scaled.get_pixel(0, 0), 0.00)
    self.assertAlmostEqual(scaled.get_pixel(1, 0), 0.00)
    self.assertAlmostEqual(scaled.get_pixel(0, 1), 0.25)
    self.assertAlmostEqual(scaled.get_pixel(0, 2), 0.50)
    self.assertAlmostEqual(scaled.get_pixel(1, 1), 0.75)
    self.assertAlmostEqual(scaled.get_pixel(1, 2), 1.00)
    
    with self.assertRaises(ValueError):
      mask.scale_with_size(-0.1, 0, 2, 2)
    with self.assertRaises(ValueError):
      mask.scale_with_size(-0.1, -0.5, 2, 2)
    with self.assertRaises(ValueError):
      mask.scale_with_size(0, -1, 2, 2)
    with self.assertRaises(ValueError):
      mask.scale_with_size(0, 0, 0, 2)
    with self.assertRaises(ValueError):
      mask.scale_with_size(0, 0, 2, 0)
    with self.assertRaises(ValueError):
      mask.scale_with_size(0, 0, -2, 2)
    with self.assertRaises(ValueError):
      mask.scale_with_size(0, 0, 2, -2)
    
    scaled = mask.scale_with_size(0, 0, 3, 3)
    self.assertEqual(scaled.width, 3)
    self.assertEqual(scaled.height, 3)
    
    scaled = mask.scale_with_size(0.25, 0.25, 3, 3)
    self.assertEqual(scaled.width, 4)
    self.assertEqual(scaled.height, 4)
        
    scaled = mask.scale_with_size(0.75, 0.75, 3, 3)
    self.assertEqual(scaled.width, 4)
    self.assertEqual(scaled.height, 4)
        
    scaled = mask.scale_with_size(0.25, 0.25, 4, 4)
    self.assertEqual(scaled.width, 5)
    self.assertEqual(scaled.height, 5)
        
    scaled = mask.scale_with_size(0.75, 0.75, 4, 4)
    self.assertEqual(scaled.width, 5)
    self.assertEqual(scaled.height, 5)
    
    # Assert that the original buffer's data is unchanged
    self.assertEqual(mask.width, 2)
    self.assertEqual(mask.height, 2)
    self.assertAlmostEqual(mask.get_pixel(0, 0), 0.25)
    self.assertAlmostEqual(mask.get_pixel(0, 1), 0.50)
    self.assertAlmostEqual(mask.get_pixel(1, 0), 0.75)
    self.assertAlmostEqual(mask.get_pixel(1, 1), 1.00)


if __name__ == '__main__':
    unittest.main(verbosity=10)
