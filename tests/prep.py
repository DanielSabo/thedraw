import sys, os

thedraw_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.insert(0, thedraw_path)

import drawbuffer

from gi.repository import Gegl
Gegl.init(None)
Gegl.config().set_property("swap", "RAM")
Gegl.config().set_property("use-opencl", False)

Gegl.load_module_directory(os.path.join(thedraw_path, "geglop"))

drawbuffer.init()

def init():
  pass
