#!/usr/bin/env python
from __future__ import print_function

import drawbuffer
import timeit
import argparse
import sys

import cairo

import drawcanvas
import layers

from gi.repository import Gegl
Gegl.init(None)
Gegl.config().set_property("swap", "RAM")
Gegl.config().set_property("use-opencl", False)

drawbuffer.init()

Gegl.load_module_directory("geglop")

drawbuffer.init()

parser = argparse.ArgumentParser()
parser.add_argument("--aligned",
                    action="store_true")
parser.add_argument("--unaligned",
                    action="store_true")
parser.add_argument("--mode",
                    type=str,
                    action="store",
                    default="NORMAL",
                    metavar="layer_mode",
                    help="layer mode for the test layers")
parser.add_argument("--pause",
                    action="store_true")
args = parser.parse_args()

if args.pause:
  raw_input("Press enter to begin")

bench_aligned = args.aligned
bench_unaligned = args.unaligned

if not any([bench_aligned, bench_unaligned]):
  bench_aligned = True

layer_mode = args.mode.upper()
if not hasattr(layers.LayerModes, layer_mode):
  print("Invalid layer mode:", layer_mode)
  Gegl.exit()
  sys.exit(1)

layer_mode = layers.LayerModes.__dict__[layer_mode]

class CanvasBenchContext():
  def __init__(self, x, y, w, h, mode=layers.LayerModes.NORMAL):
    canvas_widget = drawcanvas.DrawCanvas()
    canvas_widget.new_layer(0)
    canvas_widget.new_layer(0)
    canvas_widget.new_layer(0)
    canvas_widget.new_layer(0)
    for l in canvas_widget.layers:
      l.drawbuf.set_extent(x, y, w, h)
    canvas_widget.layers[1].drawbuf.fill(color=(0.2, 0.2, 0.8, 0.5))
    canvas_widget.layers[2].drawbuf.fill(color=(0.2, 0.8, 0.2, 0.5))
    canvas_widget.layers[3].drawbuf.fill(color=(0.8, 0.2, 0.2, 0.5))
    canvas_widget.layers[1].set_mode(mode)
    canvas_widget.layers[2].set_mode(mode)
    canvas_widget.layers[3].set_mode(mode)
    canvas_widget.layers[4].set_mode(mode)
    self.canvas_widget = canvas_widget

    self.dirty_rect = [x, y, x+w, y+h]
    self.target_surface = cairo.ImageSurface(cairo.FORMAT_RGB24, w, h)
    self.target_surface.set_device_offset(-x, -y)

  def run(self):
    self.canvas_widget.dirty_rect = self.dirty_rect
    cr = cairo.Context(self.target_surface)
    self.canvas_widget.on_draw(None, cr)

  def __enter__(self):
    return self

  def __exit__(self, exc_type, exc_value, traceback):
    self.canvas_widget.destroy()
    self.canvas_widget = None

if bench_aligned:
  timeit_count = 1000
  print("process 256 x 256, tile aligned, x%d" % timeit_count)
  with CanvasBenchContext(0, 0, 256, 256, layer_mode) as bench:
    result = timeit.timeit(bench.run, number=timeit_count)
  print(" ", result)

if bench_unaligned:
  timeit_count = 1000
  print("process 256 x 256, unaligned, x%d" % timeit_count)
  with CanvasBenchContext(10, 10, 256, 256, layer_mode) as bench:
    result = timeit.timeit(bench.run, number=timeit_count)
  print(" ", result)

Gegl.exit()