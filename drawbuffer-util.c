#include <gegl.h>
#include <gegl-utils.h>
#include <pycairo.h>
#include <pygobject.h>

extern Pycairo_CAPI_t *Pycairo_CAPI;

PyObject*
gegl_buffer_copy_to_cairo (PyObject *self, PyObject *args)
{
  PyObject *buffer_obj;
  PycairoImageSurface *surface_obj;
  
  GeglRectangle roi = {0, 0, 0, 0};
  
  if (!PyArg_ParseTuple(args, "OO!|(iiii)", &buffer_obj, &PycairoImageSurface_Type, &surface_obj,
                                            &roi.x, &roi.y,
                                            &roi.width, &roi.height)) {
    return NULL;
  }
  
  GeglBuffer *buffer = GEGL_BUFFER(pygobject_get(buffer_obj));

  unsigned int stride = cairo_image_surface_get_stride (surface_obj->surface);
  unsigned char *data = cairo_image_surface_get_data (surface_obj->surface);
  //data += dst_roi.y * stride + dst_roi.x * 4;
  
  if (roi.width == 0)
    roi.width = gegl_buffer_get_extent(buffer)->width;

  if (roi.height == 0)
    roi.height = gegl_buffer_get_extent(buffer)->height;
  
  if (roi.width > cairo_image_surface_get_width(surface_obj->surface))
    roi.width = cairo_image_surface_get_width(surface_obj->surface);

  if (roi.height > cairo_image_surface_get_height(surface_obj->surface))
    roi.height = cairo_image_surface_get_height(surface_obj->surface);
    
  cairo_format_t surface_format = cairo_image_surface_get_format(surface_obj->surface);

  cairo_surface_flush(surface_obj->surface);
  
  //printf("Copied %d %d %d %d\n", roi.x, roi.y, roi.height, roi.width);

  if (CAIRO_FORMAT_ARGB32 == surface_format)
  {
    gegl_buffer_get(buffer,
                    &roi,
                    1.0,
                    babl_format ("cairo-ARGB32"),
                    data, stride,
                    GEGL_ABYSS_NONE);
  }
  else if (CAIRO_FORMAT_RGB24 == surface_format)
  {
    gegl_buffer_get(buffer,
                    &roi,
                    1.0,
                    babl_format ("cairo-RGB24"),
                    data, stride,
                    GEGL_ABYSS_NONE);
  }
  else
  {
    PyErr_SetString(PyExc_ValueError, "Unsupported surface format");
    return NULL;
  }
  
  cairo_surface_mark_dirty(surface_obj->surface);
  
  Py_RETURN_NONE;
}

