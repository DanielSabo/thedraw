from __future__ import print_function
from __future__ import division

import glob
import os
import drawbuffer
import json
import imp

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf

# Cover for an oddity of GTK Win32
if not hasattr(GdkPixbuf.Pixbuf, "new_from_file"):
  GdkPixbuf.Pixbuf.new_from_file = GdkPixbuf.Pixbuf.new_from_file_utf8

known_engine_modules = dict()
known_tools = dict()

_resource_loader_singleton = None

def get_tool_manager():
  global _resource_loader_singleton
  if _resource_loader_singleton is None:
    _resource_loader_singleton = ToolManager()
  return _resource_loader_singleton

_verbose = False
def _log_message(*message):
  if _verbose:
    print(*message)

class ToolManager():
  def __init__(self):
    self.known_engine_modules = dict()
    self.known_tools = dict()
    
    self.tool_search_paths = []
    self.engine_search_paths = []

  def add_engine_search_paths(self, *paths):
    for path in paths:
      self.engine_search_paths += [path]

  def add_tool_search_paths(self, *paths):
    for path in paths:
      self.tool_search_paths += [path]

  def get_mask(self, mask_name, first_search_path=None):
    if first_search_path:
      search_paths = [first_search_path] + self.tool_search_paths
    else:
      search_paths = self.tool_search_paths
  
    for search_dir in search_paths:
      maybe_path = os.path.join(search_dir, mask_name)
      if os.path.isfile(maybe_path):
        return drawbuffer.maskbuffer_from_png(maybe_path)
    raise RuntimeError("Couldn't find mask image \"%s\"" % mask_name)

  def get_engine(self, full_engine_name):
    engine_module, engine_class = full_engine_name.rsplit(".", 1)
    
    if engine_module in self.known_engine_modules:
      _log_message("Reusing", engine_module)
      engine_module = self.known_engine_modules[engine_module]
    else:
      _log_message("New engine", engine_module)
      found_module = imp.find_module(engine_module, self.engine_search_paths)
      try:
        # The tools. prefix is added here to prevent a brush engine
        # for accidently stepping on any of our core modules.
        imp_module = imp.load_module("tools." + engine_module, *found_module)
      finally:
        if found_module[0] is not None:
          found_module[0].close()
      self.known_engine_modules[engine_module] = imp_module
      
      engine_module = imp_module
    return engine_module.__dict__[engine_class]

  def parse_and_load_tool_file(self, filename):
    tools_array = []
    tool_dir = os.path.dirname(filename)
    with open(filename) as tools_file:
      tools_array = json.load(tools_file)
      
    for tool_dict in tools_array:
      # "mask", "path", and "engine" have special meaning,
      # everything else is a GObject property of the tool class
      #
      # mask: Not all tools have mask, if they do it will be an
      #       image in the tools directory.
      #
      # path: Is the internal name for the tool, it must be unique.
      #
      # engine: The full path to the tool class in the form 
      #         module_name.tool_class.
      try:
        tool_internal_name = tool_dict["path"]
        if tool_internal_name in self.known_tools:
          raise RuntimeError("A tool with the internal name %s has already been loaded, refusing to replace." % tool_internal_name)
        
        if "mask" in tool_dict:
          tool_dict["mask"] = self.get_mask(tool_dict["mask"], tool_dir)

        engine_class = self.get_engine(tool_dict["engine"])
        
        del tool_dict["path"]
        del tool_dict["engine"]
        
        new_tool = engine_class()
        for prop, value in tool_dict.items():
          new_tool.set_property(prop, value)
        
        self.known_tools[tool_internal_name] = new_tool
      except:
        import traceback
        print("Tool parse falied:")
        traceback.print_exc()

  def load_all_tools(self):
    # import "tools" first so imp.load_module can injecting things into it.
    import tools
    if not tools:
      return # This line can't be reached, it's just here to make pyflakes happy
    
    for tool_dir in self.tool_search_paths:
      _log_message("Searching for tool files in", tool_dir)
      try:
        tool_files = glob.glob(os.path.join(os.path.abspath(tool_dir), "*.json"))
        for tool_file in tool_files:
          try:
            _log_message("Attempting to load", tool_file)
            self.parse_and_load_tool_file(tool_file)
          except:
            import traceback
            print("Load falied:")
            traceback.print_exc()
      except:
        import traceback
        print("Bad path:")
        traceback.print_exc()

    return self.known_tools

def get_logo_pixbuf():
  full_path = os.path.join(os.path.dirname(__file__), "icons", "thedraw.png")
  if os.path.isfile(full_path):
    return GdkPixbuf.Pixbuf.new_from_file(full_path)

_cursor_map = dict()

def get_cursor(cursor_name):
  if cursor_name in _cursor_map:
    return _cursor_map[cursor_name]
  return None

def init_stock(stock_path=None):
  if not stock_path:
    stock_path = os.path.join(os.path.dirname(__file__), "icons")
    
  icon_mapping = [
    ("thedraw-logo", "thedraw.png"),
    ("thedraw-merge-down", "thedraw-merge-down-16.png"),
    ("thedraw-merge-down", "thedraw-merge-down-22.png"),
    ("thedraw-merge-down", "thedraw-merge-down-24.png"),
    ("thedraw-merge-down", "thedraw-merge-down-32.png"),
    ("thedraw-merge-down", "thedraw-merge-down-64.png"),
  ]
  
  icon_theme = Gtk.IconTheme.get_default()
  
  for stock_name, file_name in icon_mapping:
    full_path = os.path.join(stock_path, file_name)
    if os.path.isfile(full_path):
      pixbuf = GdkPixbuf.Pixbuf.new_from_file(full_path)
      icon_theme.add_builtin_icon(stock_name, pixbuf.get_height(), pixbuf)
      _log_message("Stock icon \"%s\": %s" % (stock_name, full_path))
  
  stock_mapping = [
    ("thedraw-merge-down", "Merge layer down", 0, 0, None),
  ]

  icon_fact = Gtk.IconFactory()
  icon_fact.add_default()
  
  for stock_entry in stock_mapping:
    iset = Gtk.IconSet()
    isource = Gtk.IconSource()
    isource.set_icon_name(stock_entry[0])
    iset.add_source(isource)
    icon_fact.add(stock_entry[0], iset)

  cursor_mapping = [
    ("thedraw-cursor-move-layer", "thedraw-cursor-move-layer.png"),
    ("thedraw-cursor-move-view",  "thedraw-cursor-move-view.png"),
  ]

  for cursor_name, file_name in cursor_mapping:
    full_path = os.path.join(stock_path, file_name)
    if os.path.isfile(full_path):
      pixbuf = GdkPixbuf.Pixbuf.new_from_file(full_path)
      cursor = Gdk.Cursor.new_from_pixbuf(Gdk.Display.get_default(),
                                          pixbuf,
                                          pixbuf.get_width()  // 2,
                                          pixbuf.get_height() // 2)
    _cursor_map[cursor_name] = cursor
