#include <Python.h>
#include <structmember.h>
#include <pycairo.h>
#include <pygobject.h>

#include <gegl.h>
#include <gegl-utils.h>

#include <stdlib.h>
#include <math.h>

#include "maskbuffer.h"
#include "drawbuffer-util.h"

static const Babl *BUFFER_FORMAT = NULL;

/*
static const char BUFFER_FORMAT_NAME[] = "RGBA u8";
typedef unsigned char BUFFER_COMP;
static const unsigned char BUFFER_COMP_FILL = 0xFF;
*/

static const char BUFFER_FORMAT_NAME[] = "R'G'B'A u16";
typedef unsigned short BUFFER_COMP;
static const unsigned short BUFFER_COMP_FILL = 0xFFFF;

/*
static const char BUFFER_FORMAT_NAME[] = "RGBA float";
typedef float BUFFER_COMP;
static const float BUFFER_COMP_FILL = 1.0;
*/

enum {
  BLEND_MODE_OVER = 0,
  BLEND_MODE_GROW_ALPHA = 1
} BlendMode;

typedef struct {
    PyObject_HEAD
    int x;
    int y;
    int width;
    int height;
    GeglBuffer *buffer;
} DrawBuffer;

Pycairo_CAPI_t *Pycairo_CAPI;

static PyTypeObject DrawBufferType;

static GeglColor *parse_color_obj(PyObject *color_obj)
{
  GeglColor *output = NULL;

  if (PyString_Check(color_obj))
  {
    output = gegl_color_new(PyString_AsString(color_obj));
  }
  else if (PySequence_Check(color_obj)) {
    Py_ssize_t seq_len = PySequence_Length(color_obj);
    float r, g, b;
    float a = 1.0;
    
    if (seq_len == 3 || seq_len == 4) {
      r = PyFloat_AsDouble(PySequence_GetItem(color_obj, 0));
      g = PyFloat_AsDouble(PySequence_GetItem(color_obj, 1));
      b = PyFloat_AsDouble(PySequence_GetItem(color_obj, 2));
    }
    else {
      PyErr_SetString(PyExc_ValueError, "Bad value for color");
      return NULL;
    }
    
    if (seq_len == 4) {
      a = PyFloat_AsDouble(PySequence_GetItem(color_obj, 3));
    }
    
    output = gegl_color_new(NULL);
    gegl_color_set_rgba(output, r, g, b, a);
  }
  else
  {
    PyErr_SetString(PyExc_ValueError, "Bad value for color");
    return NULL;
  }
  return output;
}

static PyObject *
drawbuffer_init(PyObject *self, PyObject *args)
{
  gegl_init(0, NULL);

  if (!BUFFER_FORMAT)
    BUFFER_FORMAT = babl_format(BUFFER_FORMAT_NAME);

  pygobject_init(-1, -1, -1);
  
  Py_RETURN_NONE;
}


static PyObject*
gegl_cairo_sink_node(PyObject *self, PyObject *args)
{
  PycairoImageSurface *target_surface = NULL;
  PyObject *node_obj = NULL;

  if (!PyArg_ParseTuple(args, "OO!", &node_obj, &PycairoImageSurface_Type, &target_surface)) {
    return NULL;
  }

  if (!GEGL_IS_NODE(pygobject_get(node_obj))) {
    return NULL;
  }

  GeglNode *ptn = GEGL_NODE(pygobject_get(node_obj));

  GeglNode *result_node = gegl_node_new_child(ptn,
                                              "operation", "thedraw:cairo-sink",
                                              "surface", target_surface->surface,
                                              NULL);

  PyObject *result = pygobject_new(G_OBJECT(result_node));
  /* We don't need to unref because gegl_node_new_child already gave the node to ptn */
  /* g_object_unref(result_node); */

  return result;
}

static PyObject*
gegl_buffer_cairo_new(PyObject *self, PyObject *args)
{
  PycairoImageSurface *target_surface = NULL;

  if (!PyArg_ParseTuple(args, "O!", &PycairoImageSurface_Type, &target_surface)) {
    return NULL;
  }

  GeglRectangle roi;
  roi.x = 0;
  roi.y = 0;
  roi.width  = cairo_image_surface_get_width(target_surface->surface);
  roi.height = cairo_image_surface_get_height(target_surface->surface);

  GeglBuffer *cairo_buffer;
  unsigned char *data = cairo_image_surface_get_data(target_surface->surface);

  cairo_format_t surface_format = cairo_image_surface_get_format(target_surface->surface);

  if (CAIRO_FORMAT_ARGB32 == surface_format)
  {
    cairo_buffer = gegl_buffer_linear_new_from_data(data,
                                                    babl_format("cairo-ARGB32"),
                                                    &roi,
                                                    0,
                                                    NULL,
                                                    NULL);
  }
  else if (CAIRO_FORMAT_RGB24 == surface_format)
  {
    cairo_buffer = gegl_buffer_linear_new_from_data(data,
                                                    babl_format("cairo-RGB24"),
                                                    &roi,
                                                    0,
                                                    NULL,
                                                    NULL);
  }
  else
  {
    PyErr_SetString(PyExc_ValueError, "Unsupported surface format");
    return NULL;
  }

  PyObject *result = pygobject_new(G_OBJECT(cairo_buffer));

  g_object_unref(cairo_buffer); // *result owns this buffer now

  return result;
}

static PyObject*
srgb_to_rgb(PyObject *self, PyObject *args)
{
  float in_array[3];
  float out_array[3];
  
  if (!PyArg_ParseTuple(args, "(fff)", &in_array[0], &in_array[1], &in_array[2])) {
    return NULL;
  }
  
  const Babl *fish  = babl_fish(babl_format("R'G'B' float"), babl_format("RGB float"));
  babl_process(fish, in_array, out_array, 1);
  
  return Py_BuildValue("(fff)", out_array[0], out_array[1], out_array[2]);
}

static PyObject*
rgb_to_srgb(PyObject *self, PyObject *args)
{
  float in_array[3];
  float out_array[3];
  
  if (!PyArg_ParseTuple(args, "(fff)", &in_array[0], &in_array[1], &in_array[2])) {
    return NULL;
  }
  
  const Babl *fish  = babl_fish(babl_format("RGB float"), babl_format("R'G'B' float"));
  babl_process(fish, in_array, out_array, 1);
  
  return Py_BuildValue("(fff)", out_array[0], out_array[1], out_array[2]);
}

static PyObject *
drawbuffer_from_png(PyObject *self, PyObject *args)
{
  char *path;

  if (!PyArg_ParseTuple(args, "s", &path)) {
    return NULL;
  }

  /* Allocate the object first, before we release the GIL */
  DrawBuffer *drawbuf = (DrawBuffer *)PyType_GenericNew(&DrawBufferType, NULL, NULL);

  Py_BEGIN_ALLOW_THREADS;

  GeglNode *ptn, *src, *dst;

  ptn = gegl_node_new();
  src = gegl_node_new_child(ptn, "operation", "gegl:load",
                            "path", path,
                            NULL);

  GeglRectangle extent = gegl_node_get_bounding_box(src);
  drawbuf->x = 0;
  drawbuf->y = 0;
  drawbuf->width = extent.width;
  drawbuf->height = extent.height;

  GeglRectangle roi;
  roi.x = 0;
  roi.y = 0;
  roi.width = drawbuf->width;
  roi.height = drawbuf->height;

  drawbuf->buffer = gegl_buffer_new(&roi, BUFFER_FORMAT);

  dst = gegl_node_new_child(ptn, "operation", "gegl:write-buffer",
                            "buffer", drawbuf->buffer,
                            NULL);
  gegl_node_connect_to (src, "output",  dst, "input");

  gegl_node_process(dst);
  g_object_unref(ptn);

  Py_END_ALLOW_THREADS;

  //printf("Loaded %d %d\n", drawbuf->width, drawbuf->height);

  return (PyObject *)drawbuf;
}

static PyMethodDef DrawBufferModMethods[] =
{
     {"gegl_buffer_cairo_new", gegl_buffer_cairo_new, METH_VARARGS, ""},
     {"gegl_cairo_sink_node", gegl_cairo_sink_node, METH_VARARGS, ""},
     {"srgb_to_rgb", srgb_to_rgb, METH_VARARGS, ""},
     {"rgb_to_srgb", rgb_to_srgb, METH_VARARGS, ""},
     {"drawbuffer_from_png", drawbuffer_from_png, METH_VARARGS, ""},
     {"maskbuffer_from_png", maskbuffer_from_png, METH_VARARGS, ""},
     {"init", drawbuffer_init, METH_VARARGS, ""},
     {"gegl_buffer_copy_to_cairo", gegl_buffer_copy_to_cairo, METH_VARARGS, ""},
     {NULL, NULL, 0, NULL}
};

/* --- Begin class --- */

static PyObject *
DrawBuffer_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
  DrawBuffer *self;

  self = (DrawBuffer *)type->tp_alloc(type, 0);
  
  if (self == NULL) {
    return NULL;
  }
  
  self->x = 0;
  self->y = 0;
  self->width = 0;
  self->height = 0;
  self->buffer = NULL;

  return (PyObject *)self;
}

static void
DrawBuffer_dealloc(DrawBuffer* self)
{
  if (self->buffer)
  {
    g_object_unref(self->buffer);
  }
  self->ob_type->tp_free((PyObject*)self);
}

static int
DrawBuffer_init(DrawBuffer *self, PyObject *args, PyObject *kwargs)
{
  static char *kwlist[] = {"width", "height", "x", "y", "color", NULL};
  PyObject *color_obj = NULL;
  GeglColor *bgcolor  = NULL;
  
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "ii|iiO", kwlist,
                                   &self->width, &self->height,
                                   &self->x, &self->y, &color_obj)) {
    return -1;
  }
  
  if (self->width < 0)
  {
    self->ob_type->tp_free((PyObject*)self);
    PyErr_SetString(PyExc_ValueError, "Invalid width");
    return -1;
  }

  if (self->height < 0)
  {
    self->ob_type->tp_free((PyObject*)self);
    PyErr_SetString(PyExc_ValueError, "Invalid height");
    return -1;
  }

  if (color_obj)
  {
    bgcolor = parse_color_obj(color_obj);
    if (!bgcolor)
    {
      return -1;
    }
  }

  Py_BEGIN_ALLOW_THREADS;

  GeglRectangle roi;
  roi.x = self->x;
  roi.y = self->y;
  roi.width = self->width;
  roi.height = self->height;
  
  self->buffer = gegl_buffer_new(&roi, BUFFER_FORMAT);

  if(bgcolor)
  {
    gegl_buffer_set_color(self->buffer, &roi, bgcolor);
    g_object_unref(bgcolor);
  }

  Py_END_ALLOW_THREADS;

  return 0;
}

static PyObject *
DrawBuffer_blit_to(DrawBuffer *self, PyObject *args)
{
  DrawBuffer *target_buffer = NULL;

  GeglRectangle src_roi;
  src_roi.x = self->x;
  src_roi.y = self->y;
  src_roi.width = self->width;
  src_roi.height = self->height;

  GeglRectangle dst_roi;
  dst_roi.x = self->x;
  dst_roi.y = self->y;
  dst_roi.width = self->width;
  dst_roi.height = self->height;

  if (!PyArg_ParseTuple(args, "O!|ii(iiii)", &DrawBufferType, &target_buffer,
                                             &dst_roi.x, &dst_roi.y,
                                             &src_roi.x, &src_roi.y,
                                             &src_roi.width, &src_roi.height)) {
    return NULL;
  }

  dst_roi.width = src_roi.width;
  dst_roi.height = src_roi.height;

  Py_BEGIN_ALLOW_THREADS;

  gegl_buffer_copy(self->buffer, &src_roi,
                   target_buffer->buffer, &dst_roi);

  Py_END_ALLOW_THREADS;

  Py_RETURN_NONE;
}

static PyObject *
DrawBuffer_blit_to_cairo_image(DrawBuffer *self, PyObject *args)
{
  PycairoImageSurface *target_surface = NULL;
  int src_x = 0;
  int src_y = 0;
  
  GeglRectangle dst_roi;
  dst_roi.x = 0;
  dst_roi.y = 0;
  dst_roi.width = self->width;
  dst_roi.height = self->height;

  if (!PyArg_ParseTuple(args, "O!|ii(iiii)", &PycairoImageSurface_Type, &target_surface,
                                             &dst_roi.x, &dst_roi.y,
                                             &src_x, &src_y,
                                             &dst_roi.width, &dst_roi.height)) {
    return NULL;
  }
  
  int target_width  = cairo_image_surface_get_width(target_surface->surface);
  int target_height = cairo_image_surface_get_height(target_surface->surface);
  
  if (dst_roi.x >= target_width || dst_roi.y >= target_height)
  {
    Py_RETURN_NONE;
  }
  
  dst_roi.width  -= src_x;
  dst_roi.height -= src_y;
  
  if (CAIRO_FORMAT_ARGB32 != cairo_image_surface_get_format(target_surface->surface))
  {
    PyErr_SetString(PyExc_ValueError, "ImageSurface must be ARGB32");
    return NULL;
  }
  
  cairo_surface_flush(target_surface->surface);
  
  GeglNode *bg_dst, *im_src, *trans;
  GeglNode *ptn = gegl_node_new();
  im_src = gegl_node_new_child(ptn, 
                               "operation", "gegl:buffer-source",
                               "buffer", self->buffer, 
                               NULL);
  trans = gegl_node_new_child(ptn, 
                               "operation", "gegl:translate",
                               "x", (double)(dst_roi.x - src_x), 
                               "y", (double)(dst_roi.y - src_y),
                               NULL);
  bg_dst = gegl_node_new_child(ptn, 
                               "operation", "thedraw:cairo-sink",
                               "surface", target_surface->surface,
                               NULL);
  
  gegl_node_connect_to (im_src, "output",  trans, "input");
  gegl_node_connect_to (trans, "output",  bg_dst, "input");

  gegl_node_blit(bg_dst, 1.0, &dst_roi, NULL, NULL, 0, GEGL_BLIT_DEFAULT);
  
  g_object_unref(ptn);
  
  cairo_surface_mark_dirty(target_surface->surface);
  
  Py_RETURN_NONE;
}

static PyObject *
DrawBuffer_duplicate(DrawBuffer *self)
{
  DrawBuffer *drawbuf = (DrawBuffer *)PyType_GenericNew(&DrawBufferType, NULL, NULL);
  const GeglRectangle *src_extent = gegl_buffer_get_extent(self->buffer);
  drawbuf->x = src_extent->x;
  drawbuf->y = src_extent->y;
  drawbuf->width = src_extent->width;
  drawbuf->height = src_extent->height;
  drawbuf->buffer = gegl_buffer_dup(self->buffer);

  return (PyObject *)drawbuf;
}


#define gegl_tile_indice(coordinate,stride) \
  (((coordinate) >= 0)?\
      (coordinate) / (stride):\
      ((((coordinate) + 1) /(stride)) - 1))

/* This version of duplicate adjusts the extent so all tiles are cow copied,
 * this will include the invalid regions outside the current extent.
 */
static PyObject *
DrawBuffer_duplicate_tiles(DrawBuffer *self)
{
  DrawBuffer *drawbuf = (DrawBuffer *)PyType_GenericNew(&DrawBufferType, NULL, NULL);
  GeglRectangle expanded_extent, src_extent;

  int tile_width, tile_height;
  int x0, y0, x1, y1;

  gegl_rectangle_copy(&src_extent, gegl_buffer_get_extent(self->buffer));

  drawbuf->x = self->x;
  drawbuf->y = self->y;
  drawbuf->width = self->width;
  drawbuf->height = self->height;

  x0 = src_extent.x;
  y0 = src_extent.y;
  x1 = src_extent.x + src_extent.width;
  y1 = src_extent.y + src_extent.height;

  g_object_get(self->buffer,
               "tile-width", &tile_width,
               "tile-height", &tile_height,
               NULL);

  /* FIXME: account for shift */

  x0 = gegl_tile_indice(x0, tile_width) * tile_width;
  y0 = gegl_tile_indice(y0, tile_height) * tile_height;
  x1 = (gegl_tile_indice(x1, tile_width) + 1) * tile_width;
  y1 = (gegl_tile_indice(y1, tile_height) + 1) * tile_height;

  expanded_extent.x = x0;
  expanded_extent.y = y0;
  expanded_extent.width  = x1 - x0;
  expanded_extent.height = y1 - y0;

  gegl_buffer_set_extent(self->buffer, &expanded_extent);

  drawbuf->buffer = gegl_buffer_dup(self->buffer);
  gegl_buffer_set_extent(self->buffer, &src_extent);
  gegl_buffer_set_extent(drawbuf->buffer, &src_extent);

  return (PyObject *)drawbuf;
}

static PyObject *
DrawBuffer_circle(DrawBuffer *self, PyObject *args, PyObject *kwargs)
{
  static char *kwlist[] = {"x", "y", "r", "color", NULL};
  PyObject *color_obj = NULL;
  GeglColor *bgcolor = NULL;
  float x, y, r;
  
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "fff|O", kwlist, &x, &y, &r, &color_obj)) {
    return NULL;
  }
  
  if (color_obj)
  {
    bgcolor = parse_color_obj(color_obj);
    if (!bgcolor)
    {
      return NULL;
    }
  }
  else
  {
    bgcolor = gegl_color_new("#000000FF");
  }

  Py_BEGIN_ALLOW_THREADS;

  GeglRectangle roi;
  
  roi.x = x - r - 0.5;
  roi.y = y - r - 0.5;
  roi.width  = 2.0 * r + 1.5;
  roi.height = 2.0 * r + 1.5;
  
  if (roi.width < 1)
    roi.width = 1;
  if (roi.height < 1)
    roi.height = 1;

  GeglBufferIterator *iter = gegl_buffer_iterator_new(self->buffer, &roi, 0,
                                                      BUFFER_FORMAT,
                                                      GEGL_BUFFER_READWRITE,
                                                      GEGL_ABYSS_NONE);
  
  BUFFER_COMP color_pixel[4];
  
  gegl_color_get_pixel(bgcolor, BUFFER_FORMAT, &color_pixel);
  
  float r_sqr = r*r;
  while (gegl_buffer_iterator_next(iter))
  {
    BUFFER_COMP *pixel = (BUFFER_COMP *)iter->data[0];
    int iy, ix;
    for (iy = iter->roi[0].y; iy < iter->roi[0].y + iter->roi[0].height; iy++)
    {
      for (ix = iter->roi[0].x; ix < iter->roi[0].x +  iter->roi[0].width; ix++)
      {
        float d_sqr = powf(x-ix, 2.0f) + powf(y-iy, 2.0f);
        if (d_sqr < r_sqr)
        {
          float dist = sqrt(d_sqr);
          if (dist < r - 1)
          {
            pixel[0] = color_pixel[0];
            pixel[1] = color_pixel[1];
            pixel[2] = color_pixel[2];
                        
            if (pixel[3] < color_pixel[3])
              pixel[3] = color_pixel[3];
          }
          else
          {
            float alpha = (r - dist) * ((float)(color_pixel[3]) / BUFFER_COMP_FILL);
            float dst_alpha = (float)pixel[3] / BUFFER_COMP_FILL;

            float a = alpha + dst_alpha * (1.0 - alpha);
            float a_term = dst_alpha * (1.0 - alpha);
            float r = color_pixel[0] * alpha + pixel[0] * a_term;
            float g = color_pixel[1] * alpha + pixel[1] * a_term;
            float b = color_pixel[2] * alpha + pixel[2] * a_term;
            
            pixel[0] = r / a;
            pixel[1] = g / a;
            pixel[2] = b / a;

            if (pixel[3] < BUFFER_COMP_FILL * alpha)
              pixel[3] = BUFFER_COMP_FILL * alpha;
          }
        }
        pixel += 4;
      }
    }
  }
  
  g_object_unref(bgcolor);

  Py_END_ALLOW_THREADS;

  Py_RETURN_NONE;
}

void
apply_soft_dab (DrawBuffer *target, float x, float y, float radius, GeglColor *color, float hardness, float aspect_ratio, float angle)
{
  /* Adaptation of code from MyPaint's render_dab_mask, original version by Martin Renold */

  if (hardness < 0.0f)
    hardness = 0.0f;
  else if (hardness > 1.0f)
    hardness = 1.0f;

	if (aspect_ratio < 1.0f)
    aspect_ratio = 1.0f;

  float r_fringe;
  float xx, yy, rr;
  float one_over_radius2;

  r_fringe = radius + 1;
  rr = radius*radius;
  one_over_radius2 = 1.0f/rr;

  // For a graphical explanation, see:
  // http://wiki.mypaint.info/Development/Documentation/Brushlib
  //
  // The hardness calculation is explained below:
  //
  // Dab opacity gradually fades out from the center (rr=0) to
  // fringe (rr=1) of the dab. How exactly depends on the hardness.
  // We use two linear segments, for which we pre-calculate slope
  // and offset here.
  //
  // opa
  // ^
  // *   .
  // |        *
  // |          .
  // +-----------*> rr = (distance_from_center/radius)^2
  // 0           1
  //
  float segment1_offset = 1.0f;
  float segment1_slope  = -(1.0f/hardness - 1.0f);
  float segment2_offset = hardness/(1.0f-hardness);
  float segment2_slope  = -hardness/(1.0f-hardness);
  // for hardness == 1.0, segment2 will never be used

  float angle_rad=angle/360*2*M_PI;
  float cs=cosf(angle_rad);
  float sn=sinf(angle_rad);

  int x0 = floor (x - r_fringe);
  int y0 = floor (y - r_fringe);
  int x1 = ceil (x + r_fringe);
  int y1 = ceil (y + r_fringe);
  
  GeglRectangle roi = {x0, y0, x1 - x0, y1 - y0};

  GeglBufferIterator *iter = gegl_buffer_iterator_new(target->buffer, &roi, 0,
                                                      BUFFER_FORMAT,
                                                      GEGL_BUFFER_READWRITE,
                                                      GEGL_ABYSS_NONE);
  
  BUFFER_COMP color_pixel[4];
  
  gegl_color_get_pixel(color, BUFFER_FORMAT, &color_pixel);

  while (gegl_buffer_iterator_next(iter))
  {
    BUFFER_COMP *pixel = (BUFFER_COMP *)iter->data[0];
    int iy, ix;
    for (iy = iter->roi[0].y; iy < iter->roi[0].y + iter->roi[0].height; iy++)
    {
      yy = (iy + 0.5f - y);
      for (ix = iter->roi[0].x; ix < iter->roi[0].x +  iter->roi[0].width; ix++)
      {
        xx = (ix + 0.5f - x);
        float yyr=(yy*cs-xx*sn)*aspect_ratio;
        float xxr=yy*sn+xx*cs;
        rr = (yyr*yyr + xxr*xxr) * one_over_radius2;
        // rr is in range 0.0..1.0*sqrt(2)
        
        if (rr <= 1.0f)
          {
            float alpha;
            if (rr <= hardness)
              {
                alpha = segment1_offset + rr * segment1_slope;
              }
            else
              {
                alpha = segment2_offset + rr * segment2_slope;
              }
            
            alpha = alpha * ((float)(color_pixel[3]) / BUFFER_COMP_FILL);
            float dst_alpha = (float)pixel[3] / BUFFER_COMP_FILL;

            float a = alpha + dst_alpha * (1.0f - alpha);
            float a_term = dst_alpha * (1.0f - alpha);
            float r = color_pixel[0] * alpha + pixel[0] * a_term;
            float g = color_pixel[1] * alpha + pixel[1] * a_term;
            float b = color_pixel[2] * alpha + pixel[2] * a_term;
            
            pixel[0] = r / a;
            pixel[1] = g / a;
            pixel[2] = b / a;
            if (alpha > dst_alpha)
              pixel[3] = BUFFER_COMP_FILL * ((alpha - dst_alpha) * alpha + dst_alpha);
          }
        pixel += 4;
      }
    }
  }
}

static PyObject *
DrawBuffer_soft_circle(DrawBuffer *self, PyObject *args, PyObject *kwargs)
{
  static char *kwlist[] = {"x", "y", "r", "color", "hardness", NULL};
  PyObject *color_obj = NULL;
  GeglColor *bgcolor = NULL;
  float x, y, r, hardness = 1.0;
  
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "fff|Of", kwlist, &x, &y, &r, &color_obj, &hardness)) {
    return NULL;
  }
  
  if (color_obj)
  {
    bgcolor = parse_color_obj(color_obj);
    if (!bgcolor)
    {
      return NULL;
    }
  }
  else
  {
    bgcolor = gegl_color_new("#000000FF");
  }

  apply_soft_dab (self, x, y, r, bgcolor, hardness, 1.0f, 1.0f);
  
  g_object_unref(bgcolor);
  
  Py_RETURN_NONE;
}


static PyObject *
DrawBuffer_soft_dab(DrawBuffer *self, PyObject *args, PyObject *kwargs)
{
  static char *kwlist[] = {"x", "y", "r", "color", "hardness", "aspect_ratio", "angle", NULL};
  PyObject *color_obj = NULL;
  GeglColor *bgcolor = NULL;
  float x, y, r, hardness = 1.0f, aspect_ratio = 1.0f, angle = 0.0f;
  
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "fff|Offf", kwlist, &x, &y, &r, &color_obj, &hardness, &aspect_ratio, &angle)) {
    return NULL;
  }
  
  if (color_obj)
  {
    bgcolor = parse_color_obj(color_obj);
    if (!bgcolor)
    {
      return NULL;
    }
  }
  else
  {
    bgcolor = gegl_color_new("#000000FF");
  }
  
  apply_soft_dab (self, x, y, r, bgcolor, hardness, aspect_ratio, angle);
  
  g_object_unref(bgcolor);
  
  Py_RETURN_NONE;
}

void
apply_mask_over (DrawBuffer *target, int x, int y, GeglColor *color, MaskBuffer *mask_buffer)
{
  GeglRectangle roi;
  
  roi.x = x;
  roi.y = y;
  roi.width  = mask_buffer->width;
  roi.height = mask_buffer->height;
  
  BUFFER_COMP color_pixel_raw[4];
  
  gegl_color_get_pixel(color, BUFFER_FORMAT, &color_pixel_raw);
  
  if (color_pixel_raw[3] == 0)
  {
    return;
  }

  GeglBufferIterator *iter = gegl_buffer_iterator_new(target->buffer, &roi, 0,
                                                      BUFFER_FORMAT,
                                                      GEGL_BUFFER_READWRITE,
                                                      GEGL_ABYSS_NONE);
  
  const unsigned int mask_stride = mask_buffer->width;
  const float buffer_comp_fill_recip = 1.0f / BUFFER_COMP_FILL;
  const float color_pixel[3] = {color_pixel_raw[0],
                                color_pixel_raw[1],
                                color_pixel_raw[2]};
  const float color_pixel_alpha = color_pixel_raw[3] * buffer_comp_fill_recip;

  while (gegl_buffer_iterator_next(iter))
  {
    BUFFER_COMP *pixel = (BUFFER_COMP *)iter->data[0];
    int iy, ix;
    for (iy = 0; iy < iter->roi[0].height; iy++)
    {
      int mask_offset = (iy + iter->roi[0].y - roi.y) * mask_stride + iter->roi[0].x - roi.x;
      const float *mask_pixel = &mask_buffer->data[mask_offset];
      for (ix = 0; ix < iter->roi[0].width; ix++)
      {
        float alpha = *mask_pixel * color_pixel_alpha;
        if (alpha > 0.0f)
        {
          float dst_alpha = (pixel[3] * buffer_comp_fill_recip);

          float a_term = dst_alpha * (1.0f - alpha);
          float r = color_pixel[0] * alpha + pixel[0] * a_term;
          float g = color_pixel[1] * alpha + pixel[1] * a_term;
          float b = color_pixel[2] * alpha + pixel[2] * a_term;
          /* float a = BUFFER_COMP_FILL * alpha + BUFFER_COMP_FILL * a_term; */

          float a = alpha + a_term;
          float a_recip = 1.0f / a;
          pixel[0] = r * a_recip;
          pixel[1] = g * a_recip;
          pixel[2] = b * a_recip;
          pixel[3] = BUFFER_COMP_FILL * a;
        }
        
        pixel += 4;
        mask_pixel += 1;
      }
    }
  }
}


void
apply_mask_grow_alpha (DrawBuffer *target, int x, int y, GeglColor *color, MaskBuffer *mask_buffer)
{
  GeglRectangle roi;
  
  roi.x = x;
  roi.y = y;
  roi.width  = mask_buffer->width;
  roi.height = mask_buffer->height;
  
  BUFFER_COMP color_pixel_raw[4];
  
  gegl_color_get_pixel(color, BUFFER_FORMAT, &color_pixel_raw);
  
  if (color_pixel_raw[3] == 0)
  {
    return;
  }

  GeglBufferIterator *iter = gegl_buffer_iterator_new(target->buffer, &roi, 0,
                                                      BUFFER_FORMAT,
                                                      GEGL_BUFFER_READWRITE, 
                                                      GEGL_ABYSS_NONE);
  
  const unsigned int mask_stride = mask_buffer->width;
  const float buffer_comp_fill_recip = 1.0f / BUFFER_COMP_FILL;
  const float color_pixel[3] = {color_pixel_raw[0],
                                color_pixel_raw[1],
                                color_pixel_raw[2]};
  const float color_pixel_alpha = color_pixel_raw[3] * buffer_comp_fill_recip;

  while (gegl_buffer_iterator_next(iter))
  {
    BUFFER_COMP *pixel = (BUFFER_COMP *)iter->data[0];
    int iy, ix;
    for (iy = 0; iy < iter->roi[0].height; iy++)
    {
      int mask_offset = (iy + iter->roi[0].y - roi.y) * mask_stride + iter->roi[0].x - roi.x;
      const float *mask_pixel = &mask_buffer->data[mask_offset];
      for (ix = 0; ix < iter->roi[0].width; ix++)
      {
        float alpha = *mask_pixel * color_pixel_alpha;
        if (alpha > 0.0f)
        {
          float dst_alpha = (pixel[3] * buffer_comp_fill_recip);

          float a_term = dst_alpha * (1.0f - alpha);
          float r = color_pixel[0] * alpha + pixel[0] * a_term;
          float g = color_pixel[1] * alpha + pixel[1] * a_term;
          float b = color_pixel[2] * alpha + pixel[2] * a_term;
          /* float a = BUFFER_COMP_FILL * alpha + BUFFER_COMP_FILL * a_term; */

          float a = alpha + a_term;
          float a_recip = 1.0f / a;
          pixel[0] = r * a_recip;
          pixel[1] = g * a_recip;
          pixel[2] = b * a_recip;
          if (alpha > dst_alpha)
            pixel[3] = BUFFER_COMP_FILL * ((alpha - dst_alpha) * alpha + dst_alpha);
        }
        
        pixel += 4;
        mask_pixel += 1;
      }
    }
  }
}

static PyObject *
DrawBuffer_apply_mask(DrawBuffer *self, PyObject *args, PyObject *kwargs)
{
  static char *kwlist[] = {"x", "y", "mask", "color", "blendmode", NULL};
  PyObject *color_obj = NULL;
  MaskBuffer *mask_buffer;
  GeglColor *color = NULL;
  int blendmode = BLEND_MODE_OVER;
  int x, y;
  
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "iiO!|Oi", kwlist, &x, &y, 
                                                                    &MaskBufferType, &mask_buffer,
                                                                    &color_obj,
                                                                    &blendmode)) {
    return NULL;
  }
  
  if (color_obj)
  {
    color = parse_color_obj(color_obj);
    if (!color)
    {
      return NULL;
    }
  }
  else
  {
    color = gegl_color_new("#000000FF");
  }
  
  
  Py_BEGIN_ALLOW_THREADS;

  if (blendmode == BLEND_MODE_OVER)
    apply_mask_over(self, x, y, color, mask_buffer);
  else if (blendmode == BLEND_MODE_GROW_ALPHA)
    apply_mask_grow_alpha(self, x, y, color, mask_buffer);
  g_object_unref(color);

  Py_END_ALLOW_THREADS;

  Py_RETURN_NONE;
}

static PyObject *
DrawBuffer_fill(DrawBuffer *self, PyObject *args, PyObject *kwargs)
{
  static char *kwlist[] = {"x", "y", "w", "h", "color", NULL};
  PyObject *color_obj = NULL;
  GeglColor *bgcolor  = NULL;
  double x, y, w, h;
  
  x = self->x;
  y = self->y;
  w = self->width;
  h = self->height;
  
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|ddddO", kwlist, &x, &y, &w, &h, &color_obj)) {
    return NULL;
  }
  
  if (color_obj)
  {
    bgcolor = parse_color_obj(color_obj);
    if (!bgcolor)
    {
      return NULL;
    }
  }
  else
  {
    bgcolor = gegl_color_new("#000000FF");
  }

  Py_BEGIN_ALLOW_THREADS;

  GeglRectangle roi;
  roi.x = x;
  roi.y = y;
  roi.width = w;
  roi.height = h;

  gegl_buffer_set_color(self->buffer, &roi, bgcolor);
  g_object_unref(bgcolor);

  Py_END_ALLOW_THREADS;

  Py_RETURN_NONE;
}

static PyObject *
DrawBuffer_to_png(DrawBuffer *self, PyObject *args)
{
  char *path;
  int bitdepth = 8;
  if (sizeof(BUFFER_COMP) == 2)
    bitdepth = 16;

  if (!PyArg_ParseTuple(args, "s", &path)) {
    return NULL;
  }

  Py_BEGIN_ALLOW_THREADS;

  GeglNode *ptn, *src, *dst;

  ptn = gegl_node_new();
  src = gegl_node_new_child(ptn, "operation", "gegl:buffer-source",
                            "buffer", self->buffer,
                            NULL);

  dst = gegl_node_new_child(ptn, "operation", "gegl:png-save",
                            "path", path,
                            "compression", 9,
                            "bitdepth", bitdepth,
                            NULL);

  gegl_node_connect_to (src, "output",  dst, "input");

  gegl_node_process(dst);

  g_object_unref(ptn);

  Py_END_ALLOW_THREADS;

  Py_RETURN_NONE;
}

static PyObject *
DrawBuffer_get_gegl(DrawBuffer* self)
{
  return pygobject_new(G_OBJECT(self->buffer));
}

static PyObject *
DrawBuffer_get_pixel(DrawBuffer *self, PyObject *args)
{
  int x, y;
  
  x = 0;
  y = 0;
  
  if (!PyArg_ParseTuple(args, "ii", &x, &y)) {
    return NULL;
  }
  
  float out_array[4] = {0.0, 0.0, 0.0, 0.0};
  
  gegl_buffer_get(self->buffer,
                  GEGL_RECTANGLE(x, y, 1, 1),
                  1.0,
                  babl_format("RGBA float"),
                  &out_array,
                  0,
                  GEGL_ABYSS_NONE);
  
  return Py_BuildValue("(ffff)", out_array[0], out_array[1], out_array[2], out_array[3]);
}

static PyObject *
DrawBuffer_set_extent(DrawBuffer *self, PyObject *args)
{
  int x, y, width, height;

  x = 0;
  y = 0;
  width = 0;
  height = 0;

  if (!PyArg_ParseTuple(args, "iiii", &x, &y, &width, &height)) {
    return NULL;
  }

  if (width < 0 || height < 0) {
    PyErr_SetString(PyExc_ValueError, "Extent size must be >= 0");
  }

  gegl_buffer_set_extent(self->buffer, GEGL_RECTANGLE(x, y, width, height));

  self->x = x;
  self->y = y;
  self->width  = width;
  self->height = height;

  Py_RETURN_NONE;
}

/* --- end buffer interface --- */

static PyMemberDef DrawBuffer_members[] = {
    {"width", T_INT, offsetof(DrawBuffer, width), READONLY,
     "pixel width"},
    {"height", T_INT, offsetof(DrawBuffer, height), READONLY,
     "pixel height"},
    {"x", T_INT, offsetof(DrawBuffer, x), READONLY,
     "buffer origin"},
    {"y", T_INT, offsetof(DrawBuffer, y), READONLY,
     "buffer origin"},
    {NULL}  /* Sentinel */
};

static PyMethodDef DrawBuffer_methods[] = {
    {"get_gegl", (PyCFunction)DrawBuffer_get_gegl, METH_NOARGS,
     "get_gegl() -> GeglBuffer"
    },
    {"get_pixel", (PyCFunction)DrawBuffer_get_pixel, METH_VARARGS,
     "get_pixel(x, y) -> (float r, float g , float b , float a)"
    },
    {"set_extent", (PyCFunction)DrawBuffer_set_extent, METH_VARARGS,
     "set_extent(x, y, width, height) -> None\nSet the buffers extent."
    },
    {"blit_to", (PyCFunction)DrawBuffer_blit_to, METH_VARARGS,
     "blit_to(DrawBuffer, int x, int y) -> None"
    },
    {"blit_to_cairo_image", (PyCFunction)DrawBuffer_blit_to_cairo_image, METH_VARARGS,
     "blit_to_cairo_image(cairo.ImageSurface, int x, int y) -> None\nBlit to a cairo RGBA32 image surface."
    },
    {"circle", (PyCFunction)DrawBuffer_circle, METH_VARARGS|METH_KEYWORDS,
     "circle(double x, double y, double r, color) -> None\nDraw a circle of radius r centered at x, y."
    },
    {"soft_circle", (PyCFunction)DrawBuffer_soft_circle, METH_VARARGS|METH_KEYWORDS,
     "soft_circle(double x, double y, double r, color, hardness) -> None\nDraw a circle of radius r centered at x, y."
    },
    {"soft_dab", (PyCFunction)DrawBuffer_soft_dab, METH_VARARGS|METH_KEYWORDS,
     "soft_dab(double x, double y, double r, color, hardness, aspect_ratio, angle) -> None"
    },
    {"apply_mask", (PyCFunction)DrawBuffer_apply_mask, METH_VARARGS|METH_KEYWORDS,
     "apply_mask(double x, double y, MaskBuffer mask, color, blendmode) -> None\nApply mask centered at x, y."
    },
    {"fill", (PyCFunction)DrawBuffer_fill, METH_VARARGS|METH_KEYWORDS,
     "fill(double x, double y, double w, double h, color) -> None\nFill a rectangle, or the entire buffer if no dimensions are given."
    },
    {"to_png", (PyCFunction)DrawBuffer_to_png, METH_VARARGS,
     "to_png(path) -> None\nWrite the buffer to a png file."
    },
    {"duplicate", (PyCFunction)DrawBuffer_duplicate, METH_NOARGS,
     "duplicate() -> GeglBuffer\nDuplicate this DrawBuffer"
    },
    {"duplicate_tiles", (PyCFunction)DrawBuffer_duplicate_tiles, METH_NOARGS,
     "duplicate_tiles() -> GeglBuffer\nLike duplicate(), but copies full tiles, including invalid regions outside the buffer's extent"
    },
    {NULL}  /* Sentinel */
};

static PyTypeObject DrawBufferType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "drawbuffer.DrawBuffer",       /*tp_name*/
    sizeof(DrawBuffer),             /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)DrawBuffer_dealloc, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT, /*tp_flags*/
    "",           /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    DrawBuffer_methods,             /* tp_methods */
    DrawBuffer_members,             /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)DrawBuffer_init,      /* tp_init */
    0,                         /* tp_alloc */
    DrawBuffer_new,                 /* tp_new */
};

/* --- End class --- */

PyMODINIT_FUNC
 
initdrawbuffer(void)
{
  PyObject* m;
  
  if (PyType_Ready(&DrawBufferType) < 0)
      return;

  if (PyType_Ready(&MaskBufferType) < 0)
      return;
      
  m = Py_InitModule3("drawbuffer", DrawBufferModMethods,
                     "drawbuffer module");

  if (m == NULL)
    return;

  Py_INCREF(&DrawBufferType);
  PyModule_AddObject(m, "DrawBuffer", (PyObject *)&DrawBufferType);
  
  Py_INCREF(&MaskBufferType);
  PyModule_AddObject(m, "MaskBuffer", (PyObject *)&MaskBufferType);
  
  PyModule_AddIntConstant(m, "BLEND_MODE_OVER", BLEND_MODE_OVER);
  PyModule_AddIntConstant(m, "BLEND_MODE_GROW_ALPHA", BLEND_MODE_GROW_ALPHA);
  
  Pycairo_IMPORT;
}
