#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Gdk
#from gi.repository import GLib

_style = """
GtkTreeView {
    -GtkTreeView-vertical-separator: 12;
}
"""

class SelectBox(Gtk.ToggleButton):
  """A popup menu selector, like a GtkComboBox but with fixed positioning."""
  
  __gsignals__ = {
    "changed" : (GObject.SignalFlags.RUN_LAST, GObject.TYPE_NONE, ())
  }
  
  def __init__(self):
    Gtk.ToggleButton.__init__(self)
    
    self.view  = None
    self.model = None
    self.view_attributes = {}
    
    self.cell_view = Gtk.CellView.new()
    box = Gtk.Box(Gtk.Orientation.HORIZONTAL)
    box.pack_start(self.cell_view, True, True, 0)
    box.pack_end(Gtk.Arrow(Gtk.ArrowType.DOWN, Gtk.ShadowType.NONE), False, False, 0)
    self.add(box)
    
    self.popup_window = Gtk.Window(type=Gtk.WindowType.POPUP)
    self.popup_window.set_focus_on_map(False)
    self.popup_window.set_can_focus(False)
    self.popup_window.set_attached_to(self)
    
    self.popup_treeview = Gtk.TreeView()
    self.popup_treeview.set_hover_selection(True)
    self.popup_treeview.get_selection().set_mode(Gtk.SelectionMode.BROWSE)
    style_context = self.popup_treeview.get_style_context()
    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(_style)
    style_context.add_provider(style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    
    self.popup_window.add(self.popup_treeview)
    
    self.connect("button-press-event", self.on_button_press)
    self.connect("button-release-event", self.on_button_release)
    self.connect("leave-notify-event", self.on_button_exit)
    
    self.popup_window.connect("hide", self.on_popup_hide)
    self.popup_window.connect("show", self.on_popup_show)
    self.popup_window.connect("leave-notify-event", self.on_popup_exit)
    self.popup_treeview.connect("button-release-event", self.on_popup_button_release)
  
  def set_view(self, view, attributes):
    self.view = view
    self.view_attributes = attributes
    self.cell_view.clear()
    self.cell_view.pack_start(view, True)
    
    self._update_cell_view()
  
  def set_model(self, model):
    self.model = model
    self.cell_view.set_model(model)
    self.popup_treeview.set_model(model)
    self.popup_treeview.set_headers_visible(False)
    
    self._update_cell_view()
  
  def _update_cell_view(self):
    if self.view is not None and self.model is not None:
      for k, v in self.view_attributes.items():
        self.cell_view.add_attribute(self.view, k, v)
      self.cell_view.set_displayed_row(self.model.get_path(self.model.get_iter_first()))
      
      column = self.popup_treeview.get_column(0)
      if column:
        self.popup_treeview.remove_column(column)
      column = Gtk.TreeViewColumn()
      column.pack_start(self.view, True)
      for k, v in self.view_attributes.items():
        column.add_attribute(self.view, k, v)
      self.popup_treeview.append_column(column)
  
  def on_button_press(self, widget, event):
    if self.popup_window.get_visible():
      self.popup_window.hide()
    else:
      self.popup_treeview.get_selection().select_path(self.cell_view.get_displayed_row())
      # Break the button's grab so moving to the menu with the mouse down will
      # work correctly.
      Gtk.get_current_event_device().ungrab(Gdk.CURRENT_TIME)
      self.popup_window.show_all()
      self.popup_window.grab_focus()
      self.popup_treeview.grab_focus()
    return True

  def on_button_release(self, widget, event):
    return True
  
  def on_button_exit(self, widget, event):
    if event.detail != Gdk.NotifyType.INFERIOR and self.popup_window.get_visible():
      allocation = self.get_allocation()
      if event.y >= allocation.height and (event.x >= 0 and event.x < allocation.width):
        # If the mouse left by going down, it's headed to our popup
        return
      else:
        # Otherwise close the popup
        self.popup_window.hide()
  
  def on_popup_exit(self, widget, event):
    if event.detail != Gdk.NotifyType.INFERIOR:
      self.popup_window.hide()
  
  def on_popup_show(self, widget):
    # Move to the correct location just before we display so sizes are correct
    x,y = self.get_window().get_origin()[1:]
    allocation = self.get_allocation()
    win_width, win_height = self.popup_window.get_size()
    
    self.popup_window.set_size_request(allocation.width, -1)
    
    if win_width < allocation.width:
      win_width = allocation.width
    
    x += int(allocation.width / 2 + allocation.x - win_width / 2 + 0.5)
    y += allocation.height + allocation.y
    
    self.popup_window.move(x, y)
    self.popup_window.set_transient_for(self.get_toplevel())
    
    # Call the ToggleButton function directly due to a name conflict when replicating
    # the GtkComboBox API.
    #self.set_active(True)
    Gtk.ToggleButton.set_active(self, True)
  
  def on_popup_hide(self, widget):
    # Call the ToggleButton function directly due to a name conflict when replicating
    # the GtkComboBox API.
    #self.set_active(False)
    Gtk.ToggleButton.set_active(self, False)
  
  def on_popup_button_release(self, widget, event):
    allocation = self.popup_treeview.get_allocation()
    if (event.x < 0 or event.x >= allocation.width or event.y < 0 or event.y >= allocation.height):
      # Mouse was released outside of the window, don't change anything
      return
      
    list_store, tree_iter = self.popup_treeview.get_selection().get_selected()
    self.cell_view.set_displayed_row(list_store.get_path(tree_iter))
    
    self.popup_window.hide()
    self.emit("changed")
  
  def set_active(self, index):
    "Set the active item by index, starting from 0"
    self.cell_view.set_displayed_row(Gtk.TreePath(index))
  
  def get_active(self):
    "Get the active item by index"
    return self.cell_view.get_displayed_row().get_indices()[0]
  
  def set_active_iter(self, iter):
    "Set the active item using a GtkTreeIter"
    self.cell_view.set_displayed_row(self.model.get_path(iter))
  
  def get_active_iter(self):
    "Get the active item by GtkTreeIter"
    return self.model.get_iter(self.cell_view.get_displayed_row())

if __name__ == "__main__":
  list_store = Gtk.ListStore(str, str)
  list_store.append(["Thing A", "Thing.A"])
  list_store.append(["Thing B", "Thing.B"])
  list_store.append(["Other C", "Other.C"])
  
  cell_renderer = Gtk.CellRendererText()
  
  w = Gtk.Window()
  w.set_default_size(200, 40)
  box = Gtk.Box(Gtk.Orientation.VERTICAL)
  
  b1 = SelectBox()
  b1.set_view(cell_renderer, {"text":0})
  b1.set_model(list_store)
  b1.connect("changed", lambda w : print("Changed:", w, w.get_active_iter()))
  box.pack_start(b1, False, False, 0)
  
  b2 = SelectBox()
  b2.set_model(list_store)
  b2.set_view(cell_renderer, {"text":0})
  b2.connect("changed", lambda w : print("Changed:", w, w.get_active_iter()))
  box.pack_start(b2, False, False, 0)
  
  w.add(box)
  w.connect("destroy", lambda widget : Gtk.main_quit())
  w.move(10, 10)
  w.show_all()
  
  b1.set_active(1)
  
  Gtk.main()
