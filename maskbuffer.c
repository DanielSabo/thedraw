#include "maskbuffer.h"

#include <structmember.h>
#include <pygobject.h>

#include <string.h>
#include <stdlib.h>
#include <math.h>

void
nearest_neighbor_sample(MaskBuffer *buf,
                        float x, float y,
                        float w, float h,
                        float *output)
{
  int stride = buf->width;
  
  int ix = x + w / 2;
  int iy = y + h / 2;
  
  if (ix < 0)
    ix = 0;
  else if (ix >= buf->width)
    ix = buf->width - 1;
  if (iy < 0)
    iy = 0;
  else if (iy >= buf->height)
    iy = buf->height - 1;
  
  *output = buf->data[iy * stride + ix];
}

void
nearest_neighbor_scale_to(MaskBuffer *from_buf, MaskBuffer *to_buf)
{
  float sample_width = (float)from_buf->width / (float)to_buf->width;
  float sample_height = (float)from_buf->height / (float)to_buf->height;
  
  float *pixel = to_buf->data;
  int ix, iy;
  
  for (iy = 0; iy < to_buf->height; ++iy)
  {
    for (ix = 0; ix < to_buf->width; ++ix)
    {
      nearest_neighbor_sample(from_buf,
                              ix * sample_width, iy * sample_height,
                              sample_width, sample_height,
                              pixel);
      pixel++;
    }
  }
}

float static inline
safe_bilinear_sample(MaskBuffer const *buf, float x, float y)
{
  int stride = buf->width;
  
  int x1;
  if (x < 0.0f)
    x1 = floorf(x);
  else
    x1 = (int)x;
  int x2 = x1+1;
  
  int y1;
  if (y < 0.0f)
    y1 = floorf(y);
  else
    y1 = (int)y;
  int y2 = y1+1;

  float in_pixel_00, in_pixel_01, in_pixel_10, in_pixel_11;

  if ((x1 < 0) || (y1 < 0) || (x1 >= buf->width) || (y1 >= buf->height))
    in_pixel_00 = 0.0f;
  else
    in_pixel_00 = buf->data[y1 * stride + x1];
  
  if ((x2 < 0) || (y1 < 0) || (x2 >= buf->width) || (y1 >= buf->height))
    in_pixel_01 = 0.0f;
  else
    in_pixel_01 = buf->data[y1 * stride + x2];
    
  if ((x1 < 0) || (y2 < 0) || (x1 >= buf->width) || (y2 >= buf->height))
    in_pixel_10 = 0.0f;
  else
    in_pixel_10 = buf->data[y2 * stride + x1];
    
  if ((x2 < 0) || (y2 < 0) || (x2 >= buf->width) || (y2 >= buf->height))
    in_pixel_11 = 0.0f;
  else
    in_pixel_11 = buf->data[y2 * stride + x2];

  float bottom = in_pixel_00 * (x2 - x) + in_pixel_01 * (x - x1);
  float top    = in_pixel_10 * (x2 - x) + in_pixel_11 * (x - x1);
  return bottom * (y2 - y) + top * (y - y1);
}

void
bilinear_scale_to(MaskBuffer const *from_buf, MaskBuffer *to_buf,
                  float left_pad, float right_pad,
                  float top_pad, float bottom_pad)
{
  float sample_width = (float)from_buf->width / ((float)to_buf->width - left_pad - right_pad);
  float sample_height = (float)from_buf->height / ((float)to_buf->height - top_pad - bottom_pad);

  float *pixel = to_buf->data;
  float x, y;
  int ix, iy;
  int stride = from_buf->width;
  
  /* Sub zero loop */
  y = -0.5 + (sample_height / 2) - (sample_height * bottom_pad);
  iy = 0;
  for (; y < 0; ++iy, y += sample_height)
  {
    x = -0.5 + (sample_width / 2) - (sample_width * left_pad);

    for (ix = 0; ix < to_buf->width; ++ix)
    {
      *pixel = safe_bilinear_sample(from_buf, x, y);

      pixel++;
      x += sample_width;
    }
  }
  /* Fast loop */
  while(iy < to_buf->height && y < from_buf->height - 1)
  {
    x = -0.5 + (sample_width / 2) - (sample_width * left_pad);
    ix = 0;

    while (x < 0)
    {
      *pixel = safe_bilinear_sample(from_buf, x, y);

      pixel++;
      ix++;
      x += sample_width;
    }
    while(ix < to_buf->width && x < from_buf->width - 1)
    {
      int x1 = (int)(x);
      int x2 = x1 + 1;
      int y1 = (int)(y);
      int y2 = y1 + 1;

      float bottom = from_buf->data[y1 * stride + x1] * (x2 - x) +
                     from_buf->data[y1 * stride + x2] * (x - x1);
      float top    = from_buf->data[y2 * stride + x1] * (x2 - x) +
                     from_buf->data[y2 * stride + x2] * (x - x1);
      *pixel = bottom * (y2 - y) + top * (y - y1);

      pixel++;
      ix++;
      x += sample_width;
    }
    while(ix < to_buf->width)
    {
      *pixel = safe_bilinear_sample(from_buf, x, y);

      pixel++;
      ix++;
      x += sample_width;
    }

    iy++;
    y += sample_height;
  }
  /* Cleanup loop */
  for (; iy < to_buf->height; ++iy)
  {
    x = -0.5 + (sample_width / 2) - (sample_width * left_pad);

    for (ix = 0; ix < to_buf->width; ++ix)
    {
      *pixel = safe_bilinear_sample(from_buf, x, y);

      pixel++;
      x += sample_width;
    }

    y += sample_height;
  }
}

PyObject *
MaskBuffer_scale_to(MaskBuffer *self, PyObject *args)
{
  MaskBuffer *target;

  float left_pad = 0.0f;
  float right_pad = 0.0f;
  float top_pad = 0.0f;
  float bottom_pad = 0.0f;

  if (!PyArg_ParseTuple(args, "O!|ffff", &MaskBufferType, &target,
                        &left_pad, &right_pad, &top_pad, &bottom_pad)) {
    return NULL;
  }

  if (left_pad < 0.0f || right_pad < 0.0f || top_pad < 0.0f || bottom_pad < 0.0f)
  {
    PyErr_SetString(PyExc_ValueError, "Paddings must be positive values");
    return NULL;
  }

  if (left_pad + right_pad >= target->width ||
      top_pad + bottom_pad >= target->height)
  {
    memset(target->data, 0, sizeof(float) * target->width * target->height);
  }
  else
  {
    bilinear_scale_to(self, target, left_pad, right_pad, top_pad, bottom_pad);
  }

  Py_RETURN_NONE;
}

PyObject *
MaskBuffer_scale_with_size(MaskBuffer *self, PyObject *args)
{
  float offset_x = 0.0f;
  float offset_y = 0.0f;
  float width    = 0.0f;
  float height   = 0.0f;

  if (!PyArg_ParseTuple(args, "ffff", &offset_x, &offset_y, &width, &height)) {
    return NULL;
  }

  if (offset_x < 0.0f || offset_y < 0.0f)
  {
    PyErr_SetString(PyExc_ValueError, "Offsets must be positive values");
    return NULL;
  }

  if (width <= 0.0f)
  {
    PyErr_SetString(PyExc_ValueError, "Invalid width");
    return NULL;
  }

  if (height <= 0.0f)
  {
    PyErr_SetString(PyExc_ValueError, "Invalid height");
    return NULL;
  }

  float full_width  = ceilf(width);
  float full_height = ceilf(height);

  float width_fract  = full_width  - width;
  float height_fract = full_height - height;

  float left_pad   = width_fract  / 2.0f;
  float bottom_pad = height_fract / 2.0f;

  float right_pad = left_pad;
  float top_pad   = bottom_pad;

  if (offset_x != 0.0f)
  {
    if (offset_x < right_pad) {
      left_pad  += offset_x;
      right_pad -= offset_x;
    }
    else {
      left_pad   += offset_x;
      right_pad  -= offset_x - 1;
      full_width += 1;
    }
  }

  if (offset_y != 0.0f)
  {
    if (offset_y < top_pad) {
      bottom_pad += offset_y;
      top_pad    -= offset_y;
    }
    else {
      bottom_pad  += offset_y;
      top_pad     -= offset_y - 1;
      full_height += 1;
    }
  }

  /* Allocate the result buffer */
  MaskBuffer *target = (MaskBuffer *)PyType_GenericNew(&MaskBufferType, NULL, NULL);
  target->width  = full_width;
  target->height = full_height;
  target->data_size = sizeof(float) * target->width * target->height;
  target->data = malloc(target->data_size);

  bilinear_scale_to(self, target, left_pad, right_pad, top_pad, bottom_pad);

  return (PyObject *)target;
}

PyObject *
MaskBuffer_set_size(MaskBuffer *self, PyObject *args)
{
  int new_width, new_height;

  if (!PyArg_ParseTuple(args, "ii", &new_width, &new_height)) {
    return NULL;
  }

  if (new_width * new_height * sizeof(float) <= self->data_size) //FIXME: Don't hardcode float
  {
    self->width  = new_width;
    self->height = new_height;
  }
  else
  {
    PyErr_SetString(PyExc_ValueError, "New size doesn't fit in existing data buffer");
    return NULL;
  }

  Py_RETURN_NONE;
}

PyObject *
maskbuffer_from_png(PyObject *self, PyObject *args)
{
  char *path;

  if (!PyArg_ParseTuple(args, "s", &path)) {
    return NULL;
  }

  GeglNode *ptn, *src;

  ptn = gegl_node_new();
  src = gegl_node_new_child(ptn, "operation", "gegl:load",
                            "path", path,
                            NULL);

  GeglRectangle extent = gegl_node_get_bounding_box(src);

  MaskBuffer *maskbuf = (MaskBuffer *)PyType_GenericNew(&MaskBufferType, NULL, NULL);
  maskbuf->width = extent.width;
  maskbuf->height = extent.height;

  const Babl *buffer_format = babl_format("Y float");
  maskbuf->data_size = babl_format_get_bytes_per_pixel(buffer_format) * maskbuf->width * maskbuf->height;

  maskbuf->data = malloc(maskbuf->data_size);

  GeglRectangle roi;
  roi.x = 0;
  roi.y = 0;
  roi.width = maskbuf->width;
  roi.height = maskbuf->height;

  gegl_node_blit(src, 1.0, &roi, buffer_format, maskbuf->data, GEGL_AUTO_ROWSTRIDE, GEGL_BLIT_DEFAULT);

  int num_pixels = maskbuf->width * maskbuf->height;
  float *pixel = maskbuf->data;
  while (num_pixels--) {
    *pixel = 1.0 - *pixel;
    pixel++;
  }

  g_object_unref(ptn);

  return (PyObject *)maskbuf;
}

PyObject *
MaskBuffer_get_pixel(MaskBuffer *self, PyObject *args)
{
  int x, y;
  
  if (!PyArg_ParseTuple(args, "ii", &x, &y)) {
    return NULL;
  }
  
  if ((x >= self->width) || (y >= self->height))
  {
    PyErr_SetString(PyExc_IndexError, "Coordinates out of bounds");
    return NULL;
  }
  
  return PyFloat_FromDouble(self->data[y * self->width + x]);
}

PyObject *
MaskBuffer_set_pixel(MaskBuffer *self, PyObject *args)
{
  int x, y;
  float value;
  
  if (!PyArg_ParseTuple(args, "iif", &x, &y, &value)) {
    return NULL;
  }
  
  if ((x >= self->width) || (y >= self->height))
  {
    PyErr_SetString(PyExc_IndexError, "Coordinates out of bounds");
    return NULL;
  }
  
  if ((value < 0.0f) || (value > 1.0f))
  {
    PyErr_SetString(PyExc_ValueError, "Pixel values must be between 0.0 and 1.0");
    return NULL;
  }
  
  self->data[y * self->width + x] = value;
  
  Py_RETURN_NONE;
}

static PyObject *
MaskBuffer_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
  MaskBuffer *self;

  self = (MaskBuffer *)type->tp_alloc(type, 0);
  
  if (self == NULL) {
    return NULL;
  }
  
  self->width = 0;
  self->height = 0;
  self->data = NULL;

  return (PyObject *)self;
}

static int
MaskBuffer_init(MaskBuffer *self, PyObject *args, PyObject *kwds)
{
  if (!PyArg_ParseTuple(args, "ii", &self->width, &self->height)) {
    return -1;
  }
  
  if (self->width <= 0)
  {
    self->ob_type->tp_free((PyObject*)self);
    PyErr_SetString(PyExc_ValueError, "Invalid width");
    return -1;
  }

  if (self->height <= 0)
  {
    self->ob_type->tp_free((PyObject*)self);
    PyErr_SetString(PyExc_ValueError, "Invalid height");
    return -1;
  }


  const Babl *buffer_format = babl_format("Y float");
  const size_t bytesize   = babl_format_get_bytes_per_pixel(buffer_format);
  const size_t num_pixels = self->width * self->height;
  self->data_size = bytesize * num_pixels;
  self->data = calloc(num_pixels, bytesize);
  
  return 0;
}

static void
MaskBuffer_dealloc(MaskBuffer* self)
{
  if (self->data)
  {
    free(self->data);
  }
  self->ob_type->tp_free((PyObject*)self);
}

static PyMemberDef MaskBuffer_members[] = {
    {"width", T_INT, offsetof(MaskBuffer, width), READONLY,
     "pixel width"},
    {"height", T_INT, offsetof(MaskBuffer, height), READONLY,
     "pixel height"},
    {NULL}  /* Sentinel */
};

static PyObject *
MaskBuffer_get_gegl(MaskBuffer* self)
{
  GeglRectangle roi;
  roi.x = 0;
  roi.y = 0;
  roi.width = self->width;
  roi.height = self->height;
  
  const Babl *buffer_format = babl_format("Y float");
  GeglBuffer *buffer = gegl_buffer_linear_new_from_data(self->data,
                                                        buffer_format,
                                                        &roi,
                                                        0,
                                                        NULL,
                                                        NULL);

  PyObject *retval = pygobject_new(G_OBJECT(buffer));
  g_object_unref(buffer);
  
  return retval;
}


static PyMethodDef MaskBuffer_methods[] = {
    {"get_gegl", (PyCFunction)MaskBuffer_get_gegl, METH_NOARGS,
     "get_gegl() -> GeglBuffer"
    },
    {"get_pixel", (PyCFunction)MaskBuffer_get_pixel, METH_VARARGS,
     "get_pixel(x, y) -> float"
    },
    {"scale_to", (PyCFunction)MaskBuffer_scale_to, METH_VARARGS,
     "scale_to(MaskBuffer target) -> None"
    },
    {"scale_with_size", (PyCFunction)MaskBuffer_scale_with_size, METH_VARARGS,
     "scale_with_size(offset_x, offset_y, width, height, center_odd) -> MaskBuffer"
    },
    {"set_pixel", (PyCFunction)MaskBuffer_set_pixel, METH_VARARGS,
     "set_pixel(x, y, value)"
    },
    {"set_size", (PyCFunction)MaskBuffer_set_size, METH_VARARGS,
     "set_size(width, height) -> None\nTotal area must be smaller than the originally allocated buffer."
    },
    {NULL}  /* Sentinel */
};

PyTypeObject MaskBufferType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "drawbuffer.MaskBuffer",       /*tp_name*/
    sizeof(MaskBuffer),             /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)MaskBuffer_dealloc, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT,        /*tp_flags*/
    "",                        /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    MaskBuffer_methods,             /* tp_methods */
    MaskBuffer_members,             /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)MaskBuffer_init,      /* tp_init */
    0,                         /* tp_alloc */
    MaskBuffer_new,                 /* tp_new */
};
