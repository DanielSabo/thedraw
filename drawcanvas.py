#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import math
import time
import cairo

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
#from gi.repository import GdkPixbuf
from gi.repository import Gegl

import drawbuffer
import layers
import ora
import paintcursor
import resourceloader

from util import rect_union, rect_intersect

class UndoRegion():
  def __init__(self, target_layer, x, y, w, h):
    self.target_layer = target_layer
    self.buf = drawbuffer.DrawBuffer(w, h)
    self.x   = x
    self.y   = y
    self.w   = w
    self.h   = h
    
    target_layer.drawbuf.blit_to(self.buf, 0, 0, (x, y, w, h))

  def apply(self, canvas):
    redo = UndoRegion(self.target_layer, self.x, self.y, self.w, self.h)
    self.buf.blit_to(self.target_layer.drawbuf, self.x, self.y, (0, 0, self.w, self.h))
    return redo, self.x, self.y, self.w, self.h

class UndoLayers():
  def __init__(self, canvas, pre, post, target_layer):
    self.target_layer = target_layer
    self.pre  = pre[:]
    self.post = post[:]
  
  def apply(self, canvas):
    redo = UndoLayers(canvas, self.post, self.pre, self.target_layer)
    canvas._replace_layers(self.pre)
    return redo, self.target_layer.x, self.target_layer.y, self.target_layer.width, self.target_layer.height

class UndoLayerProperty():
  def __init__(self, target_layer, prop_name, old_value, new_value):
    self.target_layer = target_layer
    self.prop_name = prop_name
    self.new_value = new_value
    self.old_value = old_value
  
  def apply(self, canvas):
    redo = UndoLayerProperty(self.target_layer, self.prop_name, self.new_value, self.old_value)
    self.target_layer.set_property(self.prop_name, self.old_value)
    canvas.notify("layers")
    return redo, self.target_layer.x, self.target_layer.y, self.target_layer.width, self.target_layer.height

class DrawCanvasBackground_Checkerboard():
  def __init__(self):
    pass
  
  def get_tile_size(self):
    return (64, 64)
  
  def get_node(self, ptn):
    node = ptn.create_child("gegl:checkerboard")
    node.set_property("color1", Gegl.Color.new("#aaaaaa"))
    node.set_property("color2", Gegl.Color.new("#cccccc"))
    node.set_property("x", 8.0)
    node.set_property("y", 8.0)
    
    return node

class DrawCanvasBackground_Color():
  def __init__(self, color):
    self.color = Gegl.Color.new(color)
  
  def get_tile_size(self):
    return (64, 64)
  
  def get_node(self, ptn):
    node = ptn.create_child("gegl:color")
    node.set_property("value", self.color)
    
    return node

class TiledRegion():
  def __init__(self, tile_width = 128, tile_height = 64):
    self.tiles = {}
    self.tile_width = tile_width
    self.tile_height = tile_height
  
  def __len__(self):
    return len(self.tiles)
  
  def pop_tile(self):
    if self.tiles:
      return self.tiles.pop(self.sorted_list.pop())
    return None
  
  def set_rect(self, x, y, w, h):
    self.tiles = {}
    self.add_rect(x, y, w, h)
      
  def add_rect(self, x, y, w, h):
    x_max = x + w
    y_max = y + h
  
    x = (x // self.tile_width) * self.tile_width
    y = (y // self.tile_height) * self.tile_height
    
    x_tmp = x
    while x_tmp < x_max:
      y_tmp = y
      while y_tmp < y_max:
        self.tiles[(x_tmp, y_tmp)] = (x_tmp, y_tmp, x_tmp+self.tile_width, y_tmp+self.tile_height)
        y_tmp += self.tile_height
      x_tmp += self.tile_width
    self.sorted_list = sorted(self.tiles.keys(), lambda a, b : b[1] - a[1] or b[0] - a[0] )

class SignalSet():
  """Connects to the same signal on a group of objects, when an object
     is removed from the set it's signal is disconnected."""
  
  def __init__(self, sig_name, target_function):
    self.sig_name = sig_name
    self.target_function = target_function
    self.current_set = []
  
  def set_list(self, l):
    for i in self.current_set:
      obj, sig_id = i
      obj.disconnect(sig_id)
    
    self.current_set = []
    for obj in l:
      sig_id = obj.connect(self.sig_name, self.target_function)
      self.current_set.append((obj, sig_id))
  
  def __del__(self):
    self.set_list([])

class MouseActionMapping():
  """Provides tracking for modifier keys as they relate to mouse button clicks."""
  
  def __init__(self):
    self.state = Gdk.ModifierType(0)
    self.tracked_mask = Gdk.ModifierType(0)
    self.actions = dict()
    self.state_callback = None
    
    try:
      self._add_virtual_modifiers(0)
      self.key_to_mod = {
        Gdk.KEY_Control_L : Gdk.ModifierType.CONTROL_MASK,
        Gdk.KEY_Control_R : Gdk.ModifierType.CONTROL_MASK,
        Gdk.KEY_Shift_L   : Gdk.ModifierType.SHIFT_MASK,
        Gdk.KEY_Shift_R   : Gdk.ModifierType.SHIFT_MASK,
        Gdk.KEY_Alt_L     : Gdk.ModifierType.MOD1_MASK,
        Gdk.KEY_Alt_R     : Gdk.ModifierType.MOD1_MASK,
        Gdk.KEY_Meta_L    : Gdk.ModifierType.META_MASK,
        Gdk.KEY_Meta_R    : Gdk.ModifierType.META_MASK,
      }
    except TypeError:
      # Due to a broken binding of add_virtual_modifiers
      # we can't handle META with GTK 3.4
    
      self._add_virtual_modifiers = lambda state : state
      
      self.key_to_mod = {
        Gdk.KEY_Control_L : Gdk.ModifierType.CONTROL_MASK,
        Gdk.KEY_Control_R : Gdk.ModifierType.CONTROL_MASK,
        Gdk.KEY_Shift_L   : Gdk.ModifierType.SHIFT_MASK,
        Gdk.KEY_Shift_R   : Gdk.ModifierType.SHIFT_MASK,
        Gdk.KEY_Alt_L     : Gdk.ModifierType.MOD1_MASK,
        Gdk.KEY_Alt_R     : Gdk.ModifierType.MOD1_MASK,
      }
  
  def _add_virtual_modifiers(self, state):
    return Gdk.Keymap.get_default().add_virtual_modifiers(state)

  def clear(self):
    self.actions = dict()
  
  def set_tracked_mask(self, mask):
    self.tracked_mask = Gdk.ModifierType(mask)
  
  def set_action(self, button, mod, action):
    self.actions[(button, mod)] = action
  
  def set_state_callback(self, callback=None):
    self.state_callback = callback
  
  def get_action(self, button):
    state = self.state & self.tracked_mask
    state = (button, state)
    if state in self.actions:
      return self.actions[state]
    return None
  
  def process_event(self, event):
    last_state = self.state & self.tracked_mask
    
    if event.type == Gdk.EventType.KEY_PRESS:
      self.state |= self.key_to_mod.get(event.keyval, Gdk.ModifierType(0))
  
    elif event.type == Gdk.EventType.KEY_RELEASE:
      self.state &= ~(self.key_to_mod.get(event.keyval, Gdk.ModifierType(0)))
  
    elif event.type == Gdk.EventType.MOTION_NOTIFY or \
         event.type == Gdk.EventType.BUTTON_PRESS  or \
         event.type == Gdk.EventType.BUTTON_RELEASE:
      self.state = self._add_virtual_modifiers(event.state)

    self.state = Gdk.ModifierType(self.state)
  
    if last_state != self.state & self.tracked_mask:
      if self.state_callback:
        self.state_callback()

class CanvasAction_Paint():
  pass

class CanvasAction_PickColor():
  pass

class CanvasAction_MoveLayer():
  pass

class CanvasAction_MoveView():
  def __init__(self):
    self.last_pos = None

class DrawCanvas(Gtk.DrawingArea):
  __gproperties__ = {
    'brush' : (GObject.TYPE_PYOBJECT,
        'brush',
        'The active brush',
        GObject.PARAM_READWRITE),
    'layers' : (GObject.TYPE_PYOBJECT,
        'layers list',
        '',
        GObject.PARAM_READABLE),
    'scale' : (GObject.TYPE_FLOAT,
        'scale',
        'Canvas zoom',
        0.125,
        16.0,
        1.0,
        GObject.PARAM_READWRITE),
    'max-undo-steps' : (GObject.TYPE_INT,
        'max undo steps',
        'Maximum number of undo steps stored',
        0,
        1024,
        128,
        GObject.PARAM_READWRITE),
    'canvas-background' : (GObject.TYPE_PYOBJECT,
        'canvas-background',
        '',
        GObject.PARAM_READWRITE),
    'filename' : (GObject.TYPE_PYOBJECT,
        'filename',
        'Path the canvas was last loaded from or saved to',
        GObject.PARAM_READWRITE),
    'modified' : (GObject.TYPE_BOOLEAN,
        'modified',
        'True if the canvas has changed since it was last saved',
        False,
        GObject.PARAM_READWRITE),
    }
  
  #__gsignals__ = {}

  def __init__(self):
    Gtk.DrawingArea.__init__(self)
    
    self.debug__print_render_time = False
    
    self.max_undo_steps = 128
    self.save_filename = None
    self.canvas_modified = False
    
    self.view_offset = [0, 0]
    self.soft_move = [0, 0]
    self.dirty_rect = None
    self.dirty_tiles = TiledRegion(128*4, 64*4)
    self.idle_render_source_id = None

    self.current_action = None
    self.action_map = MouseActionMapping()
    self.action_map.set_tracked_mask(Gdk.ModifierType.CONTROL_MASK |
                                     Gdk.ModifierType.SHIFT_MASK   |
                                     Gdk.ModifierType.META_MASK)
    self.action_map.set_action(1, 0, CanvasAction_Paint)
    self.action_map.set_action(1, Gdk.ModifierType.CONTROL_MASK, CanvasAction_PickColor)
    self.action_map.set_action(1, Gdk.ModifierType.META_MASK, CanvasAction_MoveLayer)
    self.action_map.set_action(2, 0, CanvasAction_MoveLayer)
    self.action_map.set_state_callback(self.update_cursor)
    
    self.brush = None
    self.brush_modify_signal = None
    self.current_stroke = None
    self.current_stroke_bounds = None
    self.scale = 1.0
    self.canvas_background_surface_tile = None
    self.canvas_background = DrawCanvasBackground_Checkerboard()
    self._update_background_tile()
    self.layer_name_incr = 1
    
    self.layer_signals = SignalSet("graph", lambda l : self.invalidate())
    self.connect("notify::layers", lambda o, p : self.layer_signals.set_list(self.layers))
    self.connect("notify::layers", lambda o, p : self._update_drawing_size())
    
    self.new_drawing()
    
    self.connect("draw", self.on_draw)
    self.connect("realize", self.on_widget_realize)
    
    self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK   |
                    Gdk.EventMask.POINTER_MOTION_MASK |
                    Gdk.EventMask.BUTTON_RELEASE_MASK)

    self.add_events(Gdk.EventMask.KEY_PRESS_MASK |
                    Gdk.EventMask.KEY_RELEASE_MASK)

    self.add_events(Gdk.EventMask.SCROLL_MASK)
    self.add_events(Gdk.EventMask.SMOOTH_SCROLL_MASK)

    self.connect("button-press-event", self.on_button_press)
    self.connect("button-release-event", self.on_button_release)
    self.connect("motion-notify-event", self.on_motion)
    self.connect("key-press-event", self.on_key_press)
    self.connect("key-release-event", self.on_key_release)
    self.connect("scroll-event", self.on_scroll_event)
    self.connect("destroy", self.on_destroy)

    self.set_can_focus(True)
    
    self.set_property("modified", False)
    self.undo_history = []
    self.redo_history = []

  def invalidate(self):
    self.invalidate_tiles(self.drawing_origin_x, self.drawing_origin_y, self.drawing_width, self.drawing_height)
  
  def invalidate_tiles(self, x, y, w, h):
    self.output_scaled = None
    
    if not self.idle_render_source_id:
      self.idle_render_source_id = GLib.idle_add(self.idle_render)
    self.__dirty_tiles_t0 = None
    self.dirty_tiles.add_rect(x, y, w, h)
    
  def invalidate_area(self, x, y, w, h):
    """Invalidate the cairo sufrace and graph for the given rect in canvas coordinated"""
    self.output_scaled = None
    
    x_int = int(math.floor(x))
    y_int = int(math.floor(y))
    w_int = int(math.ceil(w))
    h_int = int(math.ceil(h))
    
    # Mark the graph dirty
    if self.dirty_rect:
      x0, y0, x1, y1 = self.dirty_rect
      self.dirty_rect = (min(x_int, x0), min(y_int, y0), max(x_int+w_int, x1), max(y_int+h_int, y1))
    else:
      self.dirty_rect = (x_int, y_int, x_int+w_int, y_int+h_int)
    
    # Mark the widget dirty
    self.queue_draw_area(*self._canvas_to_widget_rect(x, y, w, h))

  def zoom_to_full(self):
    allocation = self.get_allocation()
    x_scale = float(allocation.width)  / float(self.drawing_width)
    y_scale = float(allocation.height) / float(self.drawing_height)
    scale = min(x_scale, y_scale)
    
    # Don't zoom out if scale is already greater than 1
    if self.scale <= 1.0:
      if scale > 1.0:
        scale = 1.0
    else:
      scale = min(scale, self.scale)
    
    self.view_offset[0] = -self.drawing_origin_x * scale
    self.view_offset[1] = -self.drawing_origin_y * scale
    
    self.view_offset[0] += (allocation.width - self.drawing_width * scale) / 2
    self.view_offset[1] += (allocation.height - self.drawing_height * scale) / 2
    
    self.view_offset = map(int, self.view_offset)

    self.set_property("scale", scale)
    self.queue_draw()

  def _next_layer_name(self):
    name = "Layer %02d" % self.layer_name_incr
    self.layer_name_incr += 1
    return name

  def new_drawing_from_filename(self, filename):
    self.layer_name_incr = 1
    
    #FIXME: Check mime type not just the extension
    if filename.endswith(".png"):
      drawbuf = drawbuffer.drawbuffer_from_png(filename)
      self._new_drawing_from_layers([layers.Layer(drawbuf)])
    elif filename.endswith(".ora"):
      width, height, layer_list = ora.read_ora(filename)
      #TODO: Scan layer names and update self.layer_name_incr
      self._new_drawing_from_layers(layer_list)
      self.set_property("filename", filename)
    else:
      raise ValueError("Could not determine file type")

  def new_drawing(self):
    self.layer_name_incr = 1
  
    drawbuf = drawbuffer.DrawBuffer(0, 0)
    new_layer = layers.Layer(drawbuf, name=self._next_layer_name())
    self._new_drawing_from_layers([new_layer])
  
  def _new_drawing_from_layers(self, layer_list):
    #FIXME: This should try to get the correct value for self.layer_name_incr
    
    self.set_property("filename", None)
    self.set_property("modified", False)
    self.undo_history = []
    self.redo_history = []
    
    self.drawing_origin_x = 0
    self.drawing_origin_y = 0
    self.drawing_width = 0
    self.drawing_height = 0
    
    self.output_buffer = Gegl.Buffer.new("cairo-ARGB32", -10000, -10000, 20000, 20000)
    self.output_scaled = None
    
    self.current_stroke_layer = layers.Layer(x=-10000, y=-10000, width=20000, height=20000)
    self.current_stroke_layer.name = "Hidden Stroke Layer"
        
    self.output_node  = None
    self.output_graph = None
    self.current_layer = None
    self.current_layer_translate_node = None
    self.layers = layer_list
    self.set_current_layer(self.layers[0])
    
    self._update_drawing_size()
    
    self.invalidate()
    self.queue_draw()
    
    self.notify("layers")
    
  def _update_drawing_size(self):
    if not self.layers:
      return (0, 0, 0, 0)
  
    l = self.layers[0]
    if l is self.current_layer and self.soft_move:
      rect = (l.x + self.soft_move[0], l.y + self.soft_move[1], l.width, l.height)
    else:
      rect = (l.x, l.y, l.width, l.height)
    
    for l in self.layers[1:]:
      lrect = (l.x, l.y, l.width, l.height)
      if l is self.current_layer and self.soft_move:
        lrect = (l.x + self.soft_move[0], l.y + self.soft_move[1], l.width, l.height)
      rect = rect_union(rect, lrect)
        
    self.drawing_origin_x = rect[0]
    self.drawing_origin_y = rect[1]
    self.drawing_width  = rect[2]
    self.drawing_height = rect[3]
    
  def _canvas_to_widget_rect(self, x, y, w, h):
    x_offset, y_offset = map(int, self.view_offset)

    # Include all the pixels this rect influences.
    if self.scale < 1.0:
      pixel_size = max(1.0 / self.scale, 1.0)
    else:
      pixel_size = 0.0

    x = int(math.floor(x * self.scale - pixel_size))
    y = int(math.floor(y * self.scale - pixel_size))
    w = int(math.ceil(w * self.scale + 2 * pixel_size))
    h = int(math.ceil(h * self.scale + 2 * pixel_size))
    return (x+x_offset, y+y_offset, w, h)
  
  def _widget_to_canvas_xy(self, x, y):
    x_offset, y_offset = map(int, self.view_offset)
    
    x = (x - x_offset) / self.scale
    y = (y - y_offset) / self.scale
    
    return x, y
        
  def _build_output_graph(self):
    ptn = Gegl.Node()
    GObject.Object.set_property(ptn, "dont-cache", True)
    
    trans_node = None
    
    # Create the empty background
    #last = self.canvas_background.get_node(ptn)
    # Let cairo handle the background
    last = None
    
    for l in self.layers:
      if l == self.current_layer and self.current_stroke_layer:
        stroke_node = self.current_stroke_layer.get_graph(ptn)
        trans_node = ptn.create_child("gegl:translate")
        trans_node.set_property("sampler", "nearest")
        trans_node.set_property("x", 0)
        trans_node.set_property("y", 0)
        trans_node.connect_to("output", stroke_node, "input")
        layer_node = l.get_graph(ptn, trans_node, stroke_node)
      else:
        layer_node = l.get_graph(ptn)
      
      if last:
        last.connect_to("output", layer_node, "input")
      last = layer_node
    #GObject.Object.set_property(last, "dont-cache", False)
    
    self.current_layer_translate_node = trans_node
    self.output_node  = last
    self.output_graph = ptn
  
  def _update_background_tile(self):
    if self.canvas_background:
      w, h = self.canvas_background.get_tile_size()
      surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, h)

      ptn = Gegl.Node()
      GObject.Object.set_property(ptn, "dont-cache", True)
      dst = drawbuffer.gegl_cairo_sink_node(ptn, surface)
      src = self.canvas_background.get_node(ptn)
      
      src.connect_to("output", dst, "input")
      
      rect = Gegl.Rectangle()
      rect.x, rect.y, rect.width, rect.height = 0, 0, w, h
      dst.blit_buffer(None, rect)
      self.canvas_background_surface_tile = surface
    else:
      self.canvas_background_surface_tile = None
  
  def save_drawing_to_ora(self, filename):
    ora.write_ora(self.layers, filename)
    self.set_property("modified", False)
    self.set_property("filename", filename)
  
  def save_drawing_to_png(self, filename, sixteen_bit=True, with_background=True):
    ptn = Gegl.Node()
    GObject.Object.set_property(ptn, "dont-cache", True)
    dst = ptn.create_child("gegl:png-save")
    dst.set_property("compression", 8)
    dst.set_property("path", filename)
    if sixteen_bit:
      dst.set_property("bitdepth", 16)
    else:
      dst.set_property("bitdepth", 8)
    
    if with_background:
      last = self.canvas_background.get_node(ptn)
    else:
      last = ptn.create_child("gegl:nop")
    
    for l in self.layers:
      if l == self.current_layer and self.current_stroke_layer:
        stroke_node = self.current_stroke_layer.get_graph(ptn)
        layer_node = l.get_graph(ptn, stroke_node, stroke_node)
      else:
        layer_node = l.get_graph(ptn)
      
      if last:
        last.connect_to("output", layer_node, "input")
      last = layer_node
    
    crop = ptn.create_child("gegl:crop")
    crop.set_property("x", self.drawing_origin_x)
    crop.set_property("y", self.drawing_origin_y)
    crop.set_property("width", self.drawing_width)
    crop.set_property("height", self.drawing_height)
    last.connect_to("output", crop, "input")
    crop.connect_to("output", dst, "input")
    
    dst.process()
  
  def _replace_layers(self, layers_list):
    """Fully replace the layers list, this is intended for use by undo operations"""
    self.layers = layers_list[:]
    if not self.current_layer in self.layers:
      self.current_layer = None
    self._build_output_graph()
    self.invalidate()
    
    self.notify("layers")
  
  def _replace_layer(self, target_layer, new_layer):
    pos = self.layers.index(target_layer)
        
    old_layers = self.layers[:]
    self.layers = self.layers[:pos] + [new_layer] + self.layers[pos+1:]
    
    self.add_undo_step(UndoLayers(self, old_layers, self.layers, new_layer))
    
    if target_layer is self.current_layer:
      self.set_current_layer(new_layer)
    else:
      self._build_output_graph()
    self.invalidate()
    
    self.notify("layers")
    
    return new_layer
  
  def new_layer(self, pos):
    """Add a new layer above pos."""
    if pos >= len(self.layers):
      raise IndexError("Invalid layer position")
    if pos < 0:
      raise IndexError("Invalid layer position")
    new_layer = layers.Layer(width=0,
                             height=0,
                             name=self._next_layer_name())
    
    old_layers = self.layers[:]
    self.layers = self.layers[:pos+1] + [new_layer] + self.layers[pos+1:]
    self._build_output_graph()
    self.invalidate()
    
    self.add_undo_step(UndoLayers(self, old_layers, self.layers, new_layer))
    
    self.notify("layers")
    
    return new_layer
  
  def insert_layer(self, target_layer, new_layer):
    """Insert new_layer into the layer list above target_layer"""
    if target_layer:
      pos = self.layers.index(target_layer)
    else:
      pos = -1
      
    old_layers = self.layers[:]
    self.layers = self.layers[:pos+1] + [new_layer] + self.layers[pos+1:]
    self._build_output_graph()
    self.invalidate()
    
    self.add_undo_step(UndoLayers(self, old_layers, self.layers, new_layer))
    
    self.notify("layers")
    
    return new_layer
  
  def move_layer(self, target_layer, new_pos):
    """Move layer to new_pos, existing layers will be shifted up"""
    pos = self.layers.index(target_layer)
    
    if new_pos < 0:
      new_pos = 0
    
    old_layers = self.layers[:]
    del self.layers[pos]
    self.layers = self.layers[:new_pos] + [target_layer] + self.layers[new_pos:]
    
    self.add_undo_step(UndoLayers(self, old_layers, self.layers, target_layer))
    
    self._build_output_graph()
    self.notify("layers")
    self.invalidate()
  
  def del_layer(self, target_layer):
    if len(self.layers) == 1:
      return
    
    pos = self.layers.index(target_layer)
    
    if self.current_layer == self.layers[pos]:
      self.current_layer = None
    
    old_layers = self.layers[:]
    del self.layers[pos]
    
    self.add_undo_step(UndoLayers(self, old_layers, self.layers, target_layer))
    
    if not self.current_layer:
      if pos >= len(self.layers):
        pos = -1
      self.set_current_layer(self.layers[pos])
    self._build_output_graph()
    self.notify("layers")
    self.invalidate()
  
  def merge_down_layer(self, target_layer):
    if len(self.layers) == 1:
      return
    
    pos = self.layers.index(target_layer)
    if pos == 0:
      return
    
    if self.current_layer == self.layers[pos]:
      self.current_layer = None      
    elif self.current_layer == self.layers[pos-1]:
      self.current_layer = None

    old_layers = self.layers[:]
    
    top_layer = self.layers[pos]
    bottom_layer = self.layers[pos - 1]
    merged_layer = bottom_layer.duplicate()
    merged_layer.name = bottom_layer.name
    top_layer.merge_down(merged_layer, expand=True)
    
    #print("merging", top_layer, "onto", bottom_layer, "->", merged_layer)
    #print("layers are", self.layers)
    #print("stack is",pos, self.layers[:pos-1], [merged_layer], self.layers[pos+1:])
    self.layers = self.layers[:pos-1] + [merged_layer] + self.layers[pos+1:]
  
    #FIXME: This should take a rect rather that using target_layer for the size  
    self.add_undo_step(UndoLayers(self, old_layers, self.layers, target_layer))
    
    if not self.current_layer:
      # set_current_layer takes care of the output graph
      self.set_current_layer(merged_layer)
    else:
      self._build_output_graph()
      self.notify("layers")
    self.invalidate()
  
  def set_current_layer(self, target_layer):
    if self.current_layer == target_layer:
      return
    
    self.current_layer = target_layer
    
    self._build_output_graph()
    
    self.notify("layers")
  
  def set_layer_prop(self, target_layer, prop_name, value):
    old_value = target_layer.get_property(prop_name)
    if value != old_value:
      undo = UndoLayerProperty(target_layer, prop_name, old_value, value)
      # Extra logic to merge opacity changes that happen close together into one event,
      # e.g. while the slider is moving.
      if prop_name == "opacity":
        folded = False
        if len(self.undo_history) != 0:
          last_undo = self.undo_history[-1]
          if isinstance(last_undo, UndoLayerProperty) and last_undo.prop_name == "opacity":
            if hasattr(last_undo, "timestamp") and time.time() - last_undo.timestamp < 0.5:
              last_undo.timestamp = time.time()
              last_undo.new_value = value
              folded = True
        if not folded:
          undo.timestamp = time.time()
          self.add_undo_step(undo)
      else:
        self.add_undo_step(undo)
      target_layer.set_property(prop_name, value)

  def center_at(self, x, y):
    """Center the widget at canvas coordinates (x,y)"""
    # Convert X, Y to widget coordinates
    x = x * self.scale
    y = y * self.scale
    # Subtract the allocation
    allocation = self.get_allocation()
    x -= allocation.width  / 2.0
    y -= allocation.height / 2.0
    # Set view offset
    self.view_offset = map(int, [-x, -y])
    # Redraw
    self.queue_draw()

  def set_scale_at_widget(self, scale, x, y):
    """Set the canvas scale and center at widget coordinates (x, y)"""
    canvas_center = self._widget_to_canvas_xy(x, y)
    self.set_property("scale", scale)
    self.center_at(*canvas_center)
  
  def set_scale_centered(self, scale):
    """Set the canvas scale, staying centered on the same location"""
    allocation = self.get_allocation()
    self.set_scale_at_widget(scale,
                             allocation.width / 2.0,
                             allocation.height / 2.0)

  # --- GObject glue ---
  def do_set_property(self, prop, value):
    if prop.name == "brush":
      if self.brush_modify_signal:
        self.brush.disconnect(self.brush_modify_signal)
        self.brush_modify_signal = None
      self.brush = value
      if self.brush:
        self.on_brush_setting_change(self.brush)
        self.brush_modify_signal = self.brush.connect("modified", self.on_brush_setting_change)
    elif prop.name == "scale":
      if self.scale != value:
        self.scale = value

        self.output_scaled = None
        self.queue_draw()
        
        # Update the cursor to reflect the new scale
        if self.brush:
          self.on_brush_setting_change(self.brush)
    elif prop.name == "max-undo-steps":
      self.max_undo_steps = value
      if len(self.undo_history) > self.max_undo_steps:
        self.undo_history = self.undo_history[-self.max_undo_steps:]
    elif prop.name == "canvas-background":
      self.canvas_background = value
      self._update_background_tile()
      
      self._build_output_graph()
      self.invalidate()
      self.queue_draw()
    elif prop.name == "filename":
      self.save_filename = value
    elif prop.name == "modified":
      self.canvas_modified = value
    else:
      raise ValueError("Unkown property name:" + prop.name)

  def do_get_property(self, prop):
    if prop.name == "brush":
      return self.brush
    elif prop.name == "layers":
      return self.layers
    elif prop.name == "scale":
      return self.scale
    elif prop.name == "max-undo-steps":
      return self.max_undo_steps
    elif prop.name == "canvas-background":
      return self.canvas_background
    elif prop.name == "filename":
      return self.save_filename
    elif prop.name == "modified":
      return self.canvas_modified
    else:
      raise ValueError("Unkown property name:" + prop.name)

  # --- Event handlers ---
  
  def on_destroy(self, widget):
    if self.idle_render_source_id:
      # Halt idle_render
      GLib.source_remove(self.idle_render_source_id)
      self.dirty_tiles = None
    self.layer_signals = None
    self.action_map = None
    self.set_property("brush", None)
  
  def idle_render(self):
    if self.debug__print_render_time and self.__dirty_tiles_t0 is None:
      self.__dirty_tiles_t0 = time.time()
  
    if self.output_node and self.dirty_tiles:
      x0, y0, x1, y1 = self.dirty_tiles.pop_tile()
      rect = Gegl.Rectangle()
      rect.x, rect.y, rect.width, rect.height = x0, y0, x1-x0, y1-y0
      self.output_node.blit_buffer(self.output_buffer, rect)

      queue_rect = self._canvas_to_widget_rect(x0, y0, x1-x0, y1-y0)
      self.queue_draw_area(*queue_rect)
      return True
    
    if self.debug__print_render_time:
      print("Idle render %.3f" % (time.time() - self.__dirty_tiles_t0))
    self.idle_render_source_id = None
    return False
  
  def on_draw(self, widget, cr):
    if self.output_node and self.dirty_rect:
      if self.debug__print_render_time:
        t0 = time.time()
      
      x0, y0, x1, y1 = self.dirty_rect
      
      rect = Gegl.Rectangle()
      rect.x, rect.y, rect.width, rect.height = x0, y0, x1-x0, y1-y0
      self.output_node.blit_buffer(self.output_buffer, rect)
      self.dirty_rect = None

      if self.debug__print_render_time:
        print("Rendered %.3f" % (time.time() - t0))
    
    cr.save()
    cr.translate(int(self.view_offset[0]), int(self.view_offset[1]))
    cr.scale(self.scale, self.scale)
    
    if not self.canvas_background_surface_tile:
      cr.set_source_rgb(0.6, 0.6, 0.6)
    else:
      bg_pattern = cairo.SurfacePattern(self.canvas_background_surface_tile)
      bg_pattern.set_extend(cairo.EXTEND_REPEAT)
      cr.set_source(bg_pattern)
      cr.get_source().set_filter(cairo.FILTER_NEAREST)
    cr.set_operator(cairo.OPERATOR_SOURCE)
    cr.paint()
    cr.restore()
    
    limits_rect = [self.drawing_origin_x, self.drawing_origin_y, self.drawing_width, self.drawing_height]
    if self.current_stroke_bounds:
      limits_rect = rect_union(limits_rect, self.current_stroke_bounds)
    
    cr.save()
    cr.rectangle(*self._canvas_to_widget_rect(*limits_rect))
    cr.clip()
    
    cr.translate(int(self.view_offset[0]), int(self.view_offset[1]))

    if self.output_scaled:
      cr.set_source_surface(self.output_scaled, 0, 0)
      cr.set_operator(cairo.OPERATOR_OVER)
      cr.paint()
    else:
      cr.scale(self.scale, self.scale)
      try:
        clip_extents = cr.clip_extents()
        pixel_size = max(1.0 / self.scale, 1.0)
        
        output_rect = [int(math.floor(clip_extents[0] - pixel_size)),
                       int(math.floor(clip_extents[1] - pixel_size)),
                       int(math.ceil(clip_extents[2] - clip_extents[0] + 2 * pixel_size)),
                       int(math.ceil(clip_extents[3] - clip_extents[1] + 2 * pixel_size))]
        
        output_rect = rect_intersect(output_rect, limits_rect)
        
        #print("limits_rect", limits_rect, "output", output_rect)
        
        if output_rect[2] and output_rect[3]:
          # Blit the image buffer to that image
          scratch_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, output_rect[2], output_rect[3])
          drawbuffer.gegl_buffer_copy_to_cairo(self.output_buffer, scratch_surface, output_rect)
          # Paint it out
          cr.set_source_surface(scratch_surface, output_rect[0], output_rect[1])
          cr.set_operator(cairo.OPERATOR_OVER)
          cr.paint()
      except:
        import traceback
        traceback.print_exc()
    
    cr.restore()
  
  def on_key_press(self, widget, event):
    self.action_map.process_event(event)
    if event.keyval in [Gdk.KEY_Up, Gdk.KEY_Down, Gdk.KEY_Left, Gdk.KEY_Right]:
      return True
    if not self.current_action:
      #FIXME: Integrate with action map
      maybe_action = None
      if event.keyval == Gdk.KEY_space:
        maybe_action = CanvasAction_MoveView

      if maybe_action:
        self.current_action = maybe_action()
        self.update_cursor()

  def on_key_release(self, widget, event):
    self.action_map.process_event(event)
    
    if event.keyval == Gdk.KEY_Up:
      self.view_offset[1] += 64
      self.queue_draw()
    elif event.keyval == Gdk.KEY_Down:
      self.view_offset[1] -= 64
      self.queue_draw()
    elif event.keyval == Gdk.KEY_Left:
      self.view_offset[0] += 64
      self.queue_draw()
    elif event.keyval == Gdk.KEY_Right:
      self.view_offset[0] -= 64
      self.queue_draw()
    elif event.keyval == Gdk.KEY_space:
      #FIXME: Integrate with action map
      if isinstance(self.current_action, CanvasAction_MoveView):
        self.current_action = None
        self.update_cursor()
    else:
      return False
    return True
    
  def on_scroll_event(self, widget, event):
    INVERT_SCROLL = True
    
    if not self.output_scaled and self.drawing_width > 0 and self.drawing_height > 0:
      scratch_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,
                                           self.drawing_width,
                                           self.drawing_height)
      drawbuffer.gegl_buffer_copy_to_cairo(self.output_buffer, scratch_surface,
                                           (self.drawing_origin_x,
                                            self.drawing_origin_y,
                                            self.drawing_width,
                                            self.drawing_height))

      # The output is aligned such that 0,0 widget always maps to 0,0 canvas
      # So find the nearest whole pixel and set output_scaled's device offest there
      x = int(math.floor(self.drawing_origin_x * self.scale))
      y = int(math.floor(self.drawing_origin_y * self.scale))
      w = int(math.ceil(self.drawing_width     * self.scale))
      h = int(math.ceil(self.drawing_height    * self.scale))
      
      try:
        self.output_scaled = self.get_window().create_similar_surface(cairo.CONTENT_COLOR_ALPHA, w, h)
      except:
        import traceback
        traceback.print_exc()
        self.output_scaled = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, h)
      self.output_scaled.set_device_offset(-x, -y)
      
      cr = cairo.Context(self.output_scaled)
      cr.scale(self.scale, self.scale)
      cr.set_source_surface(scratch_surface, self.drawing_origin_x, self.drawing_origin_y)
      cr.set_operator(cairo.OPERATOR_SOURCE)
      cr.paint()
    
    delta_x, delta_y = event.delta_x, event.delta_y
    
    if event.direction == Gdk.ScrollDirection.UP:
      delta_x, delta_y = 0, -8 * delta_y
    elif event.direction == Gdk.ScrollDirection.DOWN:
      delta_x, delta_y = 0,  8 * delta_y
    elif event.direction == Gdk.ScrollDirection.LEFT:
      delta_x, delta_y = -8 * delta_x, 0
    elif event.direction == Gdk.ScrollDirection.RIGHT:
      delta_x, delta_y =  8 * delta_x, 0
    elif event.direction == Gdk.ScrollDirection.SMOOTH:
      delta_x, delta_y =  8 * delta_x, 8 * delta_y
    
    if INVERT_SCROLL:
      delta_x *= -1.0
      delta_y *= -1.0
    
    if abs(delta_x) > 0.01 or abs(delta_y) > 0.01:
      self.view_offset[0] += delta_x
      self.view_offset[1] += delta_y
      self.queue_draw()
      
    return False
  
    
  def stroke_points(self, points, brush=None):
    if brush:
      stroke = brush.new_stroke()
    else:
      stroke = self.brush.new_stroke()
    
    min_x, min_y, max_x, max_y = stroke.stroke_points(self.current_stroke_layer, points)
    min_x, min_y, max_x, max_y = map(int, (min_x, min_y, max_x, max_y))

    self.add_undo_step(UndoRegion(self.current_layer, min_x, min_y, max_x - min_x, max_y - min_y))
    self.current_stroke_layer.merge_down(self.current_layer, (min_x, min_y, max_x - min_x, max_y - min_y), expand=True)
    self._update_drawing_size()
    self.current_stroke_layer.drawbuf.fill(min_x, min_y, max_x - min_x, max_y - min_y,
                                           color=(1.0, 1.0, 1.0, 0.0))
    self.invalidate_area(min_x, min_y, max_x - min_x, max_y - min_y)

  def on_button_press(self, widget, event):
    self.grab_focus()
    self.action_map.process_event(event)
    
    if not self.current_layer:
      return
  
    if not self.current_action:
      maybe_action = self.action_map.get_action(event.button)
      if maybe_action:
        self.current_action = maybe_action()
      else:
        return

    if isinstance(self.current_action, CanvasAction_PickColor):
      x = event.get_axis(Gdk.AxisUse.X) or event.x
      y = event.get_axis(Gdk.AxisUse.Y) or event.y
      
      x, y = self._widget_to_canvas_xy(x, y)
      picked = self.current_layer.drawbuf.get_pixel(int(x), int(y))
      # Ignore transparent pixels
      if picked[3] != 0.0 and self.brush:
        #FIXME: Make this into a more generic "has_setting"
        if filter(lambda s : s.prop_name == "color", self.brush.setting_list):
          self.brush.set_property("color", tuple(picked[:3]))
      self.current_action = None
      self.update_cursor()
    elif isinstance(self.current_action, CanvasAction_MoveLayer):
      self.soft_move = [0, 0]
      self.current_action.start_loc = (int(event.x), int(event.y))
    elif isinstance(self.current_action, CanvasAction_MoveView):
      self.current_action.last_pos = (int(event.x), int(event.y))
    elif isinstance(self.current_action, CanvasAction_Paint):
      if self.brush is None:
        self.current_action = None
        self.update_cursor()
      else:
        pressure = event.get_axis(Gdk.AxisUse.PRESSURE)
        if pressure is None:
          pressure = 1.0
          
        x = event.get_axis(Gdk.AxisUse.X)
        y = event.get_axis(Gdk.AxisUse.Y)
        
        if x is None or y is None:
          x = event.x
          y = event.y
        
        x, y = self._widget_to_canvas_xy(x, y)
        
        self.current_stroke = self.brush.new_stroke()
        
        min_x, min_y, max_x, max_y = self.current_stroke.start_stroke(self.current_stroke_layer, x, y, pressure)

        modified_rect = (min_x, min_y, max_x - min_x, max_y - min_y)
        self.current_stroke_bounds = modified_rect
        self.invalidate_area(*modified_rect)
        
        # New stroke, clear the redo history
        self.redo_history = []
  
  def on_button_release(self, widget, event):
    self.action_map.process_event(event)
    
    if isinstance(self.current_action, CanvasAction_MoveLayer):
      #FIXME: The actual end move state is more complex than this because we don't
      #       know which button started it.
      self.current_action = None
      self.update_cursor()
      
      self.current_layer_translate_node.set_property("x", 0)
      self.current_layer_translate_node.set_property("y", 0)
      translated_layer = self.current_layer.translate_clone(self.current_layer.x + self.soft_move[0],
                                                            self.current_layer.y + self.soft_move[1])

      self.soft_move = [0, 0] # This must be done first, so the drawing size gets updated correctly
      self._update_drawing_size() # Set the drawing dimentions back to the pre-translated size
      self.invalidate() # Invalidate the old drawing rect, so we will clear places the layer moved out of
      self._replace_layer(self.current_layer, translated_layer)

    elif isinstance(self.current_action, CanvasAction_MoveView):
      self.current_action.last_pos = None

    elif isinstance(self.current_action, CanvasAction_Paint):
      self.current_action = None
      self.update_cursor()
      
      if not self.current_stroke:
        return
      
      pressure = event.get_axis(Gdk.AxisUse.PRESSURE)
      if pressure is None:
        pressure = 1.0
        
      x = event.get_axis(Gdk.AxisUse.X)
      y = event.get_axis(Gdk.AxisUse.Y)
      
      x, y = self._widget_to_canvas_xy(x, y)
      
      min_x, min_y, max_x, max_y = self.current_stroke.end_stroke(self.current_stroke_layer, x, y, pressure)
      
      modified_rect = (min_x, min_y, max_x - min_x, max_y - min_y)

      #Back up stroke area for undo
      self.add_undo_step(UndoRegion(self.current_layer, *modified_rect))

      # Apply the stroke to the canvas
      self.current_stroke_layer.merge_down(self.current_layer, modified_rect, expand=True)
      self.current_stroke_bounds = None
      self._update_drawing_size()
      
      self.current_stroke_layer.drawbuf.fill(*modified_rect,
                                             color=(1.0, 1.0, 1.0, 0.0))
  
  def on_motion(self, widget, event):
    self.action_map.process_event(event)
    
    if isinstance(self.current_action, CanvasAction_MoveLayer):
      move_start = self.current_action.start_loc
      self.soft_move = [int((event.x - move_start[0]) / self.scale),
                        int((event.y - move_start[1]) / self.scale)]
      self.current_layer_translate_node.set_property("x", self.soft_move[0])
      self.current_layer_translate_node.set_property("y", self.soft_move[1])
      self._update_drawing_size()
      self.invalidate()
      self.queue_draw() # Ensure we erase the places the layer moved out of

    elif isinstance(self.current_action, CanvasAction_MoveView):
      if self.current_action.last_pos is not None:
        new_pos  = (int(event.x), int(event.y))
        last_pos = self.current_action.last_pos
        if new_pos != last_pos:
          self.view_offset[0] += new_pos[0] - last_pos[0]
          self.view_offset[1] += new_pos[1] - last_pos[1]
          self.queue_draw()
          self.current_action.last_pos = new_pos

    elif isinstance(self.current_action, CanvasAction_Paint):
      pressure = event.get_axis(Gdk.AxisUse.PRESSURE)
      if pressure is None:
        pressure = 1.0
      x = event.get_axis(Gdk.AxisUse.X)
      y = event.get_axis(Gdk.AxisUse.Y)
      
      x, y = self._widget_to_canvas_xy(x, y)

      min_x, min_y, max_x, max_y = self.current_stroke.stroke_to(self.current_stroke_layer, x, y, pressure)
      
      min_x, min_y, max_x, max_y = map(int, (min_x, min_y, max_x, max_y))
      
      modified_rect = (min_x, min_y, max_x - min_x, max_y - min_y)
      self.current_stroke_bounds = rect_union(self.current_stroke_bounds, modified_rect)
      self.invalidate_area(*modified_rect)
  
  def add_undo_step(self, step):
    self.set_property("modified", True)
    self.redo_history = []
    self.undo_history.append(step)
    if(len(self.undo_history) > self.max_undo_steps):
      self.undo_history = self.undo_history[-self.max_undo_steps:]
    
  def undo(self):
    if self.undo_history:
      undo_entry = self.undo_history.pop()
      redo_entry, x, y, w, h = undo_entry.apply(self)
      self.redo_history.append(redo_entry)
      self.invalidate_area(x, y, w, h)
      self.set_property("modified", True)

  def redo(self):
    if self.redo_history:
      redo_entry = self.redo_history.pop()
      undo_entry, x, y, w, h = redo_entry.apply(self)
      self.undo_history.append(undo_entry)
      self.invalidate_area(x, y, w, h)
      self.set_property("modified", True)

  def on_widget_realize(self, widget):
    # The brush may have been set before we were able to set a cursor
    self.update_cursor()
  
  def on_brush_setting_change(self, tool):
    self.update_cursor()
  
  def update_cursor(self):
    new_cursor = None
    if self.current_action:
      action_class = self.current_action.__class__
    else:
      action_class = self.action_map.get_action(1)
    
    if action_class == CanvasAction_PickColor:
      #FIXME: We need an eyedropper cursor
      new_cursor = paintcursor.make_cursor(2)
    elif action_class == CanvasAction_MoveLayer:
      new_cursor = resourceloader.get_cursor("thedraw-cursor-move-layer")
    elif action_class == CanvasAction_MoveView:
      new_cursor = resourceloader.get_cursor("thedraw-cursor-move-view")
    elif action_class == CanvasAction_Paint:
      if self.brush and hasattr(self.brush, "size"):
        new_cursor = paintcursor.make_cursor(self.brush.size * self.scale)
  
    window = self.get_window()
    if window:
      window.set_cursor(new_cursor)

