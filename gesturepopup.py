from __future__ import print_function

#import cairo
from gi.repository import Gtk
from gi.repository import Gdk

from spinwidget import SpinWidget
from brushpreviewpopup import BrushPreviewWidget

class GesturePopup(Gtk.Window):
  def __init__(self):
    Gtk.Window.__init__(self, Gtk.WindowType.POPUP)
    self.set_type_hint(Gdk.WindowTypeHint.UTILITY)

    self.target_brush = None

    self.height  = 140
    self.width   = self.height + 400
    self.padding = 0
    
    self.set_default_size(self.width + self.padding, self.height + self.padding)

    self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK   |
                    Gdk.EventMask.POINTER_MOTION_MASK |
                    Gdk.EventMask.BUTTON_RELEASE_MASK)
    
    #self.spin_widget = Gtk.Label("Spin here")
    self.spin_widget = SpinWidget(rate=0.2)
    self.spin_widget.always_track = True
    self.spin_widget.set_size_request(self.height, self.height)
    #self.preview_widget = Gtk.Label("Preview here")
    self.preview_widget = BrushPreviewWidget()
    
    box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    self.add(box)
    box.pack_start(self.spin_widget, False, False, 0)
    box.pack_end(self.preview_widget, True, True, 0)

    self.connect("hide", self.on_hide)
    self.connect("destroy", self.on_destroy)
    self.spin_widget.connect("notify::value", self.on_spin_value_notify)
  
  def center_at(self, x, y):
    spin_allocation = self.spin_widget.get_allocation()
    #print(spin_allocation.x, spin_allocation.y, spin_allocation.width, spin_allocation.height)
    self.move(x - spin_allocation.width // 2, y - spin_allocation.height // 2)

  
  def on_hide(self, widget):
    pass
  
  def on_destroy(self, widget):
    pass

  def on_spin_value_notify(self, obj, prop):
    pspec = dict([(i.name, i) for i in self.target_brush.props])[self.target_prop]
    new_value = self.spin_widget.get_property("value")
    new_value = max(min(new_value, pspec.maximum), pspec.minimum)
    self.target_brush.set_property(self.target_prop, new_value)
    self.preview_widget.set_brush(self.target_brush, self.target_brush_scale)  

  def set_brush(self, brush, scale, prop_name):
    self.target_brush_scale = scale
    self.target_brush = brush
    self.target_prop = prop_name
    value = self.target_brush.get_property(self.target_prop)
    pspec = dict([(i.name, i) for i in self.target_brush.props])[self.target_prop]
    self.spin_widget.set_limits(pspec.minimum, pspec.maximum)
    
    self.spin_widget.handler_block_by_func(self.on_spin_value_notify)
    self.spin_widget.set_property("value", value)
    self.spin_widget.handler_unblock_by_func(self.on_spin_value_notify)
    
    self.preview_widget.set_brush(self.target_brush, self.target_brush_scale)
