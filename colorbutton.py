from __future__ import print_function
from __future__ import division

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Gdk
import cairo
import colorsys

class ColorButton(Gtk.Button):
  __gproperties__ = {
    'color' : (GObject.TYPE_OBJECT,
        'color',
        'Currently selected color',
        GObject.PARAM_READWRITE),
    }
  
  def __init__(self):
    Gtk.Button.__init__(self)
    self.color = Gdk.Color(0xFFFF, 0xFFFF, 0xFFFF)
    self.connect("clicked", self.on_clicked)
    
    self.drawing_area = Gtk.DrawingArea()
    self.drawing_area.set_size_request(16, 16)
    self.drawing_area.connect("draw", self.on_draw)
    
    self.add(self.drawing_area)
    self.drawing_area.show()
  
  def on_draw(self, widget, cr):
    cr.set_source_rgb(float(self.color.red)   / 0xFFFF,
                      float(self.color.green) / 0xFFFF,
                      float(self.color.blue)  / 0xFFFF)
    cr.set_operator(cairo.OPERATOR_SOURCE)
    cr.paint()
  
  def on_clicked(self, widget):
    dialog = Gtk.ColorSelectionDialog()
    dialog.get_color_selection().set_current_color(self.color)
    if dialog.run() == Gtk.ResponseType.OK:
      self.set_color(dialog.get_color_selection().get_current_color())
      self.drawing_area.queue_draw()
    dialog.destroy()
      
  def set_color(self, color):
    self.color = color
    self.notify("color")
    self.drawing_area.queue_draw()
  
  def get_color(self):
    return self.color
  
  def do_get_property(self, prop_name):
    if prop_name == "color":
      return self.get_color()
  
  def do_set_property(self, prop_name, value):
    if prop_name == "color":
      self.set_color(value)

class PopupColorButton(Gtk.Button):
  __gproperties__ = {
    'color' : (GObject.TYPE_OBJECT,
        'color',
        'Currently selected color',
        GObject.PARAM_READWRITE),
    }
  
  def __init__(self):
    Gtk.Button.__init__(self)
    self.color = Gdk.Color(0xFFFF, 0xFFFF, 0xFFFF)
    self.connect("clicked", self.on_clicked)
    self.connect("button-press-event", self.on_button_press)
    
    self.drawing_area = Gtk.DrawingArea()
    self.drawing_area.set_size_request(16, 16)
    self.drawing_area.connect("draw", self.on_draw)
    
    self.popup_window = Gtk.Window(type=Gtk.WindowType.POPUP)
    self.popup_window.set_focus_on_map(False)
    self.popup_window.set_can_focus(False)
    self.popup_window.set_attached_to(self)
    
    self.popup_window_hsv = Gtk.HSV()
    self.popup_window_hsv.set_metrics(155, 12)
    self.popup_window_hsv.set_can_focus(False)
    
    frame = Gtk.Frame()
    frame.add(self.popup_window_hsv)
    frame.show_all()
    self.popup_window.add(frame)
    
    self.popup_window_hsv.connect("changed", self.on_color_changed)
    self.popup_window.connect("leave-notify-event", self.on_popup_exit)
    self.popup_window.connect("show", self.on_popup_show)
    
    self.add(self.drawing_area)
    self.drawing_area.show()
  
  def on_draw(self, widget, cr):
    cr.set_source_rgb(float(self.color.red)   / 0xFFFF,
                      float(self.color.green) / 0xFFFF,
                      float(self.color.blue)  / 0xFFFF)
    cr.set_operator(cairo.OPERATOR_SOURCE)
    cr.paint()
  
  def on_popup_show(self, popup_window):
    # Move to the correct location just before we display so sizes are correct
    x,y = self.get_window().get_origin()[1:]
    allocation = self.get_allocation()
    win_width, win_height = self.popup_window.get_size()
    
    x += allocation.width // 2 + allocation.x - win_width // 2
    y += allocation.height + allocation.y
    
    self.popup_window.move(x, y)
    self.popup_window.set_transient_for(self.get_toplevel())
  
  def on_clicked(self, widget):
    if self.popup_window.get_visible():
      self.popup_window.hide()
    else:
      rgb_color = (float(self.color.red)   / 0xFFFF,
                   float(self.color.green) / 0xFFFF,
                   float(self.color.blue)  / 0xFFFF)
      
      hsv_color = colorsys.rgb_to_hsv(*rgb_color)
      self.popup_window_hsv.set_color(*hsv_color)
      
      self.popup_window.show()

  def on_button_press(self, widget, event):
    if event.button == 3 and event.type == Gdk.EventType.BUTTON_PRESS or \
         event.button == 1 and \
         event.type == Gdk.EventType.BUTTON_PRESS and \
         event.state & Gdk.ModifierType.SHIFT_MASK:
      dialog = Gtk.ColorSelectionDialog()
      dialog.get_color_selection().set_current_color(self.color)
      if dialog.run() == Gtk.ResponseType.OK:
        self.set_color(dialog.get_color_selection().get_current_color())
        self.drawing_area.queue_draw()
      dialog.destroy()

  def on_popup_exit(self, widget, event):
    if event.detail != Gdk.NotifyType.INFERIOR:
      self.popup_window.hide()
  
  def on_color_changed(self, widget):
    hsv_color = widget.get_color()
    rgb_color = colorsys.hsv_to_rgb(*hsv_color)
    self.set_color(Gdk.Color(rgb_color[0] * 0xFFFF,
                             rgb_color[1] * 0xFFFF,
                             rgb_color[2] * 0xFFFF))

  def set_color(self, color):
    self.color = color
    self.notify("color")
    self.drawing_area.queue_draw()
  
  def get_color(self):
    return self.color
  
  def do_get_property(self, prop_name):
    if prop_name == "color":
      return self.get_color()
  
  def do_set_property(self, prop_name, value):
    if prop_name == "color":
      self.set_color(value)


if __name__ == '__main__':
  w = Gtk.Window()
  box = Gtk.Box(Gtk.Orientation.VERTICAL)
  
  b1 = ColorButton()
  b1.set_color(Gdk.Color(0xFFFF, 0x0, 0x0))
  b1.connect("notify::color", lambda obj, prop : print(prop, obj.get_color()))
  box.pack_start(b1, False, False, 0)
  
  b2 = PopupColorButton()
  b2.set_color(Gdk.Color(0xFFFF, 0x0, 0x0))
  b2.connect("notify::color", lambda obj, prop : print(prop, obj.get_color()))
  box.pack_start(b2, False, False, 0)
  
  w.add(box)
  w.connect("destroy", lambda widget : Gtk.main_quit())
  w.show_all()
  Gtk.main()
