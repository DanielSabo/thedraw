/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2006 Øyvind Kolås <pippin@gimp.org>
 * Copyright 2013 Daniel Sabo <DanielSabo@gmail.com>
 */

#ifdef GEGL_CHANT_PROPERTIES

gegl_chant_string(format, "Output format", "RGBA float", "Babl format string")

#else

#define GEGL_CHANT_TYPE_AREA_FILTER
#define GEGL_CHANT_C_FILE "thedraw-convert-format.c"

#include "gegl-chant.h"

static void
thedraw_convert_format_prepare (GeglOperation *self)
{
  GeglChantO *o = GEGL_CHANT_PROPERTIES (self);

  if (o->format)
    gegl_operation_set_format (self, "output", babl_format (o->format));
  else
    gegl_operation_set_format (self, "output", gegl_operation_get_source_format (self, "input"));
}

static gboolean
thedraw_convert_format_process (GeglOperation       *operation,
                                GeglBuffer          *input,
                                GeglBuffer          *output,
                                const GeglRectangle *roi,
                                gint                 level)
{
  gegl_buffer_copy (input, roi, output, roi);
  return TRUE;
}

static void
gegl_chant_class_init (GeglChantClass *klass)
{
  GeglOperationClass       *operation_class;
  GeglOperationFilterClass *filter_class;

  operation_class = GEGL_OPERATION_CLASS (klass);
  filter_class    = GEGL_OPERATION_FILTER_CLASS (klass);
  operation_class->prepare  = thedraw_convert_format_prepare;
  filter_class->process     = thedraw_convert_format_process;
  operation_class->no_cache = FALSE;

  gegl_operation_class_set_keys (operation_class,
                                 "name",        "thedraw:convert-format",
                                 "categories",  "core",
                                 "description", "convert input to the specified format",
                                 NULL);
}

#endif
