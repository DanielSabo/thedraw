/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2006-2009 Øyvind Kolås <pippin@gimp.org>
 * Copyright 2013 Daniel Sabo
 */
#include <gegl-plugin.h>
#include "thedraw-blend-op.h"

G_DEFINE_DYNAMIC_TYPE (TheDrawBlendOp, thedraw_blend_op, GEGL_TYPE_OPERATION_POINT_COMPOSER);

#define parent_class thedraw_blend_op_parent_class

static void
prepare (GeglOperation *operation)
{
  const Babl *format = babl_format ("R'aG'aB'aA float");

  gegl_operation_set_format (operation, "input", format);
  gegl_operation_set_format (operation, "aux", format);
  gegl_operation_set_format (operation, "output", format);
}

static gboolean
process (GeglOperation        *operation,
         void                 *in_buf,
         void                 *aux_buf,
         void                 *out_buf,
         glong                 n_pixels,
         const GeglRectangle  *roi,
         gint                  level)
{
  return FALSE;
}

static gboolean
operation_process (GeglOperation        *operation,
                   GeglOperationContext *context,
                   const gchar          *output_prop,
                   const GeglRectangle  *result,
                   gint                  level)
{
  GeglOperationClass *operation_class = GEGL_OPERATION_CLASS (parent_class);
  GeglBuffer *input = GEGL_BUFFER (gegl_operation_context_get_object (context, "input"));
  GeglBuffer *aux   = GEGL_BUFFER (gegl_operation_context_get_object (context, "aux"));

  if (!aux ||
      (input && !gegl_rectangle_intersect (NULL, gegl_buffer_get_extent (aux), result)))
    {
       gegl_operation_context_take_object (context, "output",
                                           g_object_ref (input));
       return TRUE;
    }
  else if (!input ||
           (aux && !gegl_rectangle_intersect (NULL, gegl_buffer_get_extent (input), result)))
    {
       gegl_operation_context_take_object (context, "output",
                                           g_object_ref (aux));
       return TRUE;
    }
  return operation_class->process (operation, context, output_prop, result, level);
}

static void
thedraw_blend_op_init (TheDrawBlendOp *self)
{
}

static void
thedraw_blend_op_class_finalize (TheDrawBlendOpClass *klass)
{
  return;
}

static void
thedraw_blend_op_class_init (TheDrawBlendOpClass *klass)
{
  GeglOperationClass              *operation_class;
  GeglOperationPointComposerClass *point_composer_class;

  operation_class      = GEGL_OPERATION_CLASS (klass);
  point_composer_class = GEGL_OPERATION_POINT_COMPOSER_CLASS (klass);
  operation_class->prepare = prepare;
  operation_class->process = operation_process;
  point_composer_class->process = process;
}

void thedraw_blend_op_module_register (GTypeModule *module)
{
  thedraw_blend_op_register_type (module);
}