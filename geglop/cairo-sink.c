#include <cairo/cairo.h>

#ifdef GEGL_CHANT_PROPERTIES

gegl_chant_pointer (surface, "Cairo Surface",
                    "The cairo ImageSurface this operation will write to")

#else

#define GEGL_CHANT_TYPE_SINK
#define GEGL_CHANT_C_FILE       "cairo-sink.c"

#include "gegl-chant.h"

static gboolean
process (GeglOperation       *operation,
         GeglBuffer          *input,
         const GeglRectangle *result,
         gint                 level)
{
  GeglChantO *o = GEGL_CHANT_PROPERTIES (operation);

  if (!o->surface)
    return TRUE;

  const Babl *format = NULL;
  
  GeglRectangle roi, cairo_surface_extent;
  
  cairo_surface_extent.x = 0;
  cairo_surface_extent.y = 0;
  cairo_surface_extent.width  = cairo_image_surface_get_width(o->surface);
  cairo_surface_extent.height = cairo_image_surface_get_height(o->surface);
  
  if (!gegl_rectangle_intersect (&roi, &cairo_surface_extent, result))
    return TRUE;
  

  switch (cairo_image_surface_get_format(o->surface)) {
    case CAIRO_FORMAT_ARGB32:
      format = babl_format("cairo-ARGB32");
      break;
    case CAIRO_FORMAT_RGB24:
      format = babl_format("cairo-RGB24");
      break;
    default:
      g_warning ("Unsupported cairo surface format");
      return TRUE;
      break;
  }

  cairo_surface_flush(o->surface);
  
  const int pixel_size = babl_format_get_bytes_per_pixel(format);
  const int out_row_stride = cairo_surface_extent.width * pixel_size;
  unsigned char *out_data = cairo_image_surface_get_data(o->surface);
  
  GeglBufferIterator *iter = gegl_buffer_iterator_new(input, &roi, 0,
                                                      format,
                                                      GEGL_BUFFER_READ, 
                                                      GEGL_ABYSS_NONE);


  while (gegl_buffer_iterator_next(iter))
  {
    unsigned char *in_row = (unsigned char *)iter->data[0];
    int iy;
    int ix = iter->roi[0].x;
    const int in_row_stride = iter->roi[0].width * pixel_size;
    for (iy = iter->roi[0].y; iy < iter->roi[0].y + iter->roi[0].height; iy++)
    {
      unsigned char *out_row = &out_data[iy * out_row_stride + ix * pixel_size];
      memcpy(out_row, in_row, in_row_stride);
      in_row += in_row_stride;
    }
  }

  cairo_surface_mark_dirty_rectangle(o->surface, roi.x, roi.y, roi.width, roi.height);

  return TRUE;
}

static void
gegl_chant_class_init (GeglChantClass *klass)
{
  GeglOperationClass     *operation_class;
  GeglOperationSinkClass *sink_class;

  operation_class = GEGL_OPERATION_CLASS (klass);
  sink_class      = GEGL_OPERATION_SINK_CLASS (klass);

  sink_class->process = process;
  sink_class->needs_full = FALSE;

  gegl_operation_class_set_keys (operation_class,
      "name",       "thedraw:cairo-sink",
      "categories", "programming:output",
      "description", "Write to a Cairo ImageSurface destination surface.",
      NULL);
}

#endif
