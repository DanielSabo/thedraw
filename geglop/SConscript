import os, sys

Import("env")
geglop_env = env.Clone()

geglop_env.Append(CFLAGS = ["-fno-strict-aliasing", "-O2"])
geglop_env.Append(CFLAGS = "-I${SOURCE.srcdir}") # This is needed to make GEGL chants work
geglop_env.ParseConfig("pkg-config gegl-0.3 --cflags --libs")

if sys.platform == "win32":
  geglop_env["SHLIBPREFIX"] = ""
  geglop_env["SHLIBSUFFIX"] = ".dll"
else:
  geglop_env["SHLIBPREFIX"] = ""
  geglop_env["SHLIBSUFFIX"] = ".so"

libs = []

geglop_sse2_env = geglop_env.Clone()
geglop_sse2_env.Append(CFLAGS = ["-msse", "-msse2", "-mfpmath=sse"])

libs += geglop_env.SharedLibrary(target="thedraw-erase",
                                     source=["thedraw-erase.c"])
libs += geglop_env.SharedLibrary(target="thedraw-opacity",
                                     source=["thedraw-opacity.c"])
libs += geglop_env.SharedLibrary(target="thedraw-convert-format",
                                     source=["thedraw-convert-format.c"])

thedraw_over_objects = geglop_env.SharedObject(["thedraw-over.c"]) + geglop_sse2_env.SharedObject(["thedraw-over-sse2.c"])
libs += geglop_env.SharedLibrary(target="thedraw-over",
                                 source=thedraw_over_objects)

cairo_sink_env = geglop_env.Clone()
cairo_sink_env.ParseConfig("pkg-config cairo --cflags --libs")
libs += cairo_sink_env.SharedLibrary(target="cairo-sink",
                                     source=["cairo-sink.c"])

generated_sources = geglop_env.Command(["thedraw-blend-module.c",
                                        "thedraw-color-burn.c",
                                        "thedraw-color-dodge.c",
                                        "thedraw-multiply.c",
                                        "thedraw-screen.c"],
                                       ["generate-svg.py",
                                        "thedraw-blend-class.c.template",
                                        "thedraw-blend-mod.c.template"],
                                       "python generate-svg.py",
                                       chdir=True)

libs += geglop_env.SharedLibrary(target="thedraw-blend",
                                 source=["thedraw-blend-op.c",
                                         generated_sources])

install_env = env.Clone()

install_lib_path = os.path.join(env["PREFIX"], "lib", "thedraw", "geglop")

installed_libs = install_env.Install(install_lib_path, libs)

# Fix library link paths on OSX
if sys.platform == "darwin":
  install_env.AddPostAction(installed_libs, "install_name_tool -id $TARGET $TARGET")