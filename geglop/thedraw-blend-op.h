#ifndef __BLEND_OP_H__
#define __BLEND_OP_H__

#define THEDRAW_TYPE_BLEND_OP            (thedraw_blend_op_get_type ())
#define THEDRAW_BLEND_OP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), THEDRAW_TYPE_BLEND_OP, TheDrawBlendOp))
#define THEDRAW_IS_BLEND_OP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), THEDRAW_TYPE_BLEND_OP))
#define THEDRAW_BLEND_OP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), THEDRAW_TYPE_BLEND_OP, TheDrawBlendOpClass))
#define THEDRAW_IS_BLEND_OP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), THEDRAW_TYPE_BLEND_OP))
#define THEDRAW_BLEND_OP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), THEDRAW_TYPE_BLEND_OP, TheDrawBlendOpClass))

typedef struct _TheDrawBlendOp        TheDrawBlendOp;
typedef struct _TheDrawBlendOpClass   TheDrawBlendOpClass;

struct _TheDrawBlendOp
{
  GeglOperationPointComposer parent_instance;

  /* instance members */
};

struct _TheDrawBlendOpClass
{
  GeglOperationPointComposerClass parent_class;

  /* class members */
};

GType thedraw_blend_op_get_type (void);

#endif /* __BLEND_OP_H__*/