gboolean
_thedraw_over_process_sse2 (GeglOperation       *op,
                           void                *in_buf,
                           void                *aux_buf,
                           void                *out_buf,
                           glong                n_pixels,
                           const GeglRectangle *roi,
                           gint                 level);
