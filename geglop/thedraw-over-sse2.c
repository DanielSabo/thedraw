/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013 Daniel Sabo <DanielSabo@gmail.com>
 */

#include <gegl-plugin.h>
#include <emmintrin.h>

gboolean
_thedraw_over_process_sse2 (GeglOperation       *op,
                           void                *in_buf,
                           void                *aux_buf,
                           void                *out_buf,
                           glong                n_pixels,
                           const GeglRectangle *roi,
                           gint                 level)
{
  gfloat * GEGL_ALIGNED in = in_buf;
  gfloat * GEGL_ALIGNED aux = aux_buf;
  gfloat * GEGL_ALIGNED out = out_buf;

  int i = 0;

  if (((uintptr_t)in_buf % 16) + ((uintptr_t)aux_buf % 16) + ((uintptr_t)out_buf % 16) == 0)
  {
    const __v4sf *v_in  = (const __v4sf*) in;
    const __v4sf *v_aux = (const __v4sf*) aux;
          __v4sf *v_out = (      __v4sf*) out;

    const __v4sf one = _mm_set1_ps(1.0f);

    for (; i < n_pixels; ++i)
      {
        __v4sf rgba_in, rgba_aux, aaaa_aux, rgba_out;

        rgba_in  = *v_in++;
        rgba_aux = *v_aux++;

        aaaa_aux = (__v4sf)_mm_shuffle_epi32((__m128i)rgba_aux, _MM_SHUFFLE(3, 3, 3, 3));
        rgba_out = rgba_aux + rgba_in * (one - aaaa_aux);

        *v_out++ = rgba_out;
      }
    _mm_empty ();
  }

  for (; i < n_pixels; ++i)
      {
        out[0] = aux[0] + in[0] * (1.0f - aux[3]);
        out[1] = aux[1] + in[1] * (1.0f - aux[3]);
        out[2] = aux[2] + in[2] * (1.0f - aux[3]);
        out[3] = aux[3] + in[3] - aux[3] * in[3];

        in  += 4;
        aux += 4;
        out += 4;
      }
  return TRUE;
}

