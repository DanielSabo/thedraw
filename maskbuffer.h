#include <Python.h>
#include <gegl.h>

typedef struct {
    PyObject_HEAD
    int width;
    int height;
    float *data;
    size_t data_size;
} MaskBuffer;

PyTypeObject MaskBufferType;

PyObject *maskbuffer_from_png(PyObject *self, PyObject *args);
