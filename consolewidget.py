import sys

from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import Gtk

class tee_object():
  def __init__(self, out_func, tee_func):
    self.tee_func = tee_func
    self.out_func = out_func
  
  def write(self, data):
    self.tee_func(data)
    self.out_func(data)

#FIXME: Limit scrollback

class ConsoleWindow(Gtk.Window):
  def __init__(self, title="Console"):
    Gtk.Window.__init__(self)
    self.set_title(title)
    self.set_default_size(600, 200)
    
    self.text_view = Gtk.TextView()
    self.text_view.set_editable(False)
    self.text_view.set_cursor_visible(False)
    self.font_tag = self.text_view.get_buffer().create_tag("monospace-font", family="monospace")
    
    scroll_area = Gtk.ScrolledWindow()
    scroll_area.add(self.text_view)
    self.scroll_area = scroll_area
    
    self.add(self.scroll_area)
    
    self.recursion_guard = False

  def _append_text(self, text):
    if self.recursion_guard:
      return
    self.recursion_guard = True
    
    try:
      text_buffer = self.text_view.get_buffer()
      text_buffer.insert_with_tags(text_buffer.get_end_iter(), text, self.font_tag)
      self.show_all()
    finally:
      self.recursion_guard = False
  
  def append_text(self, text):
    Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE, self._append_text, text)

if __name__ == "__main__":
  w = ConsoleWindow()
  w.connect("destroy", lambda w : Gtk.main_quit())
  w.show_all()
  
  print "Pre"
  
  sys.stdout = tee_object(sys.__stdout__.write, w.append_text)
  
  print "One"
  print "Two"
  print "Three"
  print ".\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n.\n."
  
  Gtk.main()