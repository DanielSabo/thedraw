#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

# This needs to be imported first to avoid static initialization funkiness on Win32
import drawbuffer

import sys
import os
import json
import time
import argparse
import signal
#import cairo

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
#from gi.repository import GdkPixbuf
from gi.repository import Gegl

import dialogs
from drawcanvas import DrawCanvas, DrawCanvasBackground_Checkerboard, DrawCanvasBackground_Color
from settingswidget import ToolSettingsWidget
from layerswidget import LayersWidget
from brushpreviewpopup import BrushPreviewPopup
from gesturepopup import GesturePopup
from selectbox import SelectBox
import resourceloader

GObject.threads_init()

if not sys.stdout.isatty():
  from consolewidget import ConsoleWindow, tee_object
  w = ConsoleWindow()
  w.show_all()
  sys.stdout = tee_object(sys.__stdout__.write, w.append_text)
  sys.stderr = tee_object(sys.__stderr__.write, w.append_text)

MENU_DEF = """
<ui>
  <menubar name='MenuBar'>
    <menu action='FileMenu'>
      <menuitem action='FileNew' />
      <menuitem action='FileOpen' />
      <menuitem action='FileSave' />
      <menuitem action='FileSaveAs' />
      <menuitem action='FileExport' />
      <separator />
      <menuitem action='FileQuit' />
    </menu>
    <menu action='EditMenu'>
      <menuitem action='EditNewLayer' />
      <menuitem action='EditDuplicateLayer' />
      <separator />
      <menuitem action='EditUndo' />
      <menuitem action='EditRedo' />
    </menu>
    <menu action='CanvasMenu'>
      <menuitem action='CanvasBackgroundChecker' />
      <menuitem action='CanvasBackgroundWhite' />
      <menuitem action='CanvasBackgroundDark' />
    </menu>
    <menu action='ToolMenu'>
      <menuitem action='ToolIncBrushSize' />
      <menuitem action='ToolDecBrushSize' />
      <separator />
      <menuitem action='ToolGesturePopup' />
    </menu>
    <menu action='ViewMenu'>
      <menuitem action='ViewCurrentZoomPlaceholder' />
      <separator />
      <menuitem action='ViewZoomIn' />
      <menuitem action='ViewZoomOut' />
      <menuitem action='ViewZoomReset' />
      <menuitem action='ViewZoomFull' />
    </menu>
    <menu action='DebugMenu'>
      <menuitem action='DebugForceRedraw' />
      <menuitem action='DebugCopyStrokeData' />
      <menuitem action='DebugPasteStrokeData' />
    </menu>
    <menu action='HelpMenu'>
      <menuitem action='HelpShowToolsFolder' />
      <menuitem action='HelpAbout' />
    </menu>
  </menubar>
</ui>
"""

OSX_WINDOW_MENU_DEF = """
<ui>
  <menubar name='MenuBar'>
    <menu action='WindowMenu'>
      <menuitem action='WindowMenuMinimize' />
      <menuitem action='WindowMenuZoom' />
    </menu>
  </menubar>
</ui>
"""

# It would be a much better experience if we could thread this,
# however the current DrawCanvas save & load operations cause
# lots of thread unsafe events and general exploding.
# Until that can be refactored this window will just freeze the
# application until target_command finishes.
class ModalProgressWindow(Gtk.Window):
  def __init__(self):
    Gtk.Window.__init__(self, type=Gtk.WindowType.POPUP)
    self.set_type_hint(Gdk.WindowTypeHint.DIALOG)
    
    box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    self.spinner = Gtk.Spinner()
    self.spinner.set_size_request(32, 32)
    self.label = Gtk.Label()
    box.pack_start(self.spinner, False, False, 8)
    box.pack_start(self.label, True, True, 0)
    
    box.show_all()
    self.add(box)

  def set_label_text(self, text):
    self.label.set_text(text)

  def run(self, target_widget, target_command):
    x,y = target_widget.get_window().get_origin()[1:]
    allocation = target_widget.get_allocation()
    
    self.set_size_request(allocation.width - 40, 40)
    self.set_default_size(allocation.width - 40, 40)
    self.move(x + 20, y)
  
    self.set_transient_for(target_widget.get_toplevel())
    self.set_modal(True)
    self.show()
    self.spinner.start()
    # Flush draw commands before we start to avoid a blank window
    self.get_window().process_all_updates()
    target_command()
    self.hide()

class TheApp():
  def __init__(self):
    self.window = None
    self.canvas = None
    
    self.osxapp = None
    if "Quartz" in type(Gdk.Display.get_default()).__name__:
      from gi.repository import GtkosxApplication
      self.osxapp = GtkosxApplication.Application()
      self.osxapp.set_use_quartz_accelerators(False)

    self.device_tool_map = dict()
    self.current_canvas_device = None
    self.on_brush_settings_change_tool = None
    
    self.init_tools()

    self.init_window()
    
    if self.osxapp:
      self.uimanager.get_widget("/MenuBar/FileMenu/FileQuit").hide()
      self.osxapp.set_window_menu(self.uimanager.get_widget("/MenuBar/WindowMenu"))
      
      about_menu_item = self.uimanager.get_widget("/MenuBar/HelpMenu/HelpAbout")
      
      # Switch to a more OSX style name
      about_menu_item.set_label(about_menu_item.get_label() + " " + GLib.get_application_name())
      self.osxapp.insert_app_menu_item(about_menu_item, 0)
      
      self.osxapp.connect("NSApplicationOpenFile", self.on_osx_external_open)
      
      self.osxapp.ready()
  
  def init_tools(self):
    toolman = resourceloader.get_tool_manager()
    
    # Ensure the user directory exists
    
    user_dir = os.path.join(os.path.expanduser("~"), ".thedraw")
    
    try:
      os.makedirs(os.path.join(user_dir, "tools"), 0755)
    except:
      pass
    
    try:
      os.makedirs(os.path.join(user_dir, "engines"), 0755)
    except:
      pass
    
    toolman.add_tool_search_paths(
      os.path.join(os.path.abspath(os.path.dirname(__file__)), "tools"),
      os.path.join(user_dir, "tools")
                                 )

    toolman.add_engine_search_paths(
      os.path.join(os.path.abspath(os.path.dirname(__file__)), "tools"),
      os.path.join(user_dir, "engines")
                                   )
    
    self.tools = toolman.load_all_tools()
  
  def init_window(self):
    self.window = Gtk.Window()
    self.window.set_title("TheDraw")
    
    self.canvas = DrawCanvas()
    self.set_canvas_scale(0.5)
    self.window.set_default_size(1000, 600)
    
    try:
      self.window.set_icon_name("thedraw-logo")
    except:
      import traceback
      traceback.print_exc()
    
    self.init_menus()
    
    self.window.connect("delete-event", self.on_delete_event);
    self.window.connect("destroy", self.on_destroy);
    
    box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    self.window.add(box)
    
    menubar = self.uimanager.get_widget("/MenuBar")
    
    top_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    lower_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

    if self.osxapp:
      self.osxapp.set_menu_bar(menubar)
    else:
      top_box.pack_start(menubar, True, True, 0)
      
    # --- Gesture popup ---
    self.gesture_window = GesturePopup()
    self.gesture_window.connect("leave-notify-event", self.on_gesture_window_leave)
    
    # --- Preview window ---
    # Initialize here because it will be updated when the brush
    # controls are created.
    self.brush_preview_window = BrushPreviewPopup()
    
    self.canvas.connect("notify::brush", self.on_canvas_tool_change)
    self.canvas.connect("notify::filename", self.on_canvas_notify_filename)
    self.canvas.connect("notify::modified", self.on_canvas_notify_filename)
    self.canvas.notify("filename")
    
    # -- Tool box --
    right_tool_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    
    # --- Brush ComboBox ---
    self.brush_list_store = Gtk.ListStore(str, str)
    
    for tool_num, tool_id in enumerate(sorted(self.tools.iterkeys())):
      tool = self.tools[tool_id]
      name = tool.get_property("name")
      self.brush_list_store.append([name, tool_id])
    
    self.brush_list_combo = SelectBox()
    renderer_text = Gtk.CellRendererText()
    self.brush_list_combo.set_view(renderer_text, {"text":0})
    self.brush_list_combo.set_model(self.brush_list_store)
    
    self.brush_list_combo.connect("changed", self.on_brush_combo_select)
    
    right_tool_box.pack_start(self.brush_list_combo, False, False, 0)
    
    # --- Settings widget ---
    self.tool_settings_widget = ToolSettingsWidget()
    self.tool_settings_widget.set_tool(self.canvas.brush)
    
    # Set default brush
    self.canvas.set_property("brush", self.tools["RoundBrush"])
    
    # -- Wrap in an event box so we can get enter/leave events
    tool_settings_event_box = Gtk.EventBox()
    tool_settings_event_box.connect("enter-notify-event", self.on_settings_enter)
    tool_settings_event_box.connect("leave-notify-event", self.on_settings_leave)
    tool_settings_event_box.add(self.tool_settings_widget)
    
    right_tool_box.pack_start(tool_settings_event_box, False, False, 0)
    lower_box.pack_end(right_tool_box, False, False, 0)
    
    # ---
    right_tool_box.pack_start(Gtk.HSeparator(), False, False, 10)
    
    # --- Layers widget ---
    self.layers_widget = LayersWidget(self.canvas)
    right_tool_box.pack_start(self.layers_widget, False, False, 0)
    
    self.canvas.connect("notify::layers", lambda obj, prop : self.layers_widget.update_layers())
    
    # -- Final prep
    box.pack_start(top_box, False, False, 0)
    
    self.canvas.connect("scroll-event", self.on_canvas_scroll_event)
    lower_box.pack_start(self.canvas, True, True, 0)
    box.pack_end(lower_box, True, True, 0)
    
    self.window.connect("key-press-event", self.on_key_press_event)
    self.window.connect("motion-notify-event", self.on_motion)
    
    self.canvas.grab_focus()
  
  def on_motion(self, widget, event):
    device = event.get_source_device()
    
    if hash(device) != self.current_canvas_device:
      # Save the current device's tool
      if self.current_canvas_device is not None:
        self.device_tool_map[self.current_canvas_device] = self.canvas.brush
      
      # Find the tool associated with the new device
      if hash(device) in self.device_tool_map:
        tool = self.device_tool_map[hash(device)]
      else:
        if Gdk.InputSource.ERASER == device.get_source():
          tool = self.tools["RoundEraser"]
        else:
          tool = self.tools["RoundBrush"]
        self.device_tool_map[hash(device)] = tool
      self.canvas.set_property("brush", tool)
      self.current_canvas_device = hash(device)
  
  def init_menus(self):
    self.uimanager = Gtk.UIManager()
    self.uimanager.add_ui_from_string(MENU_DEF)
    
    accelgroup = self.uimanager.get_accel_group()
    self.window.add_accel_group(accelgroup)
    
    action_group = Gtk.ActionGroup("main_window_actions")
    
    action_group.add_actions([
      ("FileMenu", None, "File"),
      ("FileNew", Gtk.STOCK_NEW, "New...", None, None,
       self.on_file_new),
      ("FileOpen", Gtk.STOCK_OPEN, "Open...", None, None,
       self.on_file_open),
      ("FileSave", Gtk.STOCK_SAVE, "Save", None, None,
       self.on_file_save),
       # For some reason save as doesn't have a stock shortcut
      ("FileSaveAs", Gtk.STOCK_SAVE_AS, "Save As...", "<primary><shift>S", None,
       self.on_file_save_as),
      ("FileExport", Gtk.STOCK_SAVE_AS, "Export...", None, None,
       self.on_file_export),
       
      ("FileQuit", Gtk.STOCK_QUIT, "Quit", None, None,
       self.exit),
      ("EditMenu", None, "Edit"),
      ("EditUndo", Gtk.STOCK_UNDO, "Undo", "z", None,
       self.on_edit_undo),
      ("EditRedo", Gtk.STOCK_REDO, "Redo", "y", None,
       self.on_edit_redo),
      ("CanvasMenu", None, "Canvas"),
      ("CanvasBackgroundChecker", None, "Background: Checkerboard", None, None,
       lambda action : self.canvas.set_property("canvas-background",
                                                DrawCanvasBackground_Checkerboard())),
      ("CanvasBackgroundWhite", None, "Background: White", None, None,
       lambda action : self.canvas.set_property("canvas-background",
                                                DrawCanvasBackground_Color("#FFF"))),
      ("CanvasBackgroundDark", None, "Background: Dark", None, None,
       lambda action : self.canvas.set_property("canvas-background",
                                                DrawCanvasBackground_Color("#2e3846"))),
      ("ToolMenu", None, "Tool"),
      ("ToolGesturePopup", None, "Gesture", "c", None,
       lambda action : self.toggle_gesture_popup()),
      ("ToolIncBrushSize", None, "Increase Size", "f", None,
       lambda action : self.mod_brush_size(2.0)),
      ("ToolDecBrushSize", None, "Decrease Size", "d", None,
       lambda action : self.mod_brush_size(0.5)),
      ("ViewMenu", None, "View"),
      ("ViewZoomIn", None, "Zoom In", "<primary>equal", None,
       lambda action : self.set_canvas_scale(self.canvas.scale * 2)),
      ("ViewZoomOut", None, "Zoom Out", "<primary>minus", None,
       lambda action : self.set_canvas_scale(self.canvas.scale / 2)),
      ("ViewZoomReset", None, "Reset Zoom", "<primary>0", None,
       lambda action : self.set_canvas_scale(1.0)),
      ("ViewZoomFull", None, "Zoom to Image", None, None,
       lambda action : self.canvas.zoom_to_full()),
      ("DebugMenu", None, "Debug"),
      ("DebugForceRedraw", None, "Force Redraw", None, None,
       self.on_debug_force_redraw),
      ("DebugCopyStrokeData", None, "Copy Stroke Data", None, None,
       self.on_debug_copy_last_stroke),
      ("DebugPasteStrokeData", None, "Paste Stroke Data", None, None,
       self.on_debug_paste_stroke),
      ("HelpMenu", None, "Help"),
      ("HelpShowToolsFolder", None, "Show Tools Folder", None, None,
       self.on_show_tools_folder),
      ("HelpAbout", Gtk.STOCK_ABOUT, "About", None, None,
       lambda action : dialogs.AboutDialog().run()),
    ])
    
    self.view_menu_zoom_placeholder_action = Gtk.Action("ViewCurrentZoomPlaceholder",
                                                        "Current Zoom: ???%",
                                                        None,
                                                        None)
    
    self.view_menu_zoom_placeholder_action.set_sensitive(False)
    action_group.add_action(self.view_menu_zoom_placeholder_action)
    self.on_canvas_scale_change(None, None)
    self.canvas.connect("notify::scale", self.on_canvas_scale_change)
    
    self.uimanager.insert_action_group(action_group)
    
    # Layers actions
    layers_action_group = Gtk.ActionGroup("main_window_layers_actions")
    
    layers_action_group.add_actions([
      ("EditNewLayer", None, "New Layer", "n", None,
      lambda action : self.layers_widget.on_add_button(None)),
      ("EditDuplicateLayer", None, "Duplicate Layer", "<primary>d", None,
      lambda action : self.layers_widget.on_dup_button(None)),
    ])
    
    self.uimanager.insert_action_group(layers_action_group)
    
    
    # Add OSX specific Window menu
    if self.osxapp:
      self.uimanager.add_ui_from_string(OSX_WINDOW_MENU_DEF)
      
      def osx_zoom_window(window):
        if not hasattr(window, "osx_zoomed") or not window.osx_zoomed:
          window.maximize()
          window.osx_zoomed = True
        else:
          self.window.unmaximize()
          window.osx_zoomed = False
      
      osx_action_group = Gtk.ActionGroup("main_window_osx_actions")
      osx_action_group.add_actions([
        ("WindowMenu", None, "Window"),
        ("WindowMenuMinimize", None, "Minimize", "<primary>m", None,
          lambda widget : self.window.iconify()),
        ("WindowMenuZoom", None, "Zoom", None, None,
          lambda widget : osx_zoom_window(self.window)),
      ])
      self.uimanager.insert_action_group(osx_action_group)
  
  def on_file_new(self, action):
    if not self._check_canvas_clean():
      return
    
    self.canvas.new_drawing()
  
  def on_file_open(self, action):
    if not self._check_canvas_clean():
      return
    
    dialog = Gtk.FileChooserDialog("Open", self.window,
                                   Gtk.FileChooserAction.OPEN,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
    
    filter = Gtk.FileFilter()
    filter.add_mime_type("image/png")
    filter.add_pattern("*.png")
    filter.add_mime_type("image/openraster")
    filter.add_pattern("*.ora")
    dialog.set_filter(filter)
    
    if dialog.run() == Gtk.ResponseType.OK:
      filename = dialog.get_filename()
      dialog.destroy()
      
      prog_win = ModalProgressWindow()
      prog_win.set_label_text("Loading " + os.path.basename(filename))
      prog_win.run(self.window, lambda : self.canvas.new_drawing_from_filename(filename))
    else:
      dialog.destroy()
  
  def on_osx_external_open(self, osxapp, filename):
    """Called by OSXApplication when we get a request to open a file"""
    if filename.endswith(".png") or filename.endswith(".ora"):
      if not self._check_canvas_clean():
        return
      self.canvas.new_drawing_from_filename(filename)
  
  def on_file_save(self, action):
    if self.canvas.save_filename:
      filename = self.canvas.save_filename
      
      prog_win = ModalProgressWindow()
      prog_win.set_label_text("Saving " + os.path.basename(filename))
      prog_win.run(self.window, lambda : self.canvas.save_drawing_to_ora(filename))
      
    else:
      self.on_file_save_as(action)
  
  def on_file_save_as(self, action):
    dialog = Gtk.FileChooserDialog("Save As", self.window,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
    
    filter = Gtk.FileFilter()
    filter.add_mime_type("image/openraster")
    filter.add_pattern("*.ora")
    dialog.set_filter(filter)
    
    dialog.set_current_name("Untitled.ora")
    
    if dialog.run() == Gtk.ResponseType.OK:
      filename = dialog.get_filename()
      dialog.destroy()
      
      if os.path.exists(filename):
        if not self._check_overwrite(filename):
          return
      
      prog_win = ModalProgressWindow()
      prog_win.set_label_text("Saving " + os.path.basename(filename))
      prog_win.run(self.window, lambda : self.canvas.save_drawing_to_ora(filename))
    else:
      dialog.destroy()
  
  def on_file_export(self, action):
    dialog = Gtk.FileChooserDialog("Export", self.window,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
    
    filter = Gtk.FileFilter()
    filter.add_mime_type("image/png")
    filter.add_pattern("*.png")
    dialog.set_filter(filter)
    option_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    bit_depth_check = Gtk.CheckButton.new_with_label("Save 16bit image")
    bit_depth_check.set_active(False)
    background_check = Gtk.CheckButton.new_with_label("Save background")
    background_check.set_active(True)
    option_box.pack_start(bit_depth_check, True, True, 0)
    option_box.pack_start(background_check, True, True, 0)
    option_box.show_all()
    dialog.set_extra_widget(option_box)
    
    default_filename = "Untitled.png"
    
    if self.canvas.save_filename:
      just_the_name = os.path.splitext(os.path.basename(self.canvas.save_filename))[0]
      if just_the_name:
        default_filename = just_the_name + ".png"
    
    dialog.set_current_name(default_filename)
      
    if dialog.run() == Gtk.ResponseType.OK:
      filename = dialog.get_filename()
      sixteen_bit = bit_depth_check.get_active()
      with_background = background_check.get_active()
      dialog.destroy()
      
      if os.path.exists(filename):
        if not self._check_overwrite(filename):
          return
      
      prog_win = ModalProgressWindow()
      prog_win.set_label_text("Exporting " + os.path.basename(filename))
      prog_win.run(self.window,
                   lambda : self.canvas.save_drawing_to_png(filename, sixteen_bit, with_background))
    else:
      dialog.destroy()
  
  def on_edit_undo(self, widget):
    self.canvas.undo()
    
  def on_edit_redo(self, widget):
    self.canvas.redo()
  
  def mod_brush_size(self, size_mod):
    if not self.canvas.brush:
      return

    size = self.canvas.brush.get_property("size") * size_mod
    pspec = dict([(i.name, i) for i in self.canvas.brush.props])["size"]
    
    if size < pspec.minimum:
      size = pspec.minimum
    elif size > pspec.maximum:
      size = pspec.maximum
    self.canvas.brush.set_property("size", size)
  
  def set_canvas_scale(self, scale):
    pspec = dict([(i.name, i) for i in self.canvas.props])["scale"]
    if scale < pspec.minimum:
      scale = pspec.minimum
    elif scale > pspec.maximum:
      scale = pspec.maximum
    self.canvas.set_scale_centered(scale)
    
  def on_debug_force_redraw(self, action):
    self.canvas.invalidate()
  
  def on_debug_copy_last_stroke(self, action):
    if self.canvas.current_stroke:
      clipboard = Gtk.Clipboard.get_for_display(self.window.get_display(), Gdk.SELECTION_CLIPBOARD)
      stroke_string = json.dumps(self.canvas.current_stroke.current_stroke_points)
      clipboard.set_text(stroke_string, -1)
      clipboard.store()
  
  def on_debug_paste_stroke(self, action):
    clipboard = Gtk.Clipboard.get_for_display(self.window.get_display(), Gdk.SELECTION_CLIPBOARD)
    stroke_string = clipboard.wait_for_text()
    try:
      points = json.loads(stroke_string)
    except:
      return
    self.canvas.stroke_points(points)
  
  def on_show_tools_folder(self, action):
    #FIXME: We should only generate this path in one location
    p = os.path.join(os.path.expanduser("~"), ".thedraw")
    if sys.platform=='win32':
      os.startfile(p)
    elif sys.platform=='darwin':
      import subprocess
      subprocess.Popen(['/usr/bin/open', p])
    else:
      Gtk.show_uri(None, "file://" + p, Gdk.CURRENT_TIME)
  
  def on_brush_combo_select(self, widget):
    tree_iter = self.brush_list_combo.get_active_iter()
    new_tool = self.tools[self.brush_list_store[tree_iter][1]]
    self.canvas.set_property("brush", new_tool)
  
  def on_canvas_notify_filename(self, obj, prop):
    filename = obj.get_property("filename")
    modified = obj.get_property("modified")
    new_title = "???"
    if filename is not None:
      new_title = os.path.basename(filename)
    else:
      new_title = "Unsaved Drawing"
    
    if modified:
      new_title = "*" + new_title
    
    new_title = new_title + " (sRGB)"
    
    self.window.set_title(new_title)
  
  def on_canvas_tool_change(self, obj, prop):
    # Don't try to update the combo list if there's no tool
    if self.canvas.brush:
      # Get the active tool id
      active_tool_id = None
      for k, v in self.tools.items():
        if v is self.canvas.brush:
          active_tool_id = k
          break
    
      active_tool_num = 0
      for tool_num, (tool_name, tool_id) in enumerate(self.brush_list_store):
        if tool_id == active_tool_id:
          active_tool_num = tool_num
          break

      self.brush_list_combo.set_active(active_tool_num)
      self.tool_settings_widget.set_tool(self.canvas.brush)
    
    if self.brush_preview_window.get_visible():
      self.brush_preview_window.set_brush(self.canvas.brush, scale=self.canvas.scale)
    
    #FIXME: There has got to be a cleaner way to do this...
    if self.on_brush_settings_change_tool is not None:
      self.on_brush_settings_change_tool.disconnect(self.on_brush_settings_change_id)
      self.on_brush_settings_change_tool = None
    self.on_brush_settings_change_tool = self.canvas.brush
    if self.on_brush_settings_change_tool:
      self.on_brush_settings_change_id = self.canvas.brush.connect("modified",
                                                                   self.on_brush_settings_change)

  def on_canvas_scale_change(self, obj, prop):
    zoom_label = "Current Zoom: %.2f%%" % (self.canvas.scale * 100)
    self.view_menu_zoom_placeholder_action.set_label(zoom_label)
  
  def on_canvas_scroll_event(self, widget, event):
    if event.state & Gdk.ModifierType.CONTROL_MASK:
      if event.direction == Gdk.ScrollDirection.UP or \
         event.direction == Gdk.ScrollDirection.SMOOTH and event.delta_y > 0.0:
        self.set_canvas_scale(self.canvas.scale / 2)
      elif event.direction == Gdk.ScrollDirection.DOWN or \
         event.direction == Gdk.ScrollDirection.SMOOTH and event.delta_y < 0.0:
        self.set_canvas_scale(self.canvas.scale * 2)
      return True
    return False # Pass the event on to the canvas
  
  def on_brush_settings_change(self, brush):
    if self.brush_preview_window.get_visible():
      self.brush_preview_window.set_brush(brush, scale=self.canvas.scale)
  
  def on_settings_enter(self, widget, event):
    if event.detail != Gdk.NotifyType.INFERIOR:
      #allocation = widget.get_allocation()
      x,y = widget.get_window().get_origin()[1:]
      width, height = self.brush_preview_window.get_size()
      x -= width + 12
      #x += allocation.x
      #y += allocation.y + allocation.height + 4
      #y += allocation.height + 4
      
      #FIXME: Avoid putting the window offscreen
      self.brush_preview_window.set_brush(self.canvas.brush, scale=self.canvas.scale)
      
      self.brush_preview_window.move(x, y)
      self.brush_preview_window.show_all()
  
  def on_settings_leave(self, widget, event):
    if event.detail != Gdk.NotifyType.INFERIOR:
      self.brush_preview_window.hide()
  
  def on_key_press_event(self, window, event):
    if window.get_focus() == self.canvas:
      # The canvas has focus, let the normal key handler take over
      #print("Key:Canvas")
      return False
    if event.state & ~(Gdk.ModifierType.SHIFT_MASK | \
                       Gdk.ModifierType.LOCK_MASK  | \
                       Gdk.ModifierType.MOD2_MASK): # MOD2_MASK = NumLock
      # Modefier keys pressed, let the normal key handler take over
      #print("Key:Mod", int(event.state))
      return False
    if not window.propagate_key_event(event):
      # The focus widget didn't take the key event, try it as an accelerator
      #print("Key:Not Prop")
      if window.activate_key(event):
        return True
      
      # If nothing else has used the event, try some hardcoded keys
      if not event.state & ~(Gdk.ModifierType.LOCK_MASK  | \
                             Gdk.ModifierType.MOD2_MASK) \
         and event.keyval == Gdk.KEY_Escape:
        self.canvas.grab_focus()
      
    return True
  
  def toggle_gesture_popup(self):
    if self.gesture_window.get_visible():
      self.gesture_window.hide()
    else:
      x, y = Gdk.Display.get_default().get_device_manager().get_client_pointer().get_position()[1:]
      if "size" in [i.name for i in self.canvas.brush.props]:
        self.gesture_window.showtime = time.time()
        self.gesture_window.show_all()
        self.gesture_window.center_at(x, y)
        self.gesture_window.set_brush(self.canvas.brush, self.canvas.scale, "size")

  def on_gesture_window_leave(self, widget, event):
    # This is probably a GTK bug, but on OSX we get a false
    # enter/leave event if the mouse was moved just before
    # the window is show.
    if time.time() - self.gesture_window.showtime < 0.01:
      pass
    else:
      self.gesture_window.hide()

  def run(self):
    width, height = self.window.get_size()
    self.window.move((Gdk.Screen().width() - width) // 2, (Gdk.Screen().height() - height) // 2)
    
    self.window.show_all()
    
    Gtk.main()

  def _check_canvas_clean(self):
    """If the file has been modified prompt to save it.
       
       Returns:
       True it's ok to destroy the canvas
       False if the user canceled"""
    if self.canvas.get_property("modified"):
      # OSX button order is Discard, Cancel, Save (default)
      # Gnome button order is Discard, Cancel, Save (default)
      # Windows button order is Save (default), Discard, Cancel
      dialog = Gtk.MessageDialog(self.window,
                                 Gtk.DialogFlags.MODAL,
                                 Gtk.MessageType.QUESTION,
                                 Gtk.ButtonsType.NONE,
                                 "There are unsaved changes, are you sure you want to discard this drawing?")
      
      discard_button = Gtk.Button("Discard")
      discard_button.set_image(Gtk.Image.new_from_stock("gtk-delete", Gtk.IconSize.BUTTON))
      discard_button.show_all()
      
      dialog.add_action_widget(discard_button, Gtk.ResponseType.OK)
      dialog.add_button("gtk-cancel", Gtk.ResponseType.CANCEL)
      dialog.add_button("gtk-save", 1)
      dialog.set_default_response(1)
      
      dialog_response = dialog.run()
      
      dialog.destroy()
      if dialog_response == Gtk.ResponseType.OK:
        return True
      elif dialog_response == 1:
        self.on_file_save(self)
        if not self.canvas.get_property("modified"):
          return True
    else:
      return True
    return False

  def _check_overwrite(self, filename):
    dialog = Gtk.MessageDialog(self.window,
                               Gtk.DialogFlags.MODAL,
                               Gtk.MessageType.QUESTION,
                               Gtk.ButtonsType.NONE,
                               "A file with this name already exists, are you sure you want to overwrite it?\n\n%s" % filename)
    
    replace_button = Gtk.Button("Replace file")
    replace_button.set_image(Gtk.Image.new_from_stock("gtk-ok", Gtk.IconSize.BUTTON))
    replace_button.show_all()
    
    dialog.add_action_widget(replace_button, Gtk.ResponseType.OK)
    dialog.add_button("gtk-cancel", Gtk.ResponseType.CANCEL)
    dialog.set_default_response(Gtk.ResponseType.CANCEL)
    
    dialog_response = dialog.run()
    
    dialog.destroy()
    if dialog_response == Gtk.ResponseType.OK:
      return True
    return False

  def exit(self, widget):
    if self._check_canvas_clean():
      self.window.destroy()
  
  def on_destroy(self, widget):
    # Destroy before disconnecting brush, otherwise the device event may reconnect it
    self.canvas.destroy()
    self.canvas = None
    self.tool_settings_widget.set_tool(None)
    self.tool_settings_widget = None
    self.layers_widget = None
    self.brush_preview_window.destroy()
    self.brush_preview_window = None
    self.tools = None
    self.device_tool_map = None
    
    if self.on_brush_settings_change_tool is not None:
      self.on_brush_settings_change_tool.disconnect(self.on_brush_settings_change_id)
      self.on_brush_settings_change_tool = None
    
    self.window = None
    
    Gtk.main_quit()
  
  def on_delete_event(self, widget, event):
    self.exit(widget)
    return True # Block the default call to window.destroy()

def main():
  try:
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug-times",
                        help="print timing information",
                        action="store_true")
    parser.add_argument("filename",
                        nargs="?")
    args = parser.parse_args()
  except SystemExit:
    raise
  
  Gegl.init(None)
  Gegl.config().set_property("swap", "RAM")
  Gegl.config().set_property("use-opencl", False)
  #Gegl.config().set_property("babl-tolerance", 0.00015)
  Gegl.config().set_property("tile-cache-size", 1024*1024*1024)
  drawbuffer.init()
  
  gegl_module_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "geglop")
  Gegl.load_module_directory(gegl_module_dir)

  GLib.set_application_name("TheDraw")
  GLib.set_prgname("thedraw")
  
  resourceloader.init_stock()
  
  app = TheApp()
  if args.debug_times:
    app.canvas.debug__print_render_time = True
  if args.filename:
    try:
      app.canvas.new_drawing_from_filename(args.filename)
    except:
      import traceback
      traceback.print_exc()
  
  sig_source = None
  if hasattr(GLib, "unix_signal_add_full"):
    sig_source = GLib.unix_signal_add_full(GLib.PRIORITY_HIGH, signal.SIGINT, lambda user_data : app.exit(None), None)
  
  app.run()
  if sig_source:
    GLib.source_remove(sig_source)
  app = None
  
  # For some reason when a GObject callback eats an exception it sets the sys.* values
  # but not sys.exc_info. Manualy purge the traceback here so our widgets can be collected.
  sys.exc_clear()
  try:
    del sys.last_type
    del sys.last_value
    del sys.last_traceback
    print("purged traceback object from sys")
  except:
    pass

  print("Done")
  Gegl.exit()

if __name__ == "__main__":
  main()
