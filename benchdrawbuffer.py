#!/usr/bin/env python
import drawbuffer
import timeit
import argparse

from gi.repository import Gegl
Gegl.init(None)
Gegl.config().set_property("swap", "RAM")
Gegl.config().set_property("use-opencl", False)
drawbuffer.init()

parser = argparse.ArgumentParser()
parser.add_argument("--circle",
                    action="store_true")
parser.add_argument("--mask-scale",
                    action="store_true")
parser.add_argument("--mask-with-size",
                    action="store_true")
parser.add_argument("--mask-apply",
                    action="store_true")
parser.add_argument("--mask-grow",
                    action="store_true")
parser.add_argument("--duplicate",
                    action="store_true")
parser.add_argument("--blit-to",
                    action="store_true")
parser.add_argument("--pause",
                    action="store_true")
args = parser.parse_args()

bench_circle = args.circle
bench_mask_scale = args.mask_scale
bench_mask_with_size = args.mask_with_size
bench_mask_apply = args.mask_apply
bench_mask_grow = args.mask_grow
bench_duplicate = args.duplicate
bench_blit = args.blit_to

if not any([bench_circle, bench_mask_scale, bench_mask_with_size, bench_mask_apply, bench_mask_grow, bench_duplicate, bench_blit]):
  bench_circle = True
  bench_mask_scale = True
  bench_mask_apply = True

if args.pause:
  raw_input("Press enter to begin")

if bench_circle:
  timeit_setup = "import drawbuffer\nops_buf = drawbuffer.DrawBuffer(100,100,color=(0.0, 0.0, 0.0, 1.0))"
  timeit_code  = "ops_buf.circle(50, 50, 3)"
  timeit_count = 10000
  print "circle r=3, 100x100 buffer"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = "import drawbuffer\nops_buf = drawbuffer.DrawBuffer(100,100,color=(0.0, 0.0, 0.0, 1.0))"
  timeit_code  = "ops_buf.circle(50, 50, 30)"
  timeit_count = 10000
  print "circle r=30, 100x100 buffer"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = "import drawbuffer\nops_buf = drawbuffer.DrawBuffer(300,300,color=(0.0, 0.0, 0.0, 1.0))"
  timeit_code  = "ops_buf.circle(150, 150, 100)"
  timeit_count = 10000
  print "circle r=100, 300x300 buffer"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_mask_scale:
  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
oils_mask_shrunk = drawbuffer.MaskBuffer(20, 20)
"""
  timeit_code  = "oils_mask.scale_to(oils_mask_shrunk)"
  timeit_count = 10000
  print "mask.scale_to 20x20 target"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
oils_mask_shrunk = drawbuffer.MaskBuffer(50, 50)
"""
  timeit_code  = "oils_mask.scale_to(oils_mask_shrunk)"
  timeit_count = 10000
  print "mask.scale_to 50x50 target"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
oils_mask_shrunk = drawbuffer.MaskBuffer(200, 200)
"""
  timeit_code  = "oils_mask.scale_to(oils_mask_shrunk)"
  timeit_count = 10000
  print "mask.scale_to 200x200 target"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_mask_with_size:
  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
"""
  timeit_code  = "oils_mask.scale_with_size(0, 0, 20, 20)"
  timeit_count = 10000
  print "mask.scale_with_size 20x20 target"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
"""
  timeit_code  = "oils_mask.scale_with_size(0, 0, 50, 50)"
  timeit_count = 10000
  print "mask.scale_with_size 50x50 target"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
"""
  timeit_code  = "oils_mask.scale_with_size(0, 0, 200, 200)"
  timeit_count = 10000
  print "mask.scale_with_size 200x200 target"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_mask_apply:
  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
oils_mask_shrunk = drawbuffer.MaskBuffer(200, 200)
oils_mask.scale_to(oils_mask_shrunk)
ops_buf = drawbuffer.DrawBuffer(250,250,color=(1.0, 1.0, 1.0, 1.0))
"""
  timeit_code  = "ops_buf.apply_mask(25, 25, oils_mask_shrunk, (0.0, 0.0, 0.0, 1.0), blendmode=drawbuffer.BLEND_MODE_OVER)"
  timeit_count = 10000
  print "ops_buf.apply_mask 200x200 mask, 250x250 target blend=OVER"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_mask_grow:
  timeit_setup = """
import drawbuffer
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
oils_mask_shrunk = drawbuffer.MaskBuffer(200, 200)
oils_mask.scale_to(oils_mask_shrunk)
ops_buf = drawbuffer.DrawBuffer(250,250,color=(1.0, 1.0, 1.0, 1.0))
"""
  timeit_code  = "ops_buf.apply_mask(25, 25, oils_mask_shrunk, (0.0, 0.0, 0.0, 1.0), blendmode=drawbuffer.BLEND_MODE_GROW_ALPHA)"
  timeit_count = 10000
  print "ops_buf.apply_mask 200x200 mask, 250x250 target blend=GROW"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_duplicate:
  timeit_setup = """
import drawbuffer
ops_buf = drawbuffer.DrawBuffer(500,500)
"""
  timeit_code  = "ops_buf.duplicate()"
  timeit_count = 10000
  print "ops_buf.duplicate 500,500"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

  timeit_setup = """
import drawbuffer
ops_buf = drawbuffer.DrawBuffer(500,500)
"""
  timeit_code  = "ops_buf.duplicate_tiles()"
  timeit_count = 10000
  print "ops_buf.duplicate_tiles 500,500"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_blit:
  timeit_setup = """
import drawbuffer
ops_buf = drawbuffer.DrawBuffer(100,100,color=(1.0, 1.0, 1.0, 0.8))
dst_buf = drawbuffer.DrawBuffer(100,100,color=(0.0, 0.0, 0.0, 1.0))
"""
  timeit_code = "ops_buf.blit_to(dst_buf)"
  timeit_count = 10000
  print "blit 100x100"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result
