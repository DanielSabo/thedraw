from __future__ import print_function

import cairo
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gegl

import drawbuffer
import layers

import threading
import math

class BrushPreviewRenderThread(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    self.in_brush_id = None
    self.in_brush = None
    self.in_width = None
    self.in_height = None
    self.in_callback = None
    self.new_request = False
    self.in_terminate = False
    
    self.input_lock = threading.Condition()
  
  def run(self):
    while True:
      with self.input_lock:
        if not self.new_request:
          self.input_lock.wait()
        self.new_request = False
        
        if self.in_terminate:
          self.in_brush_id = None
          self.in_brush    = None
          return
        
        brush_id = self.in_brush_id
        brush    = self.in_brush
        width    = self.in_width
        height   = self.in_height
        callback = self.in_callback
      
      surface = self.render(brush, width, height)
      
      if (hasattr(Gdk, "threads_add_idle")):
        Gdk.threads_add_idle(200, callback, (brush_id, surface))
      else:
        Gdk.threads_add_idle_full(200, callback, (brush_id, surface))
  
  def render(self, brush, width, height):
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)

    if brush.get_property("size") > 200.0:
      # Brush to large to preview, give up
      cr = cairo.Context(surface)
      cr.set_source_rgb(1.0, 1.0, 1.0)
      cr.set_operator(cairo.OPERATOR_SOURCE)
      cr.paint()
      return surface
    
    preview_buf = drawbuffer.DrawBuffer(width,
                                        height,
                                        color=(1.0, 1.0, 1.0))

    stroke_buf = drawbuffer.DrawBuffer(width,
                                       height,
                                       color=(1.0, 1.0, 1.0, 0.0))
                                       
    preview_layer = layers.Layer(preview_buf)
    stroke_layer = layers.Layer(stroke_buf)
    
    def apply_stroke():
      stroke_layer.merge_down(preview_layer)
      stroke_buf.fill(0, 0, stroke_buf.width, stroke_buf.height, (1.0, 1.0, 1.0, 0.0))
      
    stroke = brush.new_stroke()
    
    points = []
    
    sin_stroke_len = width - 20
    
    for x in range(10, width - 10, 1):
      y = math.sin(6.28 * x / (sin_stroke_len)) * (height / 4) + height / 2
      pressure = float(x - 10) / (sin_stroke_len)
      if pressure < 0.25:
        pressure = 4 * pressure
      elif pressure < 0.75:
        pressure = 1.0
      else:
        pressure = 1.0 - pressure
        pressure = 4 * pressure
      points.append((x,y,pressure))
    stroke.stroke_points(stroke_layer, points)
    apply_stroke()
    
    stroke.stroke_points(stroke_layer, [(0.10 * (width - 20), 0.25 * (height - 20), 0.2)])
    apply_stroke()
    stroke.stroke_points(stroke_layer, [(0.15 * (width - 20), 0.35 * (height - 20), 0.4)])
    apply_stroke()
    stroke.stroke_points(stroke_layer, [(0.20 * (width - 20), 0.25 * (height - 20), 0.6)])
    apply_stroke()
    stroke.stroke_points(stroke_layer, [(0.25 * (width - 20), 0.35 * (height - 20), 0.8)])
    apply_stroke()
    stroke.stroke_points(stroke_layer, [(0.30 * (width - 20), 0.25 * (height - 20), 1.0)])
    apply_stroke()
    
    ptn = Gegl.Node()
    dst = drawbuffer.gegl_cairo_sink_node(ptn, surface)
    layer_node = preview_layer.get_graph(ptn)
    
    layer_node.connect_to("output", dst, "input")
    
    dst.process()
    
    return surface
  
  def request_preview(self, brush_id, brush, scale, width, height, callback):
    with self.input_lock:
      self.in_brush    = brush.clone()
      pspecs = dict([(i.name, i) for i in self.in_brush.props])
      if "size" in pspecs:
        pspec = pspecs["size"]
        new_value = self.in_brush.get_property("size") * scale
        new_value = max(min(new_value, pspec.maximum), pspec.minimum)
        self.in_brush.set_property("size", new_value)
      self.in_brush_id = brush_id
      self.in_width    = width
      self.in_height   = height
      self.in_callback = callback
      
      self.new_request = True
      self.input_lock.notify()

  def request_stop(self):
    with self.input_lock:
      self.new_request  = True
      self.in_terminate = True
      self.input_lock.notify()

class BrushPreviewWidget(Gtk.DrawingArea):
  def __init__(self, width = 0, height = 0):
    Gtk.DrawingArea.__init__(self)
    
    self.connect("draw", self.on_draw)
    
    self.surface = None
    self.width   = width
    self.height  = height
    
    self.render_thread = BrushPreviewRenderThread()
    self.render_thread.daemon = True
    self.render_thread.start()
    
    self.connect("destroy", self.on_destroy);
    self.connect("configure-event", self.on_configure_event)
  
  def on_configure_event(self, widget, event):
    self.width  = event.width
    self.height = event.height

  def on_destroy(self, widget):
    # Block until the render thread stops, this avoids trying
    # to exit while the thread may still be using GEGL objects.
    self.render_thread.request_stop()
    self.render_thread.join(5)
    
  def render_thread_callback(self, data):
    self.surface = data[1]
    self.queue_draw()
  
  def on_draw(self, widget, cr):
    if self.surface:
      cr.set_source_surface(self.surface)
      cr.set_operator(cairo.OPERATOR_SOURCE)
      cr.paint()
    else:
      cr.set_source_rgb(1.0, 1.0, 1.0)
      cr.set_operator(cairo.OPERATOR_SOURCE)
      cr.paint()
  
  def set_brush(self, brush, scale=1.0):
    if self.width < 1 or self.height < 1:
      return
    
    if brush:
      self.render_thread.request_preview(None, brush, scale,
                                         self.width, self.height,
                                         self.render_thread_callback)

class BrushPreviewPopup(Gtk.Window):
  def __init__(self):
    Gtk.Window.__init__(self, Gtk.WindowType.POPUP)
    self.set_type_hint(Gdk.WindowTypeHint.UTILITY)
    
    self.width   = 400
    self.height  = 100
    self.padding = 2
    
    self.set_default_size(self.width + self.padding, self.height + self.padding)    

    vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    
    self.preview_widget = BrushPreviewWidget(width=self.width, height=self.height)
    hbox.pack_start(self.preview_widget, True, True, self.padding)
    vbox.pack_start(hbox, True, True, self.padding)
    self.add(vbox)
    
    vbox.show_all()
  
  def set_brush(self, brush, scale=1.0):
    self.preview_widget.set_brush(brush, scale)
