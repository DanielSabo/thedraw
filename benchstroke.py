#!/usr/bin/env python
import drawbuffer
import timeit
import argparse

drawbuffer.init()

parser = argparse.ArgumentParser()
parser.add_argument("--round-brush",
                    action="store_true")
parser.add_argument("--image-brush",
                    action="store_true")
parser.add_argument("--pause",
                    action="store_true")
args = parser.parse_args()

bench_round_brush = args.round_brush
bench_image_brush = args.image_brush

if not any([bench_round_brush, bench_image_brush]):
  bench_round_brush = True
  bench_image_brush = True

if args.pause:
  raw_input("Press enter to begin")

if bench_round_brush:
  timeit_setup = """
import drawbuffer
import layers
from tools.RoundBrush import RoundBrush
test_layer = layers.Layer(drawbuffer.DrawBuffer(1000,200,color=(1.0, 1.0, 1.0, 1.0)))
points = [(i, 100, 1.0) for i in range(100, 901, 5)]
brush = RoundBrush()
brush.set_property("size", 48)
"""
  timeit_code  = """
stroke = brush.new_stroke()
stroke.stroke_points(test_layer, points)
"""
  timeit_count = 100
  print "tools.RoundBrush size 48"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result

if bench_image_brush:
  timeit_setup = """
import drawbuffer
import layers
from tools.ImageBrush import ImageBrush
test_layer = layers.Layer(drawbuffer.DrawBuffer(1000,200,color=(1.0, 1.0, 1.0, 1.0)))
points = [(i, 100, 1.0) for i in range(100, 901, 5)]
brush = ImageBrush()
oils_mask = drawbuffer.maskbuffer_from_png("tools/gimp-oils-02.png")
brush.set_property("size", 48)
brush.set_property("mask", oils_mask)
"""
  timeit_code  = """
stroke = brush.new_stroke()
stroke.stroke_points(test_layer, points)
"""
  timeit_count = 100
  print "tools.ImageBrush size 48"
  result = timeit.timeit(timeit_code, timeit_setup, number=timeit_count)
  print " ", result