from __future__ import print_function
from __future__ import division

import drawbuffer
import layers

import os
import shutil
import tempfile
import zipfile
import xml.etree.ElementTree as ElementTree
from util import rect_union

def _get_ora_mode_name(our_name):
  mode_map = {
    layers.LayerModes.NORMAL   : "svg:src-over",
    layers.LayerModes.MULTIPLY : "svg:multiply",
    layers.LayerModes.BURN     : "svg:color-burn",
    layers.LayerModes.DODGE    : "svg:color-dodge",
    layers.LayerModes.SCREEN   : "svg:screen",
  }

  if (our_name in mode_map):
    return mode_map[our_name]
  return "svg:src-over"

def _get_layer_mode(ora_mode):
  mode_map = {
    "svg:src-over"    : layers.LayerModes.NORMAL,
    "svg:multiply"    : layers.LayerModes.MULTIPLY,
    "svg:color-burn"  : layers.LayerModes.BURN,
    "svg:color-dodge" : layers.LayerModes.DODGE,
    "svg:screen"      : layers.LayerModes.SCREEN,
  }

  if (ora_mode in mode_map):
    return mode_map[ora_mode]
  return layers.LayerModes.NORMAL

def write_ora(layers_list, filename):
  dir_name = tempfile.mkdtemp(prefix="ora-tmp-")
  try:
    data_dir = os.path.join(dir_name, "data")
    os.mkdir(data_dir)
    
    with open(os.path.join(dir_name, "mimetype"),"w") as f:
      f.write("image/openraster")
    
    extent = (0, 0, 0, 0)
    for l in layers_list:
      extent = rect_union(extent, (l.x, l.y, l.width, l.height))
    offset_x = extent[0]
    offset_y = extent[1]
    width  = extent[2]
    height = extent[3]
    
    xml_root = ElementTree.Element("image", {"w":str(width), "h":str(height)})
    stack_node = ElementTree.Element("stack")
    xml_root.append(stack_node)
    
    png_filenames = list()
    
    for i, l in enumerate(reversed(layers_list)):
      png_filename = "layer%03d.png" % i
      png_filenames.append(png_filename)
      
      png_path = os.path.join(data_dir, png_filename)
      #print("Will write", l.drawbuf, "to", png_path)
      l.drawbuf.to_png(png_path)
      layer_xml_node = ElementTree.Element("layer", {"src":"data/" + png_filename})

      if l.name:
        layer_xml_node.set("name", l.name)
      
      if l.visible:
        layer_xml_node.set("visibility", "visible")
      else:
        layer_xml_node.set("visibility", "hidden")
      
      layer_xml_node.set("composite-op", _get_ora_mode_name(l.mode))
      layer_xml_node.set("opacity", "%f" % l.opacity)
      layer_xml_node.set("x", "%d" % (l.x - offset_x))
      layer_xml_node.set("y", "%d" % (l.y - offset_y))
      
      stack_node.append(layer_xml_node)

    #print("Writing", os.path.join(dir_name, "stack.xml"))
    #print(ElementTree.tostring(xml_root))
    with open(os.path.join(dir_name, "stack.xml"),"w") as f:
      f.write("<?xml version='1.0' encoding='UTF-8'?>\n")
      f.write(ElementTree.tostring(xml_root, encoding="utf-8"))
    
    ora_file = zipfile.ZipFile(filename, "w")
    ora_file.write(os.path.join(dir_name, "mimetype"), "mimetype")
    ora_file.write(os.path.join(dir_name, "stack.xml"), "stack.xml")
    for layer_file_name in png_filenames:
      ora_file.write(os.path.join(dir_name, "data", layer_file_name), "data/" + layer_file_name)
    ora_file.close()
  finally:
    shutil.rmtree(dir_name)

def read_ora(filename):
  """Returns (widht, height, layers)"""
  dir_name = tempfile.mkdtemp(prefix="ora-tmp-")
  
  try:
    ora_file = zipfile.ZipFile(filename, "r")
    ora_file.extractall(dir_name)
    
    layer_list = list()
    
    with open(os.path.join(dir_name, "stack.xml"),"r") as f:
      xml_root = ElementTree.fromstring(f.read())
      
      width  = int(xml_root.get("w"))
      height = int(xml_root.get("h"))
      
      for layer_xml_node in xml_root.find("stack").findall("layer"):
        png_filename = layer_xml_node.get("src")
        drawbuf = drawbuffer.drawbuffer_from_png(os.path.join(dir_name, png_filename))
        
        x, y = 0, 0
        if layer_xml_node.get("x") is not None:
          x = int(layer_xml_node.get("x"))
        if layer_xml_node.get("y") is not None:
          y = int(layer_xml_node.get("y"))
        
        if x or y:
          translated_drawbuf = drawbuffer.DrawBuffer(x=x, y=y, width=drawbuf.width, height=drawbuf.height)
          drawbuf.blit_to(translated_drawbuf, x, y)
          drawbuf = translated_drawbuf
        
        new_layer = layers.Layer(drawbuf)
        
        if layer_xml_node.get("name") is not None:
          new_layer.name = layer_xml_node.get("name")
        if layer_xml_node.get("visibility") == "hidden":
          new_layer.visible = False
        if layer_xml_node.get("opacity") is not None:
          new_layer.set_opacity(float(layer_xml_node.get("opacity")))
        if layer_xml_node.get("composite-op") is not None:
          new_layer.set_mode(_get_layer_mode(layer_xml_node.get("composite-op")))
        
        layer_list.append(new_layer)
    layer_list.reverse()
    return (width, height, layer_list)
  finally:
    shutil.rmtree(dir_name)

if __name__ == "__main__":
  drawbuffer.init()
  layers_list = [layers.Layer(width=100, height=100),
                 layers.Layer(width=100, height=100)]
  
  write_ora(layers_list, "/tmp/test.ora")
  
  del layers_list
  
  w,h,layers_list = read_ora("/tmp/test.ora")
  
  print(w, h, layers_list)
