from __future__ import print_function
from __future__ import division

from collections import namedtuple
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GObject

from colorbutton import PopupColorButton
import drawbuffer

GPropSetting = namedtuple("GPropSetting", ["prop_name"])
GPropSlider  = namedtuple("GPropSlider",  ["prop_name"])
GPropColor   = namedtuple("GPropColor",   ["prop_name"])


SettingWidgetData = namedtuple("SettingWidgetData",
                               ["setting", 
                                "box",
                                "widget",
                                "notify_id",
                                "value_id"])

class ToolSettingsWidget(Gtk.Box):
  def __init__(self, tool=None):
    Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
    
    self.tool = None
    self.widgets = {}
    
    self.set_tool(None)
  
  def add_entry(self, setting, pspec):
    box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    label_widget = Gtk.Label(pspec.nick)
    entry_widget = Gtk.Entry()
    entry_widget.set_text("%.2f" % self.tool.get_property(pspec.name))
    box.set_tooltip_text(pspec.blurb)
    
    #FIXME: Initial value
    
    box.pack_start(label_widget, False, False, 0)
    
    inc_button = Gtk.Button.new_with_label("+")
    inc_button.set_size_request(22,22)
    
    def incr_button(widget):
      new_value = self.tool.get_property(pspec.name)
      new_value = new_value * 2
      new_value = max(min(new_value, pspec.maximum), pspec.minimum)
      new_value = self.tool.set_property(pspec.name, new_value)
    
    inc_button.connect("clicked", incr_button)
    box.pack_end(inc_button, False, False, 0)
    
    dec_button = Gtk.Button.new_with_label("-")
    dec_button.set_size_request(22,22)
    
    def decr_button(widget):
      new_value = self.tool.get_property(pspec.name)
      new_value = new_value / 2
      new_value = max(min(new_value, pspec.maximum), pspec.minimum)
      new_value = self.tool.set_property(pspec.name, new_value)
    
    dec_button.connect("clicked", decr_button)
    box.pack_end(dec_button, False, False, 0)
    
    box.pack_end(entry_widget, False, False, 0)
    
    
    notify_id = self.tool.connect("notify::" + pspec.name,
                                  self.on_tool_value_notify_entry)
                                  
    value_id  = entry_widget.connect("activate",
                                     self.on_entry_value_change,
                                     pspec.name,
                                     pspec)
    
    self.pack_start(box, False, False, 0)
    
    self.widgets[pspec.name] = SettingWidgetData(setting, box, entry_widget, notify_id, value_id)

  def on_tool_value_notify_entry(self, obj, prop):
    new_value = obj.get_property(prop.name)
    widget = self.widgets[prop.name].widget
  
    widget.handler_block_by_func(self.on_entry_value_change)
    self.widgets[prop.name].widget.set_text("%.2f" % new_value)
    widget.handler_unblock_by_func(self.on_entry_value_change)
  
  def on_entry_value_change(self, widget, target_prop, pspec):
    try:
      new_value = float(widget.get_text())
      new_value = max(min(new_value, pspec.maximum), pspec.minimum)
    except:
      new_value = self.tool.get_property(target_prop)
    widget.set_text("%.2f" % new_value)
  
    self.tool.handler_block_by_func(self.on_tool_value_notify_entry)
    self.tool.set_property(target_prop, new_value)
    self.tool.handler_unblock_by_func(self.on_tool_value_notify_entry)

  def add_check(self, setting, pspec):
    box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    label_widget = Gtk.Label(pspec.nick)
    check_widget = Gtk.CheckButton()
    box.set_tooltip_text(pspec.blurb)
    
    #FIXME: Initial value
    
    box.pack_start(label_widget, False, False, 0)
    box.pack_end(check_widget, False, False, 0)
    
    self.pack_start(box, False, False, 0)
    
    notify_id = None
    value_id  = None
    
    self.widgets[pspec.name] = SettingWidgetData(setting, box, check_widget, notify_id, value_id)

  def add_slider(self, setting, pspec):
    box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    label_widget = Gtk.Label(pspec.nick)
    #FIXME: Include this in the setting
    step_size = pspec.maximum / 100.0
    scale_widget = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, pspec.minimum, pspec.maximum, step_size)
    scale_widget.set_value(self.tool.get_property(pspec.name))
    box.set_tooltip_text(pspec.blurb)
    
    box.pack_start(label_widget, False, False, 0)
    box.pack_end(scale_widget, True, True, 0)
    
    self.pack_start(box, False, False, 0)
    
    notify_id = self.tool.connect("notify::" + pspec.name,
                                  self.on_tool_value_notify_slider)
                                  
    value_id  = scale_widget.connect("value-changed",
                                     self.on_slider_value_change,
                                     pspec.name)
    
    self.widgets[pspec.name] = SettingWidgetData(setting, box, scale_widget, notify_id, value_id)
    
  def on_tool_value_notify_slider(self, obj, prop):
    new_value = obj.get_property(prop.name)
    widget = self.widgets[prop.name].widget
  
    widget.handler_block_by_func(self.on_slider_value_change)
    self.widgets[prop.name].widget.set_value(new_value)
    widget.handler_unblock_by_func(self.on_slider_value_change)
  
  def on_slider_value_change(self, widget, target_prop):
    self.tool.handler_block_by_func(self.on_tool_value_notify_slider)
    self.tool.set_property(target_prop, widget.get_value())
    self.tool.handler_unblock_by_func(self.on_tool_value_notify_slider)
  
  def add_color(self, setting, pspec):
    box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
    label_widget = Gtk.Label(pspec.nick)
    color_widget = PopupColorButton()
    box.set_tooltip_text(pspec.blurb)
    
    color = self.tool.get_property(pspec.name)
    color = drawbuffer.rgb_to_srgb(color)
    color = Gdk.Color(0xFFFF * color[0], 0xFFFF * color[1], 0xFFFF * color[2])
    color_widget.set_color(color)
    
    box.pack_start(label_widget, False, False, 0)
    box.pack_end(color_widget, True, False, 0)
    
    self.pack_start(box, False, False, 0)
    
    notify_id = self.tool.connect("notify::" + pspec.name,
                                  self.on_tool_value_notify_color)
                                  
    value_id  = color_widget.connect("notify::color",
                                     self.on_color_value_change,
                                     pspec.name)
    
    self.widgets[pspec.name] = SettingWidgetData(setting, box, color_widget, notify_id, value_id)
  
  def on_tool_value_notify_color(self, obj, prop):
    color = obj.get_property(prop.name)
    color = drawbuffer.rgb_to_srgb(color)
    color = Gdk.Color(0xFFFF * color[0], 0xFFFF * color[1], 0xFFFF * color[2])
    
    widget = self.widgets[prop.name].widget
  
    widget.handler_block_by_func(self.on_color_value_change)
    try:
      widget.set_color(color)
    finally:
      widget.handler_unblock_by_func(self.on_color_value_change)
  
  def on_color_value_change(self, widget, prop, target_prop):
    color = widget.get_color()
    color = [float(color.red) / 0xFFFF, float(color.green) / 0xFFFF, float(color.blue) / 0xFFFF]
    color = drawbuffer.srgb_to_rgb(color)
    
    self.tool.handler_block_by_func(self.on_tool_value_notify_color)
    try:
      self.tool.set_property(target_prop, color)
    finally:
      self.tool.handler_unblock_by_func(self.on_tool_value_notify_color)
  
  def set_tool(self, tool):
    if self.tool:
      for widget in self.widgets.values():
        if widget.notify_id:
          self.tool.disconnect(widget.notify_id)
        #FIXME: Does destroy disconnect?
        widget.box.destroy()
      self.widgets = {}

    self.tool = tool
    
    if tool is None:
      return
    
    # PyGI is overzelous about converting props to python
    # values, this gets the actual GParamSpec objects
    pspecs = dict([(i.name, i) for i in tool.props])

    for setting in tool.setting_list:
      try:
        if type(setting) == GPropSetting:
          pspec = pspecs[setting.prop_name]
          value_type = pspec.value_type
          if value_type == GObject.TYPE_FLOAT or value_type == GObject.TYPE_DOUBLE:
            self.add_entry(setting, pspec)
          elif value_type == GObject.TYPE_BOOLEAN:
            self.add_check(setting, pspec)
          else:
            raise Exception("Bad value type %s" % repr(value_type))
        elif type(setting) == GPropSlider:
          pspec = pspecs[setting.prop_name]
          value_type = pspec.value_type
          if value_type == GObject.TYPE_FLOAT or value_type == GObject.TYPE_DOUBLE:
            self.add_slider(setting, pspec)
          else:
            raise Exception("Bad value type %s" % repr(value_type))
        elif type(setting) == GPropColor:
          pspec = pspecs[setting.prop_name]
          self.add_color(setting, pspec)
        else:
          raise Exception("Bad setting type %s" % str(type(setting)))
      except Exception, e:
        import traceback
        traceback.print_exc()
        print("Error adding property:", e)

    self.show_all()

if __name__ == "__main__":
  import tools.RoundBrush
  import settingswidget
  brush = tools.RoundBrush.RoundBrush()
  
  widg = settingswidget.ToolSettingsWidget()
  widg.set_tool(brush)
  window = Gtk.Window()
  window.connect("destroy", lambda x : Gtk.main_quit())
  window.add(widg)
  window.show_all()
  window.move(100, 50)
  
  widg = settingswidget.ToolSettingsWidget()
  widg.set_tool(brush)
  window = Gtk.Window()
  window.connect("destroy", lambda x : Gtk.main_quit())
  window.add(widg)
  window.show_all()
  window.move(350, 50)
  
  Gtk.main()

