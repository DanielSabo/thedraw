from gi.repository import GObject
from gi.repository import Gegl

import drawbuffer
import util

from settingswidget import GPropSlider

CACHE_LAYER_OUTPUT = False

class LayerModes():
  NORMAL   = "thedraw:over"
  MULTIPLY = "thedraw:multiply"
  BURN     = "thedraw:color-burn"
  DODGE    = "thedraw:color-dodge"
  SCREEN   = "thedraw:screen"
  ERASE    = "thedraw:erase"

  valid  = [NORMAL, MULTIPLY, BURN, DODGE, SCREEN, ERASE]

  user   = {"Normal"   : NORMAL,
            "Multiply" : MULTIPLY,
            "Burn"     : BURN,
            "Dodge"    : DODGE,
            "Screen"   : SCREEN,
           }

class Layer(GObject.Object):
  __gproperties__ = {
    'name' : (GObject.TYPE_STRING,
        'Name',
        'Layer Name',
        'Unnamed',
        GObject.PARAM_READWRITE),
    'opacity' : (GObject.TYPE_FLOAT,
        'Opacity',
        'Opacity %',
        0.0,
        100.0,
        100.0,
        GObject.PARAM_READWRITE),
    'mode' : (GObject.TYPE_STRING,
        'Mode',
        'Layer Blend Mode',
        LayerModes.NORMAL,
        GObject.PARAM_READWRITE),
    'visible' : (GObject.TYPE_BOOLEAN,
        'Visible',
        'Layer Visibility',
        True,
        GObject.PARAM_READWRITE),
    }
  
  setting_list = [
      GPropSlider("opacity"),
    ]
    
  __gsignals__ = {
    # The "graph" signal indicates that a node's properties have been modified
    "graph" : (GObject.SignalFlags.RUN_LAST, GObject.TYPE_NONE, ()),
  }

  def __init__(self, drawbuf=None, x=0, y=0, width=None, height=None, name="Unnamed"):
    GObject.Object.__init__(self)
  
    self.opacity    = 1.0
    self.opacity_nodes = []
    self.mode       = LayerModes.NORMAL
    self.mode_nodes = []
    self.visible    = True
    self.output_pairs = []
    self.name       = name
    
    if drawbuf and not width and not height and x == 0 and y == 0:
      self.drawbuf = drawbuf
    elif not drawbuf and not width is None and not height is None:
      self.drawbuf = drawbuffer.DrawBuffer(x=x, y=y, width=width, height=height)
    else:
      raise ValueError("Need either a Drawbuffer or size to create a layer")
  
  @property
  def x(self):
    return self.drawbuf.x
    
  @property
  def y(self):
    return self.drawbuf.y
  
  @property
  def width(self):
    return self.drawbuf.width
    
  @property
  def height(self):
    return self.drawbuf.height
  
  # --- GObject glue ---
  def do_set_property(self, prop, value):
    if prop.name == "opacity":
      if self.opacity * 100.0 != value:
        self.set_opacity(value / 100.0)
    elif prop.name == "visible":
      if self.visible != value:
        self.set_visible(value)
    elif prop.name == "mode":
      if self.mode != value:
        self.set_mode(value)
    elif prop.name == "name":
      if self.name != str(value):
        self.name = str(value)
    else:
      raise ValueError("Unkown property name: %s", prop.name)

  def do_get_property(self, prop):
    if prop.name == "opacity":
      return self.opacity * 100.0
    elif prop.name == "visible":
      return self.visible
    elif prop.name == "mode":
      return self.mode
    elif prop.name == "name":
      return self.name
    else:
      raise ValueError("Unkown property name: %s", prop.name)
  
  def set_opacity(self, opacity):
    opacity = float(opacity)
    if opacity < 0.0 or opacity > 1.0:
      raise ValueError("Opacity must be between 0.0 and 1.0")
    
    if self.opacity == opacity:
      return
    self.opacity = opacity
      
    new_list = []
    for nref in self.opacity_nodes:
      n = nref()
      if n:
        new_list.append(nref)
        n.set_property("value", self.opacity)
    self.opacity_nodes = new_list
    
    self.emit("graph")

  def set_visible(self, visible):
    visible = bool(visible)
    
    if self.visible == visible:
      return
    
    self.visible = visible

    new_list = []
    for connections in self.output_pairs:
      in_node   = connections[0]()
      mode_node = connections[1]()
      if in_node:
        new_list.append([in_node, mode_node, visible])
        if visible:
          in_node.connect_to("output", mode_node, "aux");
        else:
          mode_node.disconnect("aux");
    self.mode_nodes = new_list
    
    self.emit("graph")
  
  def set_mode(self, layer_mode):
    if not layer_mode in LayerModes.valid:
      raise ValueError("Invalid mode %s" % layer_mode)
    
    if self.mode == layer_mode:
      return
    
    self.mode = layer_mode
    new_list = []
    for nref in self.mode_nodes:
      n = nref()
      if n:
        new_list.append(nref)
        n.set_property("operation", self.mode)
    self.mode_nodes = new_list
    
    self.emit("graph")
  
  def get_graph(self, parent_node, inject_input=None, inject_output=None):
    """Return a GEGL node with an "input" sink for the layer
       below and an "output" sink for the layer above.
       
       parent_node: The GEGL node to attach the graph's nodes to
       inject_input: Input node of the graph to insert between the buffer
                     and the start of this layer's chain.
       inject_output: Output node of the graph to insert between the buffer
                      and the start of this layer's chain."""
    
    db_src = parent_node.create_child("gegl:buffer-source")
    db_src.set_property("buffer", self.drawbuf.get_gegl())
    
    if (inject_input or inject_output):
      if not (inject_input and inject_output):
        raise ValueError("Must specify both inject_input and inject_output")
      db_src.connect_to("output", inject_input, "input")
      db_src = inject_output
    
    # Note, this needs to use GObject weak_ref() rather than python weakref()
    # because the only hard reference is from the parent_node in GObject land.
    
    opacity = parent_node.create_child("thedraw:opacity")
    opacity.set_property("value", self.opacity)
    self.opacity_nodes.append(opacity.weak_ref())

    if CACHE_LAYER_OUTPUT:
      reformat = parent_node.create_child("thedraw:convert-format")
      reformat.set_property("format", "R'aG'aB'aA float")
      reformat.set_property("dont-cache", False)
    
      mode = parent_node.create_child(self.mode)
      self.mode_nodes.append(mode.weak_ref())
      self.output_pairs.append([reformat.weak_ref(), mode.weak_ref(), self.visible])

      db_src.connect_to("output", opacity, "input")
      opacity.connect_to("output", reformat, "input")
      if (self.visible):
        reformat.connect_to("output", mode, "aux")
    else:
      mode = parent_node.create_child(self.mode)
      self.mode_nodes.append(mode.weak_ref())
      self.output_pairs.append([opacity.weak_ref(), mode.weak_ref(), self.visible])
    
      db_src.connect_to("output", opacity, "input")
      if (self.visible):
        opacity.connect_to("output", mode, "aux")
    
    return mode
  
  def merge_down(self, target_layer, rect=None, expand=False):
    """Blend this layer onto target_layer, if rect is specified 
       only that region of the source will be applied.
       
       target_layer: layer to write to
       rect: source region (x, y, width, height)
       expand: expand target_layer's extent to include the source"""
    ptn = Gegl.Node()
    src = ptn.create_child("gegl:buffer-source")
    src.set_property("buffer", target_layer.drawbuf.get_gegl())
    layer_node = self.get_graph(ptn)
    dst = ptn.create_child("gegl:write-buffer")
    dst.set_property("buffer", target_layer.drawbuf.get_gegl())
    
    src.connect_to("output", layer_node, "input")
    layer_node.connect_to("output", dst, "input")
    
    if rect is None:
      rect = (self.x, self.y, self.width, self.height)
    else:
      rect = util.rect_intersect(rect, (self.x, self.y, self.width, self.height))
    
    target_rect = (target_layer.x, target_layer.y, target_layer.width, target_layer.height)
    
    if expand:
      target_rect = util.rect_union(rect, target_rect)
      target_layer.drawbuf.set_extent(*target_rect)
    else:
      rect = util.rect_intersect(rect, target_rect)
    
    dst.blit_buffer(None, util.rect_to_gegl(rect))

  def duplicate(self):
    dup = Layer(drawbuf=self.drawbuf.duplicate_tiles())
    
    if self.name.endswith(" Copy"):
      dup.name = self.name
    elif self.name:
      dup.name = self.name + " Copy"
    else:
      dup.name = "Copy"
    
    dup.mode = self.mode
    dup.opacity = self.opacity
    
    return dup

  def translate_clone(self, x, y):
    """Create a clone of this layer with it's origin moved to x, y"""
    new_layer = Layer(x=x, y=y, width=self.width, height=self.height)
    new_layer.name = self.name
    new_layer.mode = self.mode
    new_layer.visible = self.visible
    new_layer.opacity = self.opacity
    
    self.drawbuf.blit_to(new_layer.drawbuf, x, y)
    
    return new_layer
