from __future__ import print_function
from __future__ import division

from gi.repository import Gegl

def raster_line(x,y,x2,y2):
  # Brensenham line algorithm generator
  
  x,y,x2,y2 = map(int, [x,y,x2,y2])
  
  steep = 0
  dx = abs(x2 - x)
  if (x2 - x) > 0: 
    sx = 1
  else: 
    sx = -1
  dy = abs(y2 - y)
  if (y2 - y) > 0:
    sy = 1
  else: 
    sy = -1
  if dy > dx:
    steep = 1
    x,y = y,x
    dx,dy = dy,dx
    sx,sy = sy,sx
  d = (2 * dy) - dx
  for i in range(0,dx):
    if steep:
      yield (y,x)
    else: 
      yield (x,y)
    while d >= 0:
      y = y + sy
      d = d - (2 * dx)
    x = x + sx
    d = d + (2 * dy)
  yield (x2,y2) 

def rect_union(r1, r2):
  if r1[2] == 0 or r1[3] == 0:
    return tuple(r2)
  elif r2[2] == 0 or r2[3] == 0:
    return tuple(r1)

  x = min(r1[0], r2[0])
  y = min(r1[1], r2[1])
  w = max(r1[0] + r1[2], r2[0] + r2[2]) - x
  h = max(r1[1] + r1[3], r2[1] + r2[3]) - y
  return (x, y, w, h)

def rect_intersect(r1, r2):
  x = max(r1[0], r2[0])
  y = max(r1[1], r2[1])
  w = min(r1[0] + r1[2], r2[0] + r2[2]) - x
  h = min(r1[1] + r1[3], r2[1] + r2[3]) - y
  
  if w <= 0 or h <= 0:
    return (0, 0, 0, 0)
  return (x, y, w, h)

def rect_to_gegl(r):
  return Gegl.Rectangle.new(r[0], r[1], r[2], r[3])
