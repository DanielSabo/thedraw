from __future__ import print_function

from gi.repository import Gtk

import resourceloader

class AboutDialog(Gtk.AboutDialog):
  def __init__(self):
    Gtk.AboutDialog.__init__(self)
    big_logo = resourceloader.get_logo_pixbuf()
    if big_logo:
      self.set_logo(big_logo)
    else:
      self.set_logo_icon_name("thedraw-logo")
    self.set_copyright(u"Copyright \xA9 2013 Daniel Sabo")
    self.set_license_type(Gtk.License.GPL_3_0)
    self.set_version("Version Zero")
  
  def do_response(self, response):
    self.destroy()

if __name__ == "__main__":
  pass