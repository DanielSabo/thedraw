#!/usr/bin/env python

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject
try:
  from gi.repository import Rsvg
except:
  import traceback
  traceback.print_exc()

import cairo
import math
import os

class SpinWindow(Gtk.Window):
  def __init__(self):
    Gtk.Window.__init__(self)
        
    self.set_title("Spin Widget")

    # Tell GTK+ that we want to draw the windows background ourself.
    # If we don't do this then GTK+ will clear the window to the
    # opaque theme default color, which isn't what we want.
    self.set_app_paintable(True)
     
    self.connect("delete-event", Gtk.main_quit)
    self.connect("draw", self.on_draw)
    self.connect("screen-changed", self.screen_changed)
    self.connect("key-press-event", self.key_pressed)
    
    self.screen_changed(self)

  def key_pressed(self, widget, event):
    if event.keyval == Gdk.KEY_Escape:
      self.destroy()
      
  def on_draw(self, widget, cr):
    if self.supports_alpha == True:
      cr.set_source_rgba(0.0, 0.0, 0.0, 0.0)
    else:
      cr.set_source_rgb(0.6, 0.6, 0.6)
    
    # Draw the background
    cr.set_operator(cairo.OPERATOR_SOURCE)
    cr.paint()
    
    return False
  
  def screen_changed(self, widget, old_screen=None):
    # To check if the display supports alpha channels, get the colormap
    screen = widget.get_screen()
    visual = screen.get_rgba_visual()
    if visual == None:
      self.supports_alpha = False
    else:
      widget.set_visual(visual)
      self.supports_alpha = True
    
    return False

class SpinWidget(Gtk.DrawingArea):
  __gproperties__ = {
    'value' : (GObject.TYPE_PYOBJECT,
        'value',
        'value',
        GObject.PARAM_READWRITE),
    }

  def __init__(self, value = 0, rate = 1.0):
    Gtk.DrawingArea.__init__(self)
    
    self.padding = 2
    self.inner_radius  = 10
    self.mark_inner_radius = 30
    self.outer_radius  = 60
    self.current_angle = None
    self.current_value = value
    self.rate = rate
    self.limits = (None, None)
    
    self.always_track = False
    self.tracking_motion = False
     
    self.connect("draw", self.on_draw)

    self.add_events(Gdk.EventMask.BUTTON_PRESS_MASK   |
                    Gdk.EventMask.POINTER_MOTION_MASK |
                    Gdk.EventMask.BUTTON_RELEASE_MASK)
    self.connect("button-press-event", self.on_button_press)
    self.connect("button-release-event", self.on_button_release)
    self.connect("motion-notify-event", self.on_motion)
    self.connect("configure-event", self.on_configure_event)
    
    try:
      dir_path  = os.path.dirname(__file__)
      file_path = os.path.join(dir_path, "spinwidget_arrows.svg")
      self.svg_background = Rsvg.Handle.new_from_file(file_path)
    except:
      # Catching a specific exception would be nice but gi only throws a generic one
      self.svg_background = None

  def do_set_property(self, prop, value):
    if prop.name == "value":
      self.current_value = value
      self.queue_draw()

  def do_get_property(self, prop):
    if prop.name == "value":
      return self.current_value

  def on_configure_event(self, widget, event):
    size = max(event.width, event.height)
    self.outer_radius = min(event.width, event.height) // 2 - self.padding
    self.set_size_request(size, size)
  
  def on_motion(self, widget, event):
    if self.tracking_motion or self.always_track:
      allocation = widget.get_allocation()
      width = allocation.width
      height = allocation.height
      center_x = width / 2
      center_y = height / 2
      
      radius = math.sqrt((event.x - center_x)**2 + (event.y - center_y)**2)
      
      if radius > self.outer_radius or radius < self.inner_radius:
        self.current_angle = None
        return

      new_angle = None
      if event.x - center_x == 0:
        if event.y > center_y:
          new_angle = 180
        else:
          new_angle = 0
      else:
        m = float(event.y - center_y) / float(event.x - center_x)
        deg = math.degrees(math.atan(m)) + 90
        if event.x < center_x:
          deg += 180
        new_angle = deg
      
      if self.current_angle is None:
        self.current_angle = new_angle
      
      delta_angle = new_angle - self.current_angle
      if delta_angle > 300:
        # Probably crossed over
        delta_angle = 360 - delta_angle
      if delta_angle < -300:
        delta_angle = 360 + delta_angle
      
      if abs(delta_angle) >= 0.1:
        self.current_angle = new_angle
        new_value = self.current_value + self.rate * delta_angle
        if self.limits[0] and self.limits[0] > new_value:
          new_value = self.limits[0]
        elif self.limits[1] and self.limits[1] < new_value:
          new_value = self.limits[1]
        self.current_value = new_value
        self.notify("value")
      
      self.queue_draw()

  def on_button_press(self, widget, event):
    if event.button == 1 or self.always_track:
      self.tracking_motion = True

  def on_button_release(self, widget, event):
    if event.button == 1 and not self.always_track:
      self.tracking_motion = False

  def on_draw(self, widget, cr):
    allocation = widget.get_allocation()
    width = allocation.width
    height = allocation.height
    
    if self.svg_background is None:
      cr.set_source_rgba(1.0, 1.0, 1.0, 0.0) # Transparent

      # Draw a circle
      cr.set_source_rgba(0.847, 0.886, 0.890, 1.0)
        
      center_x = float(width) /2
      center_y = float(height)/2
      
      cr.arc(center_x, center_y, self.outer_radius, 0, 2.0*3.14)
      cr.move_to(center_x + self.inner_radius, center_y)
      cr.arc_negative(center_x, center_y, self.inner_radius, 2.0*3.14, 0)
      cr.fill()
      
      cr.set_operator(cairo.OPERATOR_SOURCE)
      cr.set_source_rgba(0.0, 0.0, 0.0, 1.0)
      
      cr.set_line_width(1)
      
      cr.arc(center_x, center_y,
           (self.outer_radius - self.mark_inner_radius) / 2 + self.mark_inner_radius, 
           0, 2.0*3.14)

      for i in range(8):
        angle = (3.14 * 2.0 * i) / 8.0
        
        cr.move_to(math.sin(angle) * (self.mark_inner_radius + 5) + center_x,
               math.cos(angle) * (self.mark_inner_radius + 5) + center_y)

        cr.line_to(math.sin(angle) * (self.outer_radius - 5) + center_x,
               math.cos(angle) * (self.outer_radius - 5) + center_y)
      cr.stroke()
      
      
      cr.set_line_width(0.5)

      for i in range(24):
        angle = (3.14 * 2.0 * i) / 24.0 #+ (3.14 / 16.0)
        
        cr.move_to(math.sin(angle) * (self.mark_inner_radius + 10) + center_x,
               math.cos(angle) * (self.mark_inner_radius + 10) + center_y)

        cr.line_to(math.sin(angle) * (self.outer_radius - 10) + center_x,
               math.cos(angle) * (self.outer_radius - 10) + center_y)
      cr.stroke()
    else:
      svg_dimentions  = self.svg_background.get_dimensions()
      
      cr.save()
      cr.translate(self.padding, self.padding)
      cr.scale(float(width  - self.padding * 2) / float(svg_dimentions.width),
               float(height - self.padding * 2) / float(svg_dimentions.height))
      self.svg_background.render_cairo(cr)
      cr.restore()
    
    cr.set_operator(cairo.OPERATOR_SOURCE)
    cr.set_source_rgba(0.0, 0.0, 0.0, 1.0)
    
    size_string = "%.2f" % self.current_value
    
    cr.select_font_face("sans-serif",
              cairo.FONT_SLANT_NORMAL,
              cairo.FONT_WEIGHT_BOLD)

    x_bearing, y_bearing, text_width, text_height = cr.text_extents(size_string)[:4]
    cr.move_to((width - text_width)/2.0, (height + text_height)/2.0)
    cr.show_text(size_string)
    
    return False

  def set_limits(self, min_value, max_value):
    self.limits = (min_value, max_value)

if __name__ == '__main__':
  class TestWindow(Gtk.Window):
    def __init__(self):
      Gtk.Window.__init__(self)
      
      button = Gtk.Button.new_with_label("Test")
      button.connect("clicked", self.on_clicked)
      
      self.add(button)
      self.show_all()

      self.set_default_size(200, 400)
      self.move(100, 200)
      
      self.connect("destroy", self.on_destroy)
    
    def on_clicked(self, widget):
      swidg = SpinWidget()
      swin = SpinWindow()
      swin.add(swidg)
      swin.show_all()
      swin.grab_focus()

    def on_destroy(self, widget):
      Gtk.main_quit()

  win = TestWindow()
  win.show_all()
  Gtk.main()

