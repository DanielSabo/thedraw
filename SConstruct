import os, sys, subprocess

AddOption("--verbose",
          help="print all build commands",
          action="store_true")

AddOption("--prefix",
          dest="prefix",
          type="string",
          nargs=1,
          action="store",
          metavar="DIR",
          help="installation prefix")

# ENV=os.environ will clone the current enviroment variables (like
# PKG_CONFIG_PATH), by default scons doesn't propagate this.
env = Environment(ENV=os.environ)

CONFIG_FILE_NAME = "scons.conf"
variables = Variables(CONFIG_FILE_NAME)
variables.AddVariables(
    (PathVariable("PREFIX", "installation prefix", "/usr")),
    ("CCFLAGS", "C/C++ compiler arguments", ["-g", "-Wall"]),
  )

# Load saved/default values into env
variables.Update(env)

# Parse any changes the user made to the variables
if GetOption("prefix"):
  env["PREFIX"] = GetOption("prefix")

# Save the updated config before we do anything else to the environment
variables.Save(CONFIG_FILE_NAME, env)

for vk in variables.keys():
  print "%s:" % vk, env[vk]

if not GetOption("verbose"):
  env.Replace(CCCOMSTR = "CC $SOURCES ==> $TARGET",
              SHCCCOMSTR = "CC $SOURCES ==> $TARGET",
              LINKCOMSTR = "CCLD $SOURCES ==> $TARGET",
              SHLINKCOMSTR = "CCLD $SOURCES ==> $TARGET")

# Set options specific to this module
drawbuffer_env = env.Clone()

# Try to find a usable python
if 0 == subprocess.call(["pkg-config","--exists","python2"]) and \
   0 == subprocess.call(["pkg-config","--atleast-version=2.7","python2"]):
  drawbuffer_env.ParseConfig("pkg-config python2 --cflags --libs")
elif 0 == subprocess.call(["pkg-config","--exists","python-2.7"]) and \
     0 == subprocess.call(["pkg-config","--atleast-version=2.7","python-2.7"]):
  drawbuffer_env.ParseConfig("pkg-config python2 --cflags --libs")
elif 0 == subprocess.call(["pkg-config","--exists","python"]) and \
     0 == subprocess.call(["pkg-config","--atleast-version=2.7","python"]) and \
     0 == subprocess.call(["pkg-config","--max-version=2.99","python"]):
  drawbuffer_env.ParseConfig("pkg-config python2 --cflags --libs")
elif sys.platform == "win32":
  # There's no pkg-config here, so we just set some values and hope
  drawbuffer_env.Append(CFLAGS = "-I/c/Tools/Python27/include -L/c/Tools/Python27/libs")
  drawbuffer_env.Append(LDFLAGS = "-lpython27")
else:
  raise RuntimeError("Couldn't find a usable python")


if sys.platform == "win32":
  drawbuffer_env.Append(CFLAGS = ["-m32", "-mms-bitfields"])

drawbuffer_env.ParseConfig("pkg-config pycairo --cflags --libs")
drawbuffer_env.ParseConfig("pkg-config pygobject-3.0 --cflags --libs")
drawbuffer_env.ParseConfig("pkg-config gegl-0.3 --cflags --libs")

drawbuffer_env.Append(CFLAGS = ["-fno-strict-aliasing", "-O2", "-msse", "-msse2"])

if sys.platform == "win32":
  drawbuffer_env["SHLIBPREFIX"] = ""
  drawbuffer_env["SHLIBSUFFIX"] = ".pyd"
else:
  drawbuffer_env["SHLIBPREFIX"] = ""
  drawbuffer_env["SHLIBSUFFIX"] = ".so"

drawbuffer_lib = drawbuffer_env.SharedLibrary(target="drawbuffer",
                                              source=["drawbuffer.c", "maskbuffer.c", "drawbuffer-util.c"])

SConscript(["geglop/SConscript"], exports=["env"])
SConscript(["tools/SConscript"], exports=["env"])
SConscript(["tests/SConscript"], exports=["env"])

install_py_files = Split("""
    brushpreviewpopup.py
    colorbutton.py
    consolewidget.py
    dialogs.py
    drawcanvas.py
    gesturepopup.py
    layers.py
    layerswidget.py
    ora.py
    paintcursor.py
    resourceloader.py
    selectbox.py
    settingswidget.py
    spinwidget.py
    thedraw.py
    util.py
    validatedentry.py
""")

install_env = env.Clone()

install_lib_path = os.path.join(env["PREFIX"], "lib", "thedraw")

installed_lib = install_env.Install(install_lib_path, drawbuffer_lib)
installed_python = install_env.Install(install_lib_path, install_py_files)

# Fix library link paths on OSX
if sys.platform == "darwin":
  install_env.AddPostAction(installed_lib, "install_name_tool -id $TARGET $TARGET")

# Compile the python
# import py_compile
# def func(target, source, env):
#   for t in target:
#     py_compile.compile(str(t))
# install_env.AddPostAction(installed_python, func)

# Disable install alias, because we have no way to use the installed files yet
# inst_alias = env.Alias('install', env["PREFIX"])
