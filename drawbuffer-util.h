#ifndef __DRAWBUFFER_UTIL_H__
#define __DRAWBUFFER_UTIL_H__

PyObject* gegl_buffer_copy_to_cairo (PyObject *self, PyObject *args);

#endif /* __DRAWBUFFER_UTIL_H__ */
